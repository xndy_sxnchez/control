/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.dto;

/**
 *
 * @author gutya
 */
public class HorarioDTO {

    private Long id;
    private Integer idCategoria;
    private Integer idCentroRevision;
    private Boolean disponible;
    private Boolean reservado;
    private Boolean exitoso;
    private Long fecha;
    private Long hora;
    private String horaLabel;
    private String styleBTN;

    public HorarioDTO() {
    }

    public HorarioDTO(Long id, Integer idCategoria, Integer idCentroRevision, Boolean disponible, Long fecha, Long hora) {
        this.id = id;
        this.idCategoria = idCategoria;
        this.idCentroRevision = idCentroRevision;
        this.disponible = disponible;
        this.fecha = fecha;
        this.hora = hora;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }

    public Long getFecha() {
        return fecha;
    }

    public void setFecha(Long fecha) {
        this.fecha = fecha;
    }

    public Long getHora() {
        return hora;
    }

    public void setHora(Long hora) {
        this.hora = hora;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdCentroRevision() {
        return idCentroRevision;
    }

    public void setIdCentroRevision(Integer idCentroRevision) {
        this.idCentroRevision = idCentroRevision;
    }

    public String getHoraLabel() {
        return horaLabel;
    }

    public void setHoraLabel(String horaLabel) {
        this.horaLabel = horaLabel;
    }

    public String getStyleBTN() {
        return styleBTN;
    }

    public void setStyleBTN(String styleBTN) {
        this.styleBTN = styleBTN;
    }

    public Boolean getReservado() {
        return reservado;
    }

    public void setReservado(Boolean reservado) {
        this.reservado = reservado;
    }

    public Boolean getExitoso() {
        return exitoso;
    }

    public void setExitoso(Boolean exitoso) {
        this.exitoso = exitoso;
    }
    
}
