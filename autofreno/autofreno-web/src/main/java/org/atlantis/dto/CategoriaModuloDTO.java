package org.autofreno.dto;

public class CategoriaModuloDTO {

    private String codigo;
    private String descripcion;
    private String usuario;

    public CategoriaModuloDTO(){

    }

    public CategoriaModuloDTO(String codigo, String descripcion, String usuario) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.usuario = usuario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
}
