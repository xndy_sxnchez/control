/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.dto;

/**
 *
 * @author gutya
 */
public class TurnoDTO {

    private Long id;
    private Integer idCategoria;
    private String categoria;
    private Integer idCentroRevision;
    private String centroRevision;
    private Long idHorario;
    private String horario;
    private Long idSolicitante;
    private String solicitante;
    private Long idVehiculo;
    private String vehiculo;
    private Long idUsuario;
    private String usuario;
    private Integer idTipoTurno;
    private Integer idCategoriaTipo;

    public TurnoDTO() {
    }

    public TurnoDTO(Integer idCategoria, String categoria, Integer idCentroRevision,
            String centroRevision, Long idHorario, String horario, Long idSolicitante,
            String solicitante, Long idVehiculo, String vehiculo, Long idUsuario,
            String usuario, Integer idTipoTurno, Integer idCategoriaTipo) {
        this.idCategoria = idCategoria;
        this.categoria = categoria;
        this.idCentroRevision = idCentroRevision;
        this.centroRevision = centroRevision;
        this.idHorario = idHorario;
        this.horario = horario;
        this.idSolicitante = idSolicitante;
        this.solicitante = solicitante;
        this.idVehiculo = idVehiculo;
        this.vehiculo = vehiculo;
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.idTipoTurno = idTipoTurno;
        this.idCategoriaTipo = idCategoriaTipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Long idHorario) {
        this.idHorario = idHorario;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Long getIdSolicitante() {
        return idSolicitante;
    }

    public void setIdSolicitante(Long idSolicitante) {
        this.idSolicitante = idSolicitante;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public Long getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Long idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(String vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Integer getIdCentroRevision() {
        return idCentroRevision;
    }

    public void setIdCentroRevision(Integer idCentroRevision) {
        this.idCentroRevision = idCentroRevision;
    }

    public String getCentroRevision() {
        return centroRevision;
    }

    public void setCentroRevision(String centroRevision) {
        this.centroRevision = centroRevision;
    }

    public Integer getIdTipoTurno() {
        return idTipoTurno;
    }

    public void setIdTipoTurno(Integer idTipoTurno) {
        this.idTipoTurno = idTipoTurno;
    }

    public Integer getIdCategoriaTipo() {
        return idCategoriaTipo;
    }

    public void setIdCategoriaTipo(Integer idCategoriaTipo) {
        this.idCategoriaTipo = idCategoriaTipo;
    }

}
