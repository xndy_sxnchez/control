package org.autofreno.dto;

import java.util.Date;
import java.util.List;

public class CategoriaDTO {

    private Integer id;
    private Integer cantidadDiponible;
    private String tipo;
    private String abreviatura;
    private String producto;
    private String abreviaturaDescripcion;
    private String descripcionBreve;
    private Double valor;
    private String tiempoEntrega;
    private String requisito;
    private String estado;

    public CategoriaDTO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getAbreviaturaDescripcion() {
        return abreviaturaDescripcion;
    }

    public void setAbreviaturaDescripcion(String abreviaturaDescripcion) {
        this.abreviaturaDescripcion = abreviaturaDescripcion;
    }

    public String getDescripcionBreve() {
        return descripcionBreve;
    }

    public void setDescripcionBreve(String descripcionBreve) {
        this.descripcionBreve = descripcionBreve;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getTiempoEntrega() {
        return tiempoEntrega;
    }

    public void setTiempoEntrega(String tiempoEntrega) {
        this.tiempoEntrega = tiempoEntrega;
    }

    public String getRequisito() {
        return requisito;
    }

    public void setRequisito(String requisito) {
        this.requisito = requisito;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getCantidadDiponible() {
        return cantidadDiponible;
    }

    public void setCantidadDiponible(Integer cantidadDiponible) {
        this.cantidadDiponible = cantidadDiponible;
    }

}
