/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.app.filter;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.autofreno.app.UsuarioSession;

/**
 *
 * @author Xndy
 */
@WebFilter(filterName = "CORSFilter", urlPatterns = {"*.xhtml"})
public class CORSFilter implements Filter {

    @Inject
    private UsuarioSession session;

    /**
     * Default constructor.
     */
    public CORSFilter() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {
        // TODO Auto-generated method stub
    }

    /**
     * @param servletRequest
     * @param servletResponse
     * @param chain
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;

        // Authorize (allow) all domains to consume the content
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, UPDATE, HEAD, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        String reqURI = request.getRequestURI();

        if (reqURI.contains("login") || (session.getUsuario() != null) || reqURI.contains("javax.faces.resource") || reqURI.contains("resource")/*) {*/) {
            try {
                chain.doFilter(request, response);
            } catch (IOException | ServletException iOException) {
                resp.sendRedirect(request.getContextPath() + "/login");
            }
        } else if (reqURI.contains("forgot") || reqURI.contains("registro") || reqURI.contains("activar") || reqURI.contains("cambioClave")) {
            chain.doFilter(request, response);
        } else {
            resp.sendRedirect(request.getContextPath() + "/login");  // Anonymous user. Redirect to login page
        }

//
//        // pass the request along the filter chain
//        chain.doFilter(request, servletResponse);
    }

    /**
     * @param fConfig
     * @throws javax.servlet.ServletException
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }

}
