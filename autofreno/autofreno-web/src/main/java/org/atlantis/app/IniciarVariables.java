/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.app;

import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import org.autofreno.ws.UrlRest;

/**
 *
 * @author gutya
 */
@Singleton(name = "iniciarVariables")
@Startup
@ApplicationScoped
public class IniciarVariables {

    private final Logger LOG = Logger.getLogger(IniciarVariables.class.getName());
    private final String app = "ATLANTIS-WEB";
    private final String ambiente = "DESARROLLO";

    /*
        DESARROLLO   
     */
    private final String appUrlDesarrollo = "http://127.0.0.1:8780/atlantis/api";

    public void appinit(@Observes @Initialized(ApplicationScoped.class) Object init) {

        switch (ambiente) {
            case "DESARROLLO":
                UrlRest.wsApp = appUrlDesarrollo;
                break;
            case "PRODUCCION":
                break;
        }
        UrlRest.app = "ATLANTIS-WEB";
        UrlRest.userAtlantis = "atlantis";
        UrlRest.passAtlantis = "atlantis";

        //USUARIO
        UrlRest.rIniciarSesion = UrlRest.wsApp + "/iniciarSesion";
        UrlRest.rUsuario = UrlRest.wsApp + "/usuariosEMOT";
        UrlRest.rTerminosCondiciones = UrlRest.wsApp + "/terminosCondiciones";
        UrlRest.rValidarUsuario = UrlRest.wsApp + "/usuario/validar/";
        UrlRest.rBuscarUsuario = UrlRest.wsApp + "/usuario/persona/buscar/identificacion/";
        UrlRest.rRegistrarUsuario = UrlRest.wsApp + "/usuario/guardar";
        UrlRest.rActivarUsuario = UrlRest.wsApp + "/usuario/activarUsuario/identificacion/";
        UrlRest.rActualizarContrasenia = UrlRest.wsApp + "/usuario/actualizarContrasenia";
        UrlRest.rRecuperarContrasenia = UrlRest.wsApp + "/usuario/recuperar/iduser/";

        //ROLES
        UrlRest.rRoles = UrlRest.wsApp + "/roles";

        //PERSONA
        UrlRest.rBuscarPersona = UrlRest.wsApp + "/persona/aplicacion/";
        UrlRest.rPersonas = UrlRest.wsApp + "/personas";
        UrlRest.rRegistrarPersona = UrlRest.wsApp + "/persona/guardar";

        //CATEGORIGATIPO
        UrlRest.rCategoriasTipos = UrlRest.wsApp + "/categoriasTipos";
        UrlRest.rRegistrarCategoriaTipo = UrlRest.wsApp + "/categoriaTipo/guardar";

        //CATEGORIA
        UrlRest.rCategoria = UrlRest.wsApp + "/categorias";
        UrlRest.rIdCategoria = UrlRest.wsApp + "/categoria/id/";
        UrlRest.rRegistrarCategoria = UrlRest.wsApp + "/categoria/guardar";
        UrlRest.rEliminarCategoria = UrlRest.wsApp + "/categoria/eliminar";

        UrlRest.rEmpresa = UrlRest.wsApp + "/empresas";
        UrlRest.rRegistrarEmpresa = UrlRest.wsApp + "/empresa/guardar";

        UrlRest.rInventarios = UrlRest.wsApp + "/inventarios";
        UrlRest.rRegistrarInventarios = UrlRest.wsApp + "/inventario/guardar";

        UrlRest.rOrdenes = UrlRest.wsApp + "/ordenes";
        UrlRest.rRegistrarOrden = UrlRest.wsApp + "/orden/guardar";
        UrlRest.rActualizarOrden = UrlRest.wsApp + "/orden/idOrden/";

        UrlRest.rConsultaVehiculo = UrlRest.wsApp + "/antservice/placa/";
        UrlRest.rEditarUsuario = UrlRest.wsApp + "/usuario/editar";
        UrlRest.rEliminarUsuario = UrlRest.wsApp + "/usuario/eliminar";
        
    }

}
