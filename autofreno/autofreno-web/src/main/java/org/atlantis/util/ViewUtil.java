/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

@RequestScoped
public class ViewUtil implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ViewUtil.class.getName());

    public void goToPage(String page) {
        try {
            Boolean esraiz = page.startsWith("/");
            if (esraiz) {
                getContext().redirect(getUrlContext() + page);
            } else {
                getContext().redirect(page);
            }

        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Error en redireccion a URL", e);
        }
    }

    public ExternalContext getContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }

    public String getContextName() {
        return getContext().getContextName();
    }

    public String getUrlContext() {
        StringBuilder sb = new StringBuilder();
        sb.append(getContext().getRequestScheme() + "://");
        sb.append(getContext().getRequestServerName());
        if (getContext().getRequestServerPort() != 80) {
            sb.append(":" + getContext().getRequestServerPort());
        }
        sb.append(getContext().getRequestContextPath());

        return sb.toString();
    }

    public Boolean isPostBack() {
        return FacesContext.getCurrentInstance().isPostback();
    }

}
