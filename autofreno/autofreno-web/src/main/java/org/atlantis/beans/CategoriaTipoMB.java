/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.model.CategoriaTipo;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class CategoriaTipoMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;

    private List<CategoriaTipo> categoriaTipos;
    private CategoriaTipo categoriaTipo;

    @PostConstruct
    public void init() {
        loadData();
    }

    public void loadData() {
        categoriaTipos = atlantisService.findAllCategoriaTipos();
        categoriaTipo = new CategoriaTipo();
    }

    public void editar(CategoriaTipo categoriaTipo){
        this.categoriaTipo = categoriaTipo;
    }
    
    public void guardar() {
        CategoriaTipo cr = atlantisService.registrarCategoriaTipo(categoriaTipo);
        if (cr != null && cr.getId() != null) {
            loadData();
            AtlantisUtil.messageInfo("Los datos se grabaron Correctamente", "");
        } else {
            AtlantisUtil.messageWarning("Los datos no pudieron ser grabados Correctamente", "");
        }
    }

    public List<CategoriaTipo> getCategoriaTipos() {
        return categoriaTipos;
    }

    public void setCategoriaTipos(List<CategoriaTipo> categoriaTipos) {
        this.categoriaTipos = categoriaTipos;
    }

    public CategoriaTipo getCategoriaTipo() {
        return categoriaTipo;
    }

    public void setCategoriaTipo(CategoriaTipo categoriaTipo) {
        this.categoriaTipo = categoriaTipo;
    }

}
