/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.dto.CategoriaDTO;
import org.autofreno.model.Categoria;
import org.autofreno.model.CategoriaTipo;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class CategoriaMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;

    private Categoria categoria;

    private List<CategoriaDTO> categorias;
    private List<CategoriaTipo> categoriaTipos;

    private Boolean editar;

    private String imagen;

    private Integer idCategoriaTipo;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        idCategoriaTipo = null;
        imagen = "";
        categoria = new Categoria();
        categoriaTipos = atlantisService.findAllCategoriaTipos();
        categorias = atlantisService.findAllCategorias();
        editar = Boolean.FALSE;
    }

    public void loadCategoria(CategoriaDTO categoriaDTO) {
        categoria = atlantisService.buscarCategoria(categoriaDTO.getId());
        idCategoriaTipo = categoria.getCategoriaTipo().getId();
        editar = Boolean.TRUE;
    }

    public void guardarCategoria() {
        if (validar()) {
            categoria = atlantisService.registrarCategoria(categoria);
            if (categoria != null && categoria.getId() != null) {
                AtlantisUtil.messageInfo("Los datos se grabaron Correctamente", "");
                loadData();
            } else {
                AtlantisUtil.messageWarning("Los datos no pudieron ser grabados Correctamente", "");
            }
        } else {
            AtlantisUtil.messageWarning("Ingrese los campos obligatorios ", "");
        }
    }

    public void eliminarCategoria(CategoriaDTO categoriaDTO) {
        categoria = atlantisService.buscarCategoria(categoriaDTO.getId());
        categoria = atlantisService.eliminarCategoria(categoria);
        if (categoria != null && categoria.getId() != null) {
            AtlantisUtil.messageInfo("Los datos se eliminaron Correctamente", "");
            loadData();
        } else {
            AtlantisUtil.messageWarning("Los datos no pudieron ser eliminados", "");
        }
    }

    private Boolean validar() {
        if (idCategoriaTipo == null) {
            return Boolean.FALSE;
        }
        if (categoria.getAbreviatura().isEmpty()) {
            return Boolean.FALSE;
        }
        if (categoria.getProducto().isEmpty()) {
            return Boolean.FALSE;
        }
        if (categoria.getDescripcionBreve().isEmpty()) {
            return Boolean.FALSE;
        }
        
        if (categoria.getValor() == null) {
            return Boolean.FALSE;
        }
        
        categoria.setAbreviatura(categoria.getAbreviatura().toUpperCase());
        categoria.setProducto(categoria.getProducto().toUpperCase());
        categoria.setCategoriaTipo(new CategoriaTipo(idCategoriaTipo));

        return true;
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            imagen = event.getFile().getFileName();
            categoria.setImagen64(Base64.getEncoder().encodeToString(event.getFile().getContents()));
        } catch (Exception ex) {
            AtlantisUtil.messageWarning("Intente nuevamente", "");
            Logger.getLogger(CategoriaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<CategoriaDTO> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaDTO> categorias) {
        this.categorias = categorias;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Boolean getEditar() {
        return editar;
    }

    public void setEditar(Boolean editar) {
        this.editar = editar;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public List<CategoriaTipo> getCategoriaTipos() {
        return categoriaTipos;
    }

    public void setCategoriaTipos(List<CategoriaTipo> categoriaTipos) {
        this.categoriaTipos = categoriaTipos;
    }

    public Integer getIdCategoriaTipo() {
        return idCategoriaTipo;
    }

    public void setIdCategoriaTipo(Integer idCategoriaTipo) {
        this.idCategoriaTipo = idCategoriaTipo;
    }

}
