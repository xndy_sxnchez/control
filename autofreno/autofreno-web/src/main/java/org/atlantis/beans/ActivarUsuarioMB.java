/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.model.Usuario;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author Origami
 */
@Named
@ViewScoped
public class ActivarUsuarioMB implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ActivarUsuarioMB.class.getName());

    @Inject
    private AtlantisService atlantisService;

    private Integer code1;  //Id de PubUsuario
    private String code2;   //Identificacion Persona
    private Long code3;     //Fecha limite de validacion

    private Usuario usuario;
    private String mensaje = "";
    private Boolean render = false;

    public void doPreRenderView() {
        if (!AtlantisUtil.isAjaxRequest()) {
            iniView();
        }
    }

    protected void iniView() {
        try {
            if (code1 != null && code2 != null && code3 != null) {
                Date fecha = new Date(code3);
                if (fecha.after(new Date())) {
                    usuario = atlantisService.buscarUsuario(code2);
                    System.out.println("usuario: " + usuario.toString());
                    if (usuario != null && usuario.getId() != null) {
                        if (usuario.getId().equals(code1.longValue())) {
                            render = true;
                            if (usuario.getEstado()) {
                                mensaje = "Su usuario ya ha sido Activado";
                            } else {
                                mensaje = "Su usuario ha sido correctamente Activado";
                                usuario.setEstado(Boolean.TRUE);
                                atlantisService.activarUsuario(code3, code2);
                            }
                        } else {
                            mensaje = "Error al Validar los Parametros ingresados!!!";
                        }
                    } else {
                        usuario = new Usuario();
                        mensaje = "Error al Validar los Parametros ingresados!!!";
                    }
                } else {
                    mensaje = "El tiempo para la activación del Usuario ha expirado, realice una nueva solicitud.";
                }
            } else {
                mensaje = "Error al Validar los Parametros ingresados!!!";
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
    }

    public Integer getCode1() {
        return code1;
    }

    public void setCode1(Integer code1) {
        this.code1 = code1;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public Long getCode3() {
        return code3;
    }

    public void setCode3(Long code3) {
        this.code3 = code3;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getRender() {
        return render;
    }

    public void setRender(Boolean render) {
        this.render = render;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
