/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.app.UsuarioSession;
import org.autofreno.model.Rol;
import org.autofreno.model.Usuario;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author ORIGAMI
 */
@Named
@ViewScoped
public class UsuarioMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;
    @Inject
    private UsuarioSession usuarioSession;
    private Usuario usuario;
    private List<Usuario> usuarios;
    private List<Rol> roles;
    private Boolean edit;
    private Long idRol;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        usuarios = atlantisService.findAllUsuarios();
        roles = atlantisService.findAllRoles();
        loadValue();
    }

    public void loadValue() {
        usuario = usuarioSession.getUsuario();
        edit = Boolean.TRUE;
        idRol = usuarioSession.getUsuario().getRol().getId();
    }

    public void loadUsuario(Usuario usuario) {
        this.usuario = usuario;
        edit = Boolean.TRUE;
        if (usuario.getRol() != null) {
            setIdRol(usuario.getRol().getId());
        }
    }

    public void actualizarUsuario() {
        if (idRol != null && edit) {
            if (usuario.getRol() != null) {
                usuario.getRol().setId(idRol);
            } else {
                usuario.setRol(new Rol(idRol));
            }
            System.out.println("usuario " + usuario.getClave());
            Usuario u = atlantisService.editarUsuario(usuario);
            usuarioSession.setUsuario(u);
            if (u != null && u.getId() != null) {
                AtlantisUtil.messageInfo("Los datos se actualizaron Correctamente", "");
                loadData();
            } else {
                AtlantisUtil.messageWarning("Los datos no pudieron ser actualizados", "");
            }
        }
    }

    public void eliminarUsuario(Usuario usuario) {
        this.usuario = atlantisService.eliminarUsuario(usuario);
        if (this.usuario != null && this.usuario.getId() != null) {
            AtlantisUtil.messageInfo("Los datos se eliminaron Correctamente", "");
            loadData();
        } else {
            AtlantisUtil.messageWarning("Los datos no pudieron ser eliminados", "");
        }
    }

//<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public Long getIdRol() {
        return idRol;
    }

    public void setIdRol(Long idRol) {
        this.idRol = idRol;
    }
//</editor-fold>
}
