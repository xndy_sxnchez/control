/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.atlantis.model.Empresa;
import org.atlantis.model.Inventario;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author Andy Sanchez
 */
@Named
@ViewScoped
public class EmpresaMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;

    private Empresa dato;

    private List<Empresa> datos;
    private Boolean editar;
    
     @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
     
        dato = new Empresa();
        datos = atlantisService.findAllEmpresas();
        editar = Boolean.FALSE;
    }

    public void loadDato(Empresa i) {
        dato = i;
        editar = Boolean.TRUE;
    }

    public void guardar() {

        dato = atlantisService.registrarEmpresa(dato);
        if (dato != null && dato.getId() != null) {
            AtlantisUtil.messageInfo("Los datos se grabaron Correctamente", "");
            loadData();
        } else {
            AtlantisUtil.messageWarning("Los datos no pudieron ser grabados Correctamente", "");
        }
    }

    public Empresa getDato() {
        return dato;
    }

    public void setDato(Empresa dato) {
        this.dato = dato;
    }

    public List<Empresa> getDatos() {
        return datos;
    }

    public void setDatos(List<Empresa> datos) {
        this.datos = datos;
    }

    public Boolean getEditar() {
        return editar;
    }

    public void setEditar(Boolean editar) {
        this.editar = editar;
    }
    
    
    
}
