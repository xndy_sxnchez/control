/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.dto.CategoriaDTO;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class DashBoardMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;

    private CategoriaDTO categoria;
    private List<CategoriaDTO> categorias;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        categorias = atlantisService.findAllCategorias();
    }

    public CategoriaDTO getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaDTO categoria) {
        this.categoria = categoria;
    }

    public List<CategoriaDTO> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaDTO> categorias) {
        this.categorias = categorias;
    }

}
