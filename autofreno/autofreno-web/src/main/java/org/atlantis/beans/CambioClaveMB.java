/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.beans;

import java.io.Serializable;
import java.util.Date;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.model.Usuario;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.util.ViewUtil;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class CambioClaveMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;
    @Inject
    private ViewUtil viewUtil;

    private Integer code1;  //Id de Usuario
    private String code2;   //Identificacion de Persona
    private Long code3;     //Fecha limite de validacion

    private Usuario usuario;
    private String mensaje = "";
    private Boolean render = false;

    private String contrasenia;
    private String contraseniaconfirmar;

    public void doPreRenderView() {
        if (!AtlantisUtil.isAjaxRequest()) {
            iniView();
        }
    }

    protected void iniView() {
        try {
            if (code1 != null && code2 != null && code3 != null) {
                Date fecha = new Date(code3);
                if (fecha.after(new Date())) {
                    usuario = atlantisService.buscarUsuario(code2);
                    if (usuario != null) {
                        if (usuario.getId().equals(code1.longValue())) {
                            render = true;
                            mensaje = "Ingrese su nueva contraseña.";
                        } else {
                            mensaje = "Error!!!";
                        }
                    } else {
                        usuario = new Usuario();
                        mensaje = "Error!!!";
                    }
                } else {
                    mensaje = "El tiempo para el cambio de clave ha expirado, realice una nueva solicitud.";
                }
            } else {
                mensaje = "Error!!!";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cambiarClave() {
        try {

            if (contrasenia != null && contraseniaconfirmar != null) {
                if (contrasenia.length() > 4) {
                    if (contrasenia.equals(contraseniaconfirmar)) {
                        if (usuario.getClave().equals(contrasenia)) {
                            AtlantisUtil.messageWarning("La contraseña no puede ser la misma que la Anterior.", "");
                            return;
                        }
                        if (usuario.getId() != null) {
                            usuario.setClave(contrasenia);
                            Usuario response = atlantisService.actualizarContrasenia(usuario);
                            if (response != null) {
                                if (response.getId() != null) {
                                    viewUtil.goToPage("/login.xhtml");
                                }
                            }

                        }
                    } else {
                        AtlantisUtil.messageWarning("La contraseñas no coinciden.", "");
                    }
                } else {
                    AtlantisUtil.messageWarning("La contraseña debe ser mayor o igual a 5 caracteres.", "");
                }
            } else {
                AtlantisUtil.messageWarning("Debe ingresar la nueva contraseña y repetirla.", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Integer getCode1() {
        return code1;
    }

    public void setCode1(Integer code1) {
        this.code1 = code1;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public Long getCode3() {
        return code3;
    }

    public void setCode3(Long code3) {
        this.code3 = code3;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getRender() {
        return render;
    }

    public void setRender(Boolean render) {
        this.render = render;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getContraseniaconfirmar() {
        return contraseniaconfirmar;
    }

    public void setContraseniaconfirmar(String contraseniaconfirmar) {
        this.contraseniaconfirmar = contraseniaconfirmar;
    }

}
