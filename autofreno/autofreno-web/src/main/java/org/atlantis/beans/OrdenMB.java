/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.atlantis.app.ServletSession;
import org.atlantis.model.DatosVehiculo;
import org.atlantis.model.Empresa;
import org.atlantis.model.Orden;
import org.atlantis.model.OrdenDetalle;
import org.autofreno.app.UsuarioSession;
import org.autofreno.dto.CategoriaDTO;
import org.autofreno.model.Persona;
import org.autofreno.model.Usuario;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Andy Sanchez
 */
@Named
@ViewScoped
public class OrdenMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;
    @Inject
    private UsuarioSession usuarioSession;
    @Inject
    private ServletSession ss;

    private Orden dato;
    private Persona persona;
    private String identificacion;
    private OrdenDetalle ordenDetalle;

    private List<Orden> datos;
    private List<OrdenDetalle> detalles;
    private List<CategoriaDTO> categorias;
    private CategoriaDTO categoria;

    private List<Empresa> empresas;
    private List<Usuario> mecanicos;

    private Boolean editar;

    private Long idPersona;
    private Long idMecanico;
    private Integer idEmpresa;

    private Date fecha;

    private BigDecimal subTotal, totalPago;

    private DatosVehiculo datosVehiculo;

    private String placa;

    private Date fechaInicio;
    private Date fechaEntrega;
    
    private Orden ordenInfo;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        fechaEntrega = new Date();
        fechaInicio = new Date();
        idPersona = null;
        idEmpresa = null;
        fecha = new Date();
        dato = new Orden();
        persona = null;
        dato.setFecha(new Date());
        dato.setFechaL(new Date().getTime());
        datos = atlantisService.findAllOrdenes();
        categorias = atlantisService.findAllCategorias();
        empresas = atlantisService.findAllEmpresas();
        mecanicos = atlantisService.findAllUsuarios();
        editar = Boolean.FALSE;
        totalPago = BigDecimal.ZERO;
        subTotal = BigDecimal.ZERO;
        detalles = new ArrayList<>();
        ordenDetalle = new OrdenDetalle();
        datosVehiculo = new DatosVehiculo();
        placa = "";
        idMecanico = null;
    }

    public void loadDato(Orden i) {
        dato = i;
        fecha = dato.getFecha();
        idEmpresa = dato.getEmpresa().getId();
        idPersona = dato.getPersona().getId();
        editar = Boolean.TRUE;
        datosVehiculo = new DatosVehiculo();
    }

    public void agregarCategoria() {

    }

    public void consultarVehiculo() {
        if (identificacion != null && !identificacion.isEmpty()
                && placa != null && !placa.isEmpty()) {
            datosVehiculo = atlantisService.consultaVehiculo(placa, identificacion);
            if (datosVehiculo == null) {
                AtlantisUtil.messageError("No se encontraron datos", "");
                datosVehiculo = new DatosVehiculo();
            }
        } else {
            datosVehiculo = new DatosVehiculo();
            AtlantisUtil.messageError("Deben ingresar la Placa y la identificacion de la persona", "");
        }

    }

    public void guardar() {
        if (idEmpresa == null) {
            AtlantisUtil.messageError("Debe ingresar un Establecimiento", "");
            return;
        }
        if (idMecanico == null && !dato.getTipo().equals("PROFORMA")) {
            AtlantisUtil.messageError("Debe ingresar Asignarle la Solicitud a Alguien", "");
            return;
        }

        if (idPersona == null) {
            AtlantisUtil.messageError("Debe ingresar un Cliente", "");
            return;
        }
        if (AtlantisUtil.isEmpty(detalles)) {
            AtlantisUtil.messageError("Debe ingresar el Detalle de la Factura", "");
            return;
        }

        if (datosVehiculo == null) {
            AtlantisUtil.messageError("Debe ingresar el Vehiculo", "");
            return;
        }

        if (datosVehiculo.getPlaca() == null || datosVehiculo.getPlaca().isEmpty()) {
            AtlantisUtil.messageError("Debe ingresar el Vehiculo", "");
            return;
        }

        if (datosVehiculo != null) {
            dato.setDatosVehiculo(datosVehiculo);
        }
        dato.setFechaEntrega(fechaEntrega.getTime());
        dato.setFechaInicio(fechaInicio.getTime());
        dato.setUsuario(usuarioSession.getUsuario());
        dato.setTotalOrden(totalPago.doubleValue());
        dato.setOrdenDetalles(detalles);
        dato.setFechaL(fecha.getTime());
        dato.setPersona(persona);

        if (idMecanico != null) {
            for (Usuario m : mecanicos) {
                if (m.getId().equals(idMecanico)) {
                    dato.setMecanico(m);
                    break;
                }
            }
        }

        for (Empresa emp : empresas) {
            if (emp.getId().equals(idEmpresa)) {
                dato.setEmpresa(emp);
                break;
            }
        }
        dato = atlantisService.registrarOrden(dato);
        if (dato != null && dato.getId() != null) {
            AtlantisUtil.messageInfo("Los datos se grabaron Correctamente", "");
            generarReporteOrdenPago(dato);
            loadData();
        } else {
            AtlantisUtil.messageWarning("Los datos no pudieron ser grabados Correctamente", "");
        }
    }

    public void buscarCliente() {
        try {
            if (identificacion != null) {
                switch (identificacion.length()) {
                    case 10:
                    case 13:
                        break;
                    default:
                        AtlantisUtil.messageError("Ingrese un numero de Identificación Incorrecto", "");
                        return;
                }
                persona = atlantisService.buscarPersona(identificacion);
                if (dato == null) {
                    AtlantisUtil.messageWarning("No se encontraron Resultados.", "");
                    return;
                }
                idPersona = persona.getId();
            } else {
                AtlantisUtil.messageWarning("Ingrese el número de Identificación para la consulta.", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectRubro() {
        if (validarRubroSeleccionado()) {
            if (dato.getTipo() == null) {
                categoria = null;
                AtlantisUtil.messageError("Debe seleccionar el tipo de Orden", "");
                return;
            }
            if (dato.getTipo().equals("ORDEN DE PAGO") && categoria.getCantidadDiponible() != 0) {
                ordenDetalle.setCategoria(atlantisService.buscarCategoria(categoria.getId()));
                ordenDetalle.setCantidad(1);
                ordenDetalle.setValor(ordenDetalle.getCantidad() * ordenDetalle.getCategoria().getValor());
                AtlantisUtil.executeJS("PF('dlgRubroValor').show()");
            } else {
                if (dato.getTipo().equals("PROFORMA")) {
                    ordenDetalle.setCategoria(atlantisService.buscarCategoria(categoria.getId()));
                    ordenDetalle.setCantidad(1);
                    ordenDetalle.setValor(ordenDetalle.getCantidad() * ordenDetalle.getCategoria().getValor());
                    AtlantisUtil.executeJS("PF('dlgRubroValor').show()");
                } else {
                    AtlantisUtil.messageError("No existen productos en stock", "");
                }
            }
        } else {
            categoria = null;
            AtlantisUtil.messageError("Producto ya fue seleccionado", "");
        }
    }

    private Boolean validarRubroSeleccionado() {
        for (OrdenDetalle det : detalles) {
            if (det.getCategoria().getId().equals(categoria.getId())) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public void calcularRubro() {
        ordenDetalle.setValor(ordenDetalle.getCantidad() * ordenDetalle.getCategoria().getValor());
    }

    public void agregarDetalle() {
        ordenDetalle.setId(new Random(System.currentTimeMillis()).nextInt());
        detalles.add(ordenDetalle);
        subTotal = BigDecimal.ZERO;
        for (OrdenDetalle det : detalles) {
            subTotal = subTotal.add(new BigDecimal(det.getValor()));

        };
        totalPago = subTotal;
        ordenDetalle = new OrdenDetalle();
        categoria = new CategoriaDTO();
    }

    public void removerDetalle(Integer index) {
        detalles.remove(index.intValue());
        subTotal = BigDecimal.ZERO;
        totalPago = BigDecimal.ZERO;
        if (AtlantisUtil.isNotEmpty(detalles)) {
            detalles.forEach((det) -> {
                subTotal = subTotal.add(new BigDecimal(det.getValor()));
            });
            totalPago = subTotal;

        }
    }

    public void generarReporteOrdenPago(Orden orden) {
        try {
            ss.borrarParametros();
            ss.instanciarParametros();
            ss.setTieneDatasource(Boolean.FALSE);
            ss.setNombreDocumento(orden.getNumeroOrden().toString());
            ss.agregarParametro("LOGO", "http://localhost:9080/autofreno/javax.faces.resource/images/logo.png.xhtml?ln=prestige-layout");
            cabeceraValores(orden);
            ss.setNombreReporte("ordenPago");
            ss.setDataSource(orden.getOrdenDetalles());
            AtlantisUtil.redirectNewTab("/autofreno/Documento");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cabeceraValores(Orden orden) {
        ss.agregarParametro("valorTotal", orden.getTotalOrden());
        ss.agregarParametro("RUC", orden.getEmpresa().getRuc());
        ss.agregarParametro("NUM_FACT", orden.getNumeroOrden());
        ss.agregarParametro("FECHA_AUT", new Date());
        ss.agregarParametro("RAZON_SOCIAL", orden.getEmpresa().getNombreComercial());
        ss.agregarParametro("NOM_COMERCIAL", orden.getEmpresa().getNombreComercial());
        ss.agregarParametro("DIR_MATRIZ", orden.getEmpresa().getDireccion());
        ss.agregarParametro("CONT_ESPECIAL", "NO");
        ss.agregarParametro("LLEVA_CONTABILIDAD", "SI");
        ss.agregarParametro("RS_COMPRADOR", orden.getPersona().getPrimerNombreApellido());
        ss.agregarParametro("RUC_COMPRADOR", orden.getPersona().getIdentificacion());
        ss.agregarParametro("FECHA_EMISION", new Date());
        ss.agregarParametro("DIRECCION", orden.getPersona().getDireccion());
        ss.agregarParametro("TELEFONO", orden.getPersona().getTelefono());
        ss.agregarParametro("CORREO", orden.getPersona().getCorreo());

        ss.agregarParametro("TIPO", orden.getTipo());

        ss.agregarParametro("PLACA", orden.getDatosVehiculo().getPlaca());
        ss.agregarParametro("CHASIS", orden.getDatosVehiculo().getChasis());
        ss.agregarParametro("MODELO", orden.getDatosVehiculo().getModelo() + " - " + orden.getDatosVehiculo().getMarca());
        ss.agregarParametro("MOTOR", orden.getDatosVehiculo().getMotor());
    }

    public void onCellEdit(CellEditEvent event) {
        if (usuarioSession.getUsuario().getId().equals(datos.get(event.getRowIndex()).getUsuario().getId())) {
            Orden d = atlantisService.actualizarOrden(datos.get(event.getRowIndex()).getId(), event.getNewValue().toString());
            if (d != null) {
                AtlantisUtil.messageInfo("Datos Actualizados", "");
            } else {
                AtlantisUtil.messageError("Intente nuevamente", "");
            }
        } else {
            AtlantisUtil.messageError("Solo el usuario asignado puede Actualizar el estado", "");
        }

    }
    
    public void loadOrdenInfo(Orden orden){
        System.out.println("orden: " + orden.getAsignado());
        this.ordenInfo = orden;
    }

    public Orden getDato() {
        return dato;
    }

    public void setDato(Orden dato) {
        this.dato = dato;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public OrdenDetalle getOrdenDetalle() {
        return ordenDetalle;
    }

    public void setOrdenDetalle(OrdenDetalle ordenDetalle) {
        this.ordenDetalle = ordenDetalle;
    }

    public List<Orden> getDatos() {
        return datos;
    }

    public void setDatos(List<Orden> datos) {
        this.datos = datos;
    }

    public List<OrdenDetalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<OrdenDetalle> detalles) {
        this.detalles = detalles;
    }

    public List<CategoriaDTO> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaDTO> categorias) {
        this.categorias = categorias;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

    public Boolean getEditar() {
        return editar;
    }

    public void setEditar(Boolean editar) {
        this.editar = editar;
    }

    public Long getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Long idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public CategoriaDTO getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaDTO categoria) {
        this.categoria = categoria;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getTotalPago() {
        return totalPago;
    }

    public void setTotalPago(BigDecimal totalPago) {
        this.totalPago = totalPago;
    }

    public DatosVehiculo getDatosVehiculo() {
        return datosVehiculo;
    }

    public void setDatosVehiculo(DatosVehiculo datosVehiculo) {
        this.datosVehiculo = datosVehiculo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public List<Usuario> getMecanicos() {
        return mecanicos;
    }

    public void setMecanicos(List<Usuario> mecanicos) {
        this.mecanicos = mecanicos;
    }

    public Long getIdMecanico() {
        return idMecanico;
    }

    public void setIdMecanico(Long idMecanico) {
        this.idMecanico = idMecanico;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Orden getOrdenInfo() {
        return ordenInfo;
    }

    public void setOrdenInfo(Orden ordenInfo) {
        this.ordenInfo = ordenInfo;
    }
    
}
