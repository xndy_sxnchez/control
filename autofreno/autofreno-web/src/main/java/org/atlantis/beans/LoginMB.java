/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.beans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.app.UsuarioSession;
import org.autofreno.model.Usuario;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class LoginMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;
    @Inject
    private UsuarioSession usuarioSession;

    private Usuario usuario;

    @PostConstruct
    public void init() {
        usuario = new Usuario();
    }

    public String iniciarSesion() {
        FacesContext context = FacesContext.getCurrentInstance();
        Usuario response = atlantisService.iniciarSesion(usuario);
        if (response != null) {
            if (response.getId() != null) {
                if (response.getEstado()) {

                    usuarioSession.setIsLogged(Boolean.TRUE);
                    response.setClave(usuario.getClave());
                    System.out.println("clave" + usuario.getClave());
                    usuarioSession.setUsuario(response);
                    return "/public/dashboard.xhtml?faces-redirect=true";
                } else {
                    usuarioSession.setUsuario(null);
                    usuarioSession.setIsLogged(Boolean.TRUE);
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario no se encuentra habilitado", ""));
                }
            }
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Contraseña o Usuario Incorrectos"));
        return "login.xhtml";
    }

    public String logout() {
        usuarioSession.setUsuario(null);
        return "/login.xhtml?faces-redirect=true";
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
