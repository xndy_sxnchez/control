/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.beans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.atlantis.model.Empresa;
import org.autofreno.model.Persona;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author Andy Sanchez
 */
@Named
@ViewScoped
public class PersonaMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;

    private Persona dato;
    private String identificacion;

    private List<Persona> datos;
    private Boolean editar;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        identificacion = "";
        dato = new Persona();
        datos = atlantisService.findAllPersonas();
        editar = Boolean.FALSE;
    }

    public void loadDato(Persona i) {
        dato = i;
        identificacion = dato.getIdentificacion();
        editar = Boolean.TRUE;
    }

    public void consultar() {
        try {
            if (identificacion != null) {
                switch (identificacion.length()) {
                    case 10:
                    case 13:
                        break;
                    default:
                        AtlantisUtil.messageError("Ingrese un numero de Identificación Incorrecto", "");
                        return;
                }
                System.out.println("identificacion: " + identificacion);
                dato = atlantisService.buscarPersona(identificacion);
                if (dato == null) {
                    AtlantisUtil.messageWarning("No se encontraron Resultados.", "");
                    return;
                }
                AtlantisUtil.update("mainForm");
            } else {
                AtlantisUtil.messageWarning("Ingrese el número de Identificación para la consulta.", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardar() {

        dato = atlantisService.registrarPersona(dato);
        if (dato != null && dato.getId() != null) {
            AtlantisUtil.messageInfo("Los datos se grabaron Correctamente", "");
            loadData();
        } else {
            AtlantisUtil.messageWarning("Los datos no pudieron ser grabados Correctamente", "");
        }
    }

    public Persona getDato() {
        return dato;
    }

    public void setDato(Persona dato) {
        this.dato = dato;
    }

    public List<Persona> getDatos() {
        return datos;
    }

    public void setDatos(List<Persona> datos) {
        this.datos = datos;
    }

    public Boolean getEditar() {
        return editar;
    }

    public void setEditar(Boolean editar) {
        this.editar = editar;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

}
