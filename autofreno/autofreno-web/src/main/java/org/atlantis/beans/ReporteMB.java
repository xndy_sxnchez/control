/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.beans;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.atlantis.app.ServletSession;
import org.atlantis.model.Inventario;
import org.atlantis.model.Orden;
import org.autofreno.app.UsuarioSession;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.UrlRest;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class ReporteMB implements Serializable {

    @Inject
    private ServletSession ss;
    @Inject
    private UsuarioSession usuarioSession;

    private Integer tipoReporte;
    private String fechaLarga;
    private Date desde;
    private Date hasta;
    private SimpleDateFormat sdf;

    @PostConstruct
    public void initView() {
        iniciarVariables();
    }

    private void iniciarVariables() {
        sdf = new SimpleDateFormat("EEEEE, dd MMMMM yyyy");
        fechaLarga = sdf.format(new Date());
        desde = new Date();
        hasta = new Date();
    }

    public void imprimir(Boolean excel) {
        ss.borrarParametros();
        ss.instanciarParametros();
        ss.setTieneDatasource(Boolean.FALSE);
        ss.agregarParametro("LOGO", "http://localhost:9080/autofreno/javax.faces.resource/images/logo.png.xhtml?ln=prestige-layout");

        ss.agregarParametro("FECHA_AUT", desde);
        ss.agregarParametro("FECHA_EMISION", hasta);
        switch (tipoReporte) {
            case 1:
                 try {
                RestTemplate restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(),
                        usuarioSession.getUsuario().getClave()));
                URI uri = new URI(UrlRest.rOrdenes + "/desde/" + desde.getTime() + "/hasta/" + hasta.getTime());
                Orden[] response = restTemplate.getForObject(uri, Orden[].class);
                ss.setNombreReporte("ordenesPago");
                ss.setDataSource(Arrays.asList(response));
            } catch (URISyntaxException | RestClientException e) {
                e.printStackTrace();
            }
            break;
            case 2:
                 try {
                RestTemplate restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(),
                        usuarioSession.getUsuario().getClave()));
                URI uri = new URI(UrlRest.rInventarios + "/desde/" + desde.getTime() + "/hasta/" + hasta.getTime());
                Inventario[] response = restTemplate.getForObject(uri, Inventario[].class);
                ss.setNombreReporte("inventario");
                ss.setDataSource(Arrays.asList(response));
            } catch (URISyntaxException | RestClientException e) {
                e.printStackTrace();
            }
            break;
        }
        if (!excel) {
            AtlantisUtil.redirectNewTab("/autofreno/Documento");
        } else {
            AtlantisUtil.redirectNewTab("/autofreno/DocumentoExcel");
        }
    }

    public Integer getTipoReporte() {
        return tipoReporte;
    }

    public void setTipoReporte(Integer tipoReporte) {
        this.tipoReporte = tipoReporte;
    }

    public String getFechaLarga() {
        return fechaLarga;
    }

    public void setFechaLarga(String fechaLarga) {
        this.fechaLarga = fechaLarga;
    }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

}
