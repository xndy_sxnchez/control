/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.beans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.model.Persona;
import org.autofreno.model.Usuario;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class OlvidoClaveMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;

    private Usuario usuario;

    private Boolean init, siguiente, finish;

    @PostConstruct
    public void loadInit() {
        init = Boolean.TRUE;
        siguiente = Boolean.FALSE;
        finish = Boolean.FALSE;
        loadUsuario();
    }

    public void loadUsuario() {
        usuario = new Usuario();
        usuario.setPersona(new Persona());
    }

    public void consultar() {
        try {
            if (usuario.getPersona().getIdentificacion() != null) {
                switch (usuario.getPersona().getIdentificacion().length()) {
                    case 10:
                    case 13:
                        break;
                    default:
                        AtlantisUtil.messageError("Ingrese un numero de Identificación Correcto", "");
                        return;
                }
                usuario = atlantisService.buscarUsuario(usuario.getPersona().getIdentificacion());
                if (usuario != null) {
                    if (usuario.getId() != null) {
                        init = Boolean.FALSE;
                        siguiente = Boolean.TRUE;
                    }
                } else {
                    loadUsuario();
                    siguiente = Boolean.FALSE;
                    AtlantisUtil.messageWarning("No existen datos ingresados para la identificacion: " + usuario.getPersona().getIdentificacion(), "");
                }

            } else {
                AtlantisUtil.messageWarning("Ingrese el número de Identificación para la consulta.", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void recuperarClave() {
        try {
            atlantisService.recuperarContrasenia(usuario.getPersona().getCorreo(),
                    usuario.getId(), usuario.getUsuario());
            siguiente = Boolean.FALSE;
            init = Boolean.FALSE;
            finish = Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Boolean getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Boolean siguiente) {
        this.siguiente = siguiente;
    }

    public Boolean getFinish() {
        return finish;
    }

    public void setFinish(Boolean finish) {
        this.finish = finish;
    }

    public Boolean getInit() {
        return init;
    }

    public void setInit(Boolean init) {
        this.init = init;
    }

}
