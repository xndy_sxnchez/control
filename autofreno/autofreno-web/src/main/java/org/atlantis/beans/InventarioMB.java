/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.atlantis.model.Inventario;
import org.autofreno.dto.CategoriaDTO;
import org.autofreno.model.Categoria;
import org.autofreno.model.CategoriaTipo;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;

/**
 *
 * @author Andy Sanchez
 */
@Named
@ViewScoped
public class InventarioMB implements Serializable {

    @Inject
    private AtlantisService atlantisService;

    private Inventario dato;

    private List<Inventario> datos;
    private List<CategoriaDTO> categorias;

    private Boolean editar;

    private Integer idCategoria;

    private Date fecha;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        idCategoria = null;
        fecha = new Date();
        dato = new Inventario();
        datos = atlantisService.findAllInventarios();
        categorias = atlantisService.findAllCategorias();
        editar = Boolean.FALSE;
    }

    public void loadDato(Inventario i) {
        dato = i;
        fecha = dato.getFecha();
        idCategoria = dato.getCategoria().getId();
        editar = Boolean.TRUE;
    }

    public void guardar() {
        dato.setFechaL(fecha.getTime());
        dato.setCategoria(new Categoria(idCategoria));
        dato = atlantisService.registrarInventario(dato);
        if (dato != null && dato.getId() != null) {
            AtlantisUtil.messageInfo("Los datos se grabaron Correctamente", "");
            loadData();
        } else {
            AtlantisUtil.messageWarning("Los datos no pudieron ser grabados Correctamente", "");
        }
    }

    public Inventario getDato() {
        return dato;
    }

    public void setDato(Inventario dato) {
        this.dato = dato;
    }

    public List<Inventario> getDatos() {
        return datos;
    }

    public void setDatos(List<Inventario> datos) {
        this.datos = datos;
    }

    public List<CategoriaDTO> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaDTO> categorias) {
        this.categorias = categorias;
    }

    public Boolean getEditar() {
        return editar;
    }

    public void setEditar(Boolean editar) {
        this.editar = editar;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
