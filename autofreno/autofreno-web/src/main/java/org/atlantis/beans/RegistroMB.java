/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.beans;

import java.io.Serializable;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.autofreno.model.Persona;
import org.autofreno.model.Usuario;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.interfaces.AtlantisService;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class RegistroMB implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(RegistroMB.class.getName());

    @Inject
    private AtlantisService atlantisService;

    private String identificacion, tipoDoc;
    private String contrasenia;
    private String contraseniaconfirmar;
    private Persona persona;
    private String correo;
    private Usuario usuario;
    private Boolean ok = false, readOnlyIdentificacion,
            existePersona, siguiente, existeUsuario, busqueda, validoUsuario;
    private Integer registrar = 0;
    private Date fechaExpedicion;

    private String imagen;

    @PostConstruct
    public void init() {
        busqueda = Boolean.TRUE;
        siguiente = Boolean.FALSE;
        validoUsuario = Boolean.FALSE;
        existePersona = Boolean.FALSE;
        existeUsuario = Boolean.FALSE;
        readOnlyIdentificacion = Boolean.FALSE;
        persona = new Persona();
    }

    public void consultar() {
        try {
            if (identificacion != null) {
                switch (identificacion.length()) {
                    case 10:
                    case 13:
                        break;
                    default:
                        AtlantisUtil.messageError("Ingrese un numero de Identificación Correcto", "");
                        return;
                }
                usuario = atlantisService.buscarUsuario(identificacion);
                if (usuario != null) {
                    if (usuario.getId() != null) {
                        if (usuario.getEstado()) {
                            AtlantisUtil.messageWarning(
                                    "Ya existe un usuario ingresado con esos datos. \n"
                                    + "Si presume que su usuario fue tomado por otra persona porfavor reportar a este correo: \n"
                                    + " emot-servicios@gmail.com", "");
                            return;
                        } else {
                            persona = usuario.getPersona();
                        }
                    }
                }
                if (persona == null || (persona != null && persona.getId() == null)) {
                    persona = atlantisService.buscarPersona(identificacion);
                }
                if (persona == null) {
                    persona = new Persona();
                    existePersona = Boolean.FALSE;
                    AtlantisUtil.messageWarning("No se encontraron Resultados.", "");
                    return;
                }

                if (!persona.getFechaExpedicionLong().equals(fechaExpedicion.getTime())) {
                    existePersona = Boolean.FALSE;
                    AtlantisUtil.messageWarning("Los datos de la Fecha Expedición o Inicio de Actividades son Incorrectos", "");
                    return;
                }
                busqueda = Boolean.FALSE;
                existePersona = Boolean.TRUE;
                AtlantisUtil.update("mainForm");
            } else {
                AtlantisUtil.messageWarning("Ingrese el número de Identificación para la consulta.", "");
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
    }

    public void siguientePaso() {
        busqueda = Boolean.FALSE;
        existePersona = Boolean.FALSE;
        siguiente = Boolean.TRUE;
    }

    public void validarUsuario() {
        existeUsuario = atlantisService.validarUsuario(usuario.getUsuario());
        if (existeUsuario) {
            AtlantisUtil.messageWarning("Usuario se encuentra en uso. Intente con otro usuario.", "");
        } else {
            AtlantisUtil.messageInfo("Usuario se encuentra disponible.", "");
        }
    }

    public void validarUsuarioFronEnd() {
        validarUsuario();
        validoUsuario = Boolean.TRUE;
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            imagen = event.getFile().getFileName();
            usuario.setImagen64(Base64.getEncoder().encodeToString(event.getFile().getContents()));
        } catch (Exception ex) {
            AtlantisUtil.messageWarning("Intente nuevamente", "");
            Logger.getLogger(CategoriaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void registrar() {
        try {
            if (!validoUsuario) {
                validarUsuario();
            }
            if (contrasenia.length() < 4) {
                AtlantisUtil.messageWarning("La clave debe contener mínimo 5 caracteres.", "");
            } else if (!contrasenia.equals(contraseniaconfirmar)) {
                AtlantisUtil.messageWarning("No coinciden las claves ingresadas.", "");
            } else if (existeUsuario) {
                return;
            } else {
                usuario.setClave(contrasenia);
                usuario.setPersona(persona);
                ok = atlantisService.registrarUsuario(usuario);
                if (!ok) {
                    AtlantisUtil.messageError("Intente nuevamente", "");
                } else {
                    busqueda = Boolean.FALSE;
                    existePersona = Boolean.FALSE;
                    siguiente = Boolean.FALSE;
                }
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
    }

    public void buscarUsuarioRegistrado() {
        try {
            if (identificacion != null) {
                if (identificacion.length() == 10 || identificacion.length() == 13) {
                    usuario = atlantisService.buscarUsuario(identificacion);
                    if (usuario == null) {
                        AtlantisUtil.messageWarning("No se encuentra usuario registrado con el número de documento.", "");
                    } else {
                        persona = usuario.getPersona();
                        if (persona == null) {
                            persona = new Persona();
                            AtlantisUtil.messageWarning("Usuario no posee datos registrados.", "");
                        } else {
                            correo = persona.getCorreo();
                            persona.setCorreo(persona.getCorreo().replaceAll("(?<=.{3}).(?=.*@)", "*"));
                            registrar = 1;
                            AtlantisUtil.messageInfo("Se enviará un correo electrónico a su cuenta registrada", "");
                        }

                    }
                } else {
                    AtlantisUtil.messageWarning("El número de cédula ingresado no es válido.", "");
                }
            } else {
                AtlantisUtil.messageWarning("Ingrese el número de cédula para la consulta.", "");
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
    }

    public void recuperarClave() {
        try {
            if (correo != null) {
                //atlantisService.recuperarContrasenia(correo, usuario.getId().longValue(), usuario.getUsuario());
                AtlantisUtil.update("frmEnviarCorreo");
                AtlantisUtil.executeJS("PF('dlgEnviarCorreo').show();");
            } else {
                AtlantisUtil.messageWarning("Usuario no tiene correo válido.", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getMailHtmlRecuperarClave() {
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, 24);
            StringBuilder sb = new StringBuilder();
            // cabecera:
            sb.append("<div> <img src=\"").append("").append("/resources/images/mailhead1.png\" /> </div>");
            sb.append("<h1> Cambio de Clave </h1>");
            // mensaje:
            String mensaje = "Este correo tiene una validez de 24 horas.<br/>"
                    + "Para el cambio de clave hacer clic en el siguiente enlace: "
                    + "<a href=\"" + "/registro/cambio-clave.xhtml?code1=" + "&code2="
                    + "&code3=" + cal.getTimeInMillis() + "\" target=\"_new\">AQUI.</a>";
            sb.append("<p>").append(mensaje).append("</p>");
            // footer:
            sb.append("<p>Dirección: Clodoveo Carrión y Av.Orillas del Zamora <br/>");
            sb.append("Teléfono: +593073702350.</P>");
            return sb.toString();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
            return null;
        }
    }

    public Integer getRegistrar() {
        return registrar;
    }

    public void setRegistrar(Integer registrar) {
        this.registrar = registrar;
    }

    public String getCedula() {
        return identificacion;
    }

    public void setCedula(String identificacion) {
        this.identificacion = identificacion;
    }

    public Boolean getReadOnlyIdentificacion() {
        return readOnlyIdentificacion;
    }

    public void setReadOnlyIdentificacion(Boolean readOnlyIdentificacion) {
        this.readOnlyIdentificacion = readOnlyIdentificacion;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public Date getFechaExpedicion() {
        return fechaExpedicion;
    }

    public void setFechaExpedicion(Date fechaExpedicion) {
        this.fechaExpedicion = fechaExpedicion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getContraseniaconfirmar() {
        return contraseniaconfirmar;
    }

    public void setContraseniaconfirmar(String contraseniaconfirmar) {
        this.contraseniaconfirmar = contraseniaconfirmar;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Boolean getExistePersona() {
        return existePersona;
    }

    public void setExistePersona(Boolean existePersona) {
        this.existePersona = existePersona;
    }

    public Boolean getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Boolean siguiente) {
        this.siguiente = siguiente;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Boolean getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(Boolean busqueda) {
        this.busqueda = busqueda;
    }

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    
    
}
