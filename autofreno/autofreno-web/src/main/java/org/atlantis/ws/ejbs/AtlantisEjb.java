/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.ws.ejbs;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.atlantis.model.DatosVehiculo;
import org.atlantis.model.Empresa;
import org.atlantis.model.Inventario;
import org.atlantis.model.Orden;
import org.autofreno.app.UsuarioSession;
import org.autofreno.dto.CategoriaDTO;
import org.autofreno.model.Categoria;
import org.autofreno.model.CategoriaTipo;
import org.autofreno.model.Persona;
import org.autofreno.model.Rol;
import org.autofreno.model.Usuario;
import org.autofreno.util.AtlantisUtil;
import org.autofreno.ws.UrlRest;
import org.autofreno.ws.interfaces.AtlantisService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author gutya
 */
@Singleton(name = "atlantisEjb")
@ApplicationScoped
public class AtlantisEjb implements AtlantisService {

    private static final Logger LOG = Logger.getLogger(AtlantisEjb.class.getName());

    @Inject
    private UsuarioSession usuarioSession;

    private Gson gson;
    private HttpClient httpClient;
    private HttpPost httpPost;
    private HttpPut httpPut;
    private ExecutorService executorService;
    private Future<HttpResponse> futureResponse;
    private HttpResponse httpResponse;
    private Client client;

    private RestTemplate restTemplate;

    public AtlantisEjb() {
        gson = new Gson();
        client = ClientBuilder.newClient();
    }

    @Override
    public Usuario iniciarSesion(Usuario usuario) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rIniciarSesion);
        httpPost.setEntity(new StringEntity(gson.toJson(usuario), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(null, null));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(30, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                usuario = gson.fromJson(sb.toString(), Usuario.class);
                return usuario;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            //  Logger.getLogger(VentanillaWs.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Usuario> findAllUsuarios() {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(UrlRest.userAtlantis,
                    UrlRest.passAtlantis));
            URI uri = new URI(UrlRest.rUsuario);
            Usuario[] response = restTemplate.getForObject(uri, Usuario[].class);
            return Arrays.asList(response);
        } catch (URISyntaxException | RestClientException e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public Boolean registrarUsuario(Usuario usuario) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rRegistrarUsuario);
        httpPost.setEntity(new StringEntity(gson.toJson(usuario), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(null, null));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(30, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                Usuario userResponse = gson.fromJson(sb.toString(), Usuario.class);
                if (userResponse != null) {
                    if (userResponse.getId() != null) {
                        return Boolean.TRUE;
                    }
                }
                return Boolean.FALSE;
            } else {
                return Boolean.FALSE;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Boolean.FALSE;

    }

    @Override
    public void activarUsuario(Long fechaValidacion, String identificacion) {
        restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(null, null));
        String urlActivarUsuarioRP = UrlRest.rActivarUsuario + identificacion + "/fechaValidacion/" + fechaValidacion;
        restTemplate.getForObject(urlActivarUsuarioRP, String.class);
    }

    @Override
    public Usuario actualizarContrasenia(Usuario usuario) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rActualizarContrasenia);
        httpPost.setEntity(new StringEntity(gson.toJson(usuario), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(null, null));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                return gson.fromJson(sb.toString(), Usuario.class);
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void recuperarContrasenia(String emailuser, Long iduser, String username) {
        restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(null, null));
        String urlConsultarTramiteRP = UrlRest.rRecuperarContrasenia + iduser;
        restTemplate.getForObject(urlConsultarTramiteRP, String.class);
    }

    @Override
    public Usuario eliminarUsuario(Usuario u) {
        httpClient = HttpClientBuilder.create().build();
        httpPut = new HttpPut(UrlRest.rEliminarUsuario);
        httpPut.setEntity(new StringEntity(gson.toJson(u), "UTF-8"));
        httpPut.setHeader("Content-type", "application/json; charset=utf-8");
        httpPut.setHeader("Authorization", AtlantisUtil.auth(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPut));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                return gson.fromJson(sb.toString(), Usuario.class);
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Usuario editarUsuario(Usuario usuario) {
        httpClient = HttpClientBuilder.create().build();
        httpPut = new HttpPut(UrlRest.rEditarUsuario);
        httpPut.setEntity(new StringEntity(gson.toJson(usuario), "UTF-8"));
        httpPut.setHeader("Content-type", "application/json; charset=utf-8");
        httpPut.setHeader("Authorization", AtlantisUtil.auth(null, null));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPut));
        try {
            httpResponse = futureResponse.get(30, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                Usuario userResponse = gson.fromJson(sb.toString(), Usuario.class);
                if (userResponse != null) {
                    if (userResponse.getId() != null) {
                        return userResponse;
                    }
                }
                return null;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Usuario buscarUsuario(String identificacion) {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(null, null));
            URI uri = new URI(UrlRest.rBuscarUsuario + identificacion);
            ResponseEntity<Usuario> response = restTemplate.getForEntity(uri, Usuario.class);
            return response.getBody();
        } catch (URISyntaxException | RestClientException e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public Boolean validarUsuario(String nombreUsuario) {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(null, null));
            URI uri = new URI(UrlRest.rValidarUsuario + nombreUsuario);
            Boolean existeUsuario = restTemplate.getForObject(uri, Boolean.class);
            return existeUsuario;
        } catch (URISyntaxException | RestClientException e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return Boolean.FALSE;
    }

    @Override
    public Persona buscarPersona(String identificacion) {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(null, null));
            URI uri = new URI(UrlRest.rBuscarPersona + UrlRest.app + "/identificacion/" + identificacion);
            ResponseEntity<Persona> contribuyente = restTemplate.getForEntity(uri, Persona.class);
            return contribuyente.getBody();
        } catch (URISyntaxException | RestClientException e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public List<Persona> findAllPersonas() {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(null, null));
            URI uri = new URI(UrlRest.rPersonas);
            Persona[] roles = restTemplate.getForObject(uri, Persona[].class);
            return Arrays.asList(roles);

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public Persona registrarPersona(Persona persona) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rRegistrarPersona);
        httpPost.setEntity(new StringEntity(gson.toJson(persona), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                Persona categoriaResponse = gson.fromJson(sb.toString(), Persona.class);
                if (categoriaResponse != null) {
                    if (categoriaResponse.getId() != null) {
                        return categoriaResponse;
                    }
                }
                return null;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Rol> findAllRoles() {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(null, null));
            URI uri = new URI(UrlRest.rRoles);
            Rol[] roles = restTemplate.getForObject(uri, Rol[].class);
            return Arrays.asList(roles);

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public List<CategoriaDTO> findAllCategorias() {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(),
                    usuarioSession.getUsuario().getClave()));
            URI uri = new URI(UrlRest.rCategoria);
            CategoriaDTO[] response = restTemplate.getForObject(uri, CategoriaDTO[].class);
            return Arrays.asList(response);

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public Categoria eliminarCategoria(Categoria categoria) {
        httpClient = HttpClientBuilder.create().build();
        httpPut = new HttpPut(UrlRest.rEliminarCategoria);
        httpPut.setEntity(new StringEntity(gson.toJson(categoria), "UTF-8"));
        httpPut.setHeader("Content-type", "application/json; charset=utf-8");
        httpPut.setHeader("Authorization", AtlantisUtil.auth(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPut));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                return gson.fromJson(sb.toString(), Categoria.class);
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Categoria registrarCategoria(Categoria categoria) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rRegistrarCategoria);
        httpPost.setEntity(new StringEntity(gson.toJson(categoria), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                Categoria categoriaResponse = gson.fromJson(sb.toString(), Categoria.class);
                if (categoriaResponse != null) {
                    if (categoriaResponse.getId() != null) {
                        return categoriaResponse;
                    }
                }
                return null;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Categoria buscarCategoria(Integer idCategoria) {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));
            URI uri = new URI(UrlRest.rIdCategoria + idCategoria);
            ResponseEntity<Categoria> response = restTemplate.getForEntity(uri, Categoria.class);
            return response.getBody();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public List<CategoriaTipo> findAllCategoriaTipos() {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(),
                    usuarioSession.getUsuario().getClave()));
            URI uri = new URI(UrlRest.rCategoriasTipos);
            CategoriaTipo[] response = restTemplate.getForObject(uri, CategoriaTipo[].class);
            return Arrays.asList(response);

        } catch (URISyntaxException | RestClientException e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public CategoriaTipo registrarCategoriaTipo(CategoriaTipo categoria) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rRegistrarCategoriaTipo);
        httpPost.setEntity(new StringEntity(gson.toJson(categoria), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                CategoriaTipo reponse = gson.fromJson(sb.toString(), CategoriaTipo.class);
                if (reponse != null) {
                    if (reponse.getId() != null) {
                        return reponse;
                    }
                }
                return null;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Empresa> findAllEmpresas() {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(),
                    usuarioSession.getUsuario().getClave()));
            URI uri = new URI(UrlRest.rEmpresa);
            Empresa[] response = restTemplate.getForObject(uri, Empresa[].class);
            return Arrays.asList(response);

        } catch (URISyntaxException | RestClientException e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public Empresa registrarEmpresa(Empresa empresa) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rRegistrarEmpresa);
        httpPost.setEntity(new StringEntity(gson.toJson(empresa), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                Empresa reponse = gson.fromJson(sb.toString(), Empresa.class);
                if (reponse != null) {
                    if (reponse.getId() != null) {
                        return reponse;
                    }
                }
                return null;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Inventario> findAllInventarios() {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(),
                    usuarioSession.getUsuario().getClave()));
            URI uri = new URI(UrlRest.rInventarios);
            Inventario[] response = restTemplate.getForObject(uri, Inventario[].class);
            return Arrays.asList(response);

        } catch (URISyntaxException | RestClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Inventario registrarInventario(Inventario inventario) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rRegistrarInventarios);
        httpPost.setEntity(new StringEntity(gson.toJson(inventario), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                Inventario reponse = gson.fromJson(sb.toString(), Inventario.class);
                if (reponse != null) {
                    if (reponse.getId() != null) {
                        return reponse;
                    }
                }
                return null;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Orden> findAllOrdenes() {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(),
                    usuarioSession.getUsuario().getClave()));
            URI uri = new URI(UrlRest.rOrdenes);
            Orden[] response = restTemplate.getForObject(uri, Orden[].class);
            return Arrays.asList(response);

        } catch (URISyntaxException | RestClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Orden registrarOrden(Orden orden) {
        httpClient = HttpClientBuilder.create().build();
        httpPost = new HttpPost(UrlRest.rRegistrarOrden);
        httpPost.setEntity(new StringEntity(gson.toJson(orden), "UTF-8"));
        httpPost.setHeader("Content-type", "application/json; charset=utf-8");
        httpPost.setHeader("Authorization", AtlantisUtil.auth(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));

        executorService = Executors.newSingleThreadExecutor();
        futureResponse = executorService.submit(() -> httpClient.execute(httpPost));
        try {
            httpResponse = futureResponse.get(60, TimeUnit.SECONDS);
            if (httpResponse != null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpResponse.getEntity().getContent()));
                String inputLine;
                StringBuilder sb = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();

                Orden reponse = gson.fromJson(sb.toString(), Orden.class);
                if (reponse != null) {
                    if (reponse.getId() != null) {
                        return reponse;
                    }
                }
                return null;
            } else {
                return null;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException | IOException ex) {
            Logger.getLogger(AtlantisEjb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public DatosVehiculo consultaVehiculo(String placa, String identificacion
    ) {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(usuarioSession.getUsuario().getUsuario(), usuarioSession.getUsuario().getClave()));
            URI uri = new URI(UrlRest.rConsultaVehiculo + placa + "/identificacion/" + identificacion);
            ResponseEntity<DatosVehiculo> response = restTemplate.getForEntity(uri, DatosVehiculo.class);
            return response.getBody();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

    @Override
    public Orden actualizarOrden(Integer orden, String avance) {
        try {
            restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(null, null));
            System.out.println(UrlRest.rActualizarOrden + orden + "/avance/" + avance);
            URI uri = new URI(UrlRest.rActualizarOrden + orden + "/avance/" + avance);
            ResponseEntity<Orden> response = restTemplate.getForEntity(uri, Orden.class);
            return response.getBody();
        } catch (URISyntaxException | RestClientException e) {
            LOG.log(Level.SEVERE, "", e);
        }
        return null;
    }

}
