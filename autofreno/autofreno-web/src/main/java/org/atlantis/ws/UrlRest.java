/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.ws;

/**
 *
 * @author gutya
 */
public class UrlRest {

    public static String app;
    public static String userAtlantis;
    public static String passAtlantis;

    public static String wsApp;

    public static String rIniciarSesion;
    public static String rTerminosCondiciones;
    public static String rValidarUsuario;
    public static String rBuscarUsuario;
    public static String rBuscarPersona;
    public static String rPersonas;
    public static String rRegistrarPersona;
    public static String rRegistrarUsuario;
    public static String rUsuario;
    public static String rActivarUsuario;
    public static String rActualizarContrasenia;
    public static String rRecuperarContrasenia;
    public static String rEditarUsuario;
    public static String rEliminarUsuario;

    //ROLES
    public static String rRoles;

    public static String rCategoriasTipos;
    public static String rRegistrarCategoriaTipo;

    public static String rCategoria;
    public static String rIdCategoria;
    public static String rRegistrarCategoria;
    public static String rEliminarCategoria;

    public static String rEmpresa;
    public static String rRegistrarEmpresa;

    public static String rInventarios;
    public static String rRegistrarInventarios;

    public static String rOrdenes;
    public static String rRegistrarOrden;
    public static String rActualizarOrden;
    
    public static String rConsultaVehiculo;

}
