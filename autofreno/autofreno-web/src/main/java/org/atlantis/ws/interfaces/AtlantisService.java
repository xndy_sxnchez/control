/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.ws.interfaces;

import java.util.List;
import javax.ejb.Local;
import org.atlantis.model.DatosVehiculo;
import org.atlantis.model.Empresa;
import org.atlantis.model.Inventario;
import org.atlantis.model.Orden;
import org.autofreno.dto.CategoriaDTO;
import org.autofreno.model.Categoria;
import org.autofreno.model.CategoriaTipo;
import org.autofreno.model.Persona;
import org.autofreno.model.Rol;
import org.autofreno.model.Usuario;

/**
 *
 * @author gutya
 */
@Local
public interface AtlantisService {

    public Usuario iniciarSesion(Usuario usuario);

    public Usuario buscarUsuario(String identificacion);

    public Boolean validarUsuario(String nombreUsuario);

    public List<Usuario> findAllUsuarios();

    public Boolean registrarUsuario(Usuario usuario);

    public void activarUsuario(Long fechaValidacion, String identificacion);

    public Usuario actualizarContrasenia(Usuario usuario);

    public void recuperarContrasenia(String emailuser, Long iduser, String username);

    public Usuario eliminarUsuario(Usuario u);

    public Usuario editarUsuario(Usuario usuario);
    
    //ROL
    public List<Rol> findAllRoles();

    //PERSONAS
    public Persona buscarPersona(String identificacion);

    public List<Persona> findAllPersonas();

    public Persona registrarPersona(Persona persona);

    //CATEGORIAS TIPOS
    public List<CategoriaTipo> findAllCategoriaTipos();

    public CategoriaTipo registrarCategoriaTipo(CategoriaTipo categoria);

    //CATEGORIAS
    public Categoria eliminarCategoria(Categoria categoria);

    public List<CategoriaDTO> findAllCategorias();

    public Categoria registrarCategoria(Categoria categoria);

    public Categoria buscarCategoria(Integer idCategoria);

    public List<Empresa> findAllEmpresas();

    public Empresa registrarEmpresa(Empresa empresa);

    public List<Inventario> findAllInventarios();

    public Inventario registrarInventario(Inventario inventario);

    public List<Orden> findAllOrdenes();

    public Orden registrarOrden(Orden orden);
    
    public Orden actualizarOrden(Integer orden, String avance);
    
    public DatosVehiculo consultaVehiculo(String placa, String identificacion);

}
