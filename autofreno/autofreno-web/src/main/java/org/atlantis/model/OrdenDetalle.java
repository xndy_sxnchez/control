/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.model;

import org.autofreno.model.Categoria;

/**
 *
 * @author Andy Sanchez
 */
public class OrdenDetalle {

    private Integer id;
    private Orden orden;
    private Categoria categoria;
    private String producto;
    private Integer cantidad;
    private Double valor;
    private Double valorUnitario;

    public OrdenDetalle() {
    }

    public OrdenDetalle(Integer id, Orden orden, Categoria categoria, Integer cantidad) {
        this.id = id;
        this.orden = orden;
        this.categoria = categoria;
        this.cantidad = cantidad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getProducto() {
        if (categoria != null) {
            producto = categoria.getProducto();
        }
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Double getValorUnitario() {
        if (categoria != null) {
            valorUnitario = categoria.getValor();
        }
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

}
