/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.model;

import com.google.gson.annotations.Expose;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.autofreno.model.Persona;
import org.autofreno.model.Usuario;

/**
 *
 * @author Andy Sanchez
 */
public class Orden {

    private Integer id;
    private String estado;
    private String avance;
    private Long fechaEntrega;
    private Long fechaInicio;
    private String observacion;
    private String tipo;
    private Integer numeroOrden;
    private Double totalOrden;
    private Empresa empresa;
    private Persona persona;
    private Usuario usuario;
    private Usuario mecanico;
    private DatosVehiculo datosVehiculo;
    private List<OrdenDetalle> ordenDetalles;
    private Long fechaL;
    @Expose(serialize = false, deserialize = false)
    private Date fecha;
    @Expose(serialize = false, deserialize = false)
    private Date fechaI;
    @Expose(serialize = false, deserialize = false)
    private Date fechaE;
    @Expose(serialize = false, deserialize = false)
    private String cliente;
    @Expose(serialize = false, deserialize = false)
    private String sucursal;
    @Expose(serialize = false, deserialize = false)
    private String asignado;
    @Expose(serialize = false, deserialize = false)
    private String fechaInicioEntrega;

    public Orden(Integer id, String estado, Date fecha, Integer numeroOrden, Double totalOrden, Empresa empresa, Persona persona, Usuario usuario) {
        this.id = id;
        this.estado = estado;
        this.fecha = fecha;
        this.numeroOrden = numeroOrden;
        this.totalOrden = totalOrden;
        this.empresa = empresa;
        this.persona = persona;
        this.usuario = usuario;
    }

    public Orden() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(Integer numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public Double getTotalOrden() {
        return totalOrden;
    }

    public void setTotalOrden(Double totalOrden) {
        this.totalOrden = totalOrden;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Persona getPersona() {
        if (persona == null) {
            persona = new Persona();
        }
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getFechaL() {
        return fechaL;
    }

    public void setFechaL(Long fechaL) {
        this.fechaL = fechaL;
    }

    public Date getFecha() {
        if (fechaL != null) {
            fecha = new Date(fechaL);
        }
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<OrdenDetalle> getOrdenDetalles() {
        return ordenDetalles;
    }

    public void setOrdenDetalles(List<OrdenDetalle> ordenDetalles) {
        this.ordenDetalles = ordenDetalles;
    }

    public String getCliente() {
        if (persona != null) {
            cliente = persona.getIdentificacion() + "-" + persona.getPrimerNombreApellido();
        }
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getSucursal() {
        if (empresa != null) {
            sucursal = empresa.getNombreComercial();
        }
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public DatosVehiculo getDatosVehiculo() {
        return datosVehiculo;
    }

    public void setDatosVehiculo(DatosVehiculo datosVehiculo) {
        this.datosVehiculo = datosVehiculo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Usuario getMecanico() {
        return mecanico;
    }

    public void setMecanico(Usuario mecanico) {
        this.mecanico = mecanico;
    }

    public Long getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Long fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Long getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Long fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getAvance() {
        return avance;
    }

    public void setAvance(String avance) {
        this.avance = avance;
    }

    public Date getFechaI() {
        if (fechaInicio != null) {
            fechaI = new Date(fechaInicio);
        }
        return fechaI;
    }

    public void setFechaI(Date fechaI) {
        this.fechaI = fechaI;
    }

    public Date getFechaE() {
        if (fechaEntrega != null) {
            fechaE = new Date(fechaEntrega);
        }
        return fechaE;
    }

    public void setFechaE(Date fechaE) {
        this.fechaE = fechaE;
    }

    public String getAsignado() {
        return asignado;
    }

    public void setAsignado(String asignado) {
        this.asignado = asignado;
    }

    public String getFechaInicioEntrega() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        fechaInicioEntrega = (getFechaI() != null ? sdf.format(getFechaI()) : "") + " / "
                + (getFechaE() != null ? sdf.format(getFechaE()) : "");
        return fechaInicioEntrega;
    }

    public void setFechaInicioEntrega(String fechaInicioEntrega) {
        this.fechaInicioEntrega = fechaInicioEntrega;
    }

}
