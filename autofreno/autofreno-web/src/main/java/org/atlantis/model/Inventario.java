/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.model;

import com.google.gson.annotations.Expose;
import java.util.Date;
import org.autofreno.model.Categoria;

/**
 *
 * @author Andy Sanchez
 */
public class Inventario {

    private Integer id;
    private Categoria categoria;
    private Integer cantidadInicial;
    private Integer cantidadActual;
    private Long fechaL;
    @Expose(serialize = false, deserialize = false)
    private Date fecha;
    @Expose(serialize = false, deserialize = false)
    private String producto;

    public Inventario() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public Integer getCantidadActual() {
        return cantidadActual;
    }

    public void setCantidadActual(Integer cantidadActual) {
        this.cantidadActual = cantidadActual;
    }

    public Long getFechaL() {
        return fechaL;
    }

    public void setFechaL(Long fechaL) {
        this.fechaL = fechaL;
    }

    public Date getFecha() {
        if (fechaL != null) {
            fecha = new Date(fechaL);
        }
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getProducto() {
        if (categoria != null) {
            producto = categoria.getCategoriaTipo().getCodigo() + "-" + categoria.getProducto();
        }
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

}
