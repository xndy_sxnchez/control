/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.model;

import java.io.Serializable;

/**
 *
 * @author Andy Sanchez
 */
public class Empresa implements Serializable{

    private Integer id;
    private String ruc;
    private String razonSocial;
    private String nombreComercial;
    private String actividadEconomica;
    private String direccion;
    private String correo;
    private String correoClave;
    private String correoHost;
    private String correoPort;
    private String telefono;
    private Boolean estado;
    private String carpetaDocumento;

    public Empresa() {
    }

    public Empresa(Integer id) {
        this.id = id;
    }
    
    
    
    public Empresa(Integer id, String ruc, String razonSocial, String nombreComercial, String actividadEconomica, String direccion, String correo, String correoClave, String correoHost, String correoPort, String telefono, Boolean estado, String carpetaDocumento) {
        this.id = id;
        this.ruc = ruc;
        this.razonSocial = razonSocial;
        this.nombreComercial = nombreComercial;
        this.actividadEconomica = actividadEconomica;
        this.direccion = direccion;
        this.correo = correo;
        this.correoClave = correoClave;
        this.correoHost = correoHost;
        this.correoPort = correoPort;
        this.telefono = telefono;
        this.estado = estado;
        this.carpetaDocumento = carpetaDocumento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getActividadEconomica() {
        return actividadEconomica;
    }

    public void setActividadEconomica(String actividadEconomica) {
        this.actividadEconomica = actividadEconomica;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreoClave() {
        return correoClave;
    }

    public void setCorreoClave(String correoClave) {
        this.correoClave = correoClave;
    }

    public String getCorreoHost() {
        return correoHost;
    }

    public void setCorreoHost(String correoHost) {
        this.correoHost = correoHost;
    }

    public String getCorreoPort() {
        return correoPort;
    }

    public void setCorreoPort(String correoPort) {
        this.correoPort = correoPort;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getCarpetaDocumento() {
        return carpetaDocumento;
    }

    public void setCarpetaDocumento(String carpetaDocumento) {
        this.carpetaDocumento = carpetaDocumento;
    }
    
    
    
}
