/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.autofreno.model;

/**
 *
 * @author gutya
 */
public class Persona {

    private Long id;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private String tipoDocumento;
    private String direccion;
    private String telefono;
    private String celular;
    private String correo;
    private Boolean estado;
    private String estadoCivil;
    private String nombreConyuge;
    private String apellidoConyuge;
    private String correoCodificado;
    //CAMPO PARA LA EXPEDICION DE LA CEDULA E INICIO DE ACTIVIDADES 
    private Long fechaExpedicionLong;
    private Long fechaNacimientoLong;
    private String primerNombreApellido;

    public Persona() {
    }

    public Persona(Long id) {
        this.id = id;
    }

    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNombreConyuge() {
        return nombreConyuge;
    }

    public void setNombreConyuge(String nombreConyuge) {
        this.nombreConyuge = nombreConyuge;
    }

    public String getApellidoConyuge() {
        return apellidoConyuge;
    }

    public void setApellidoConyuge(String apellidoConyuge) {
        this.apellidoConyuge = apellidoConyuge;
    }

    public String getCorreoCodificado() {
        return correoCodificado;
    }

    public void setCorreoCodificado(String correoCodificado) {
        this.correoCodificado = correoCodificado;
    }

    public Long getFechaExpedicionLong() {
        return fechaExpedicionLong;
    }

    public void setFechaExpedicionLong(Long fechaExpedicionLong) {
        this.fechaExpedicionLong = fechaExpedicionLong;
    }

    public Long getFechaNacimientoLong() {
        return fechaNacimientoLong;
    }

    public void setFechaNacimientoLong(Long fechaNacimientoLong) {
        this.fechaNacimientoLong = fechaNacimientoLong;
    }

    @Override
    public String toString() {
        return "Persona{" + "id=" + id + ", identificacion=" + identificacion + ", nombres=" + nombres + ", apellidos=" + apellidos + ", tipoDocumento=" + tipoDocumento + ", direccion=" + direccion + ", telefono=" + telefono + ", celular=" + celular + ", correo=" + correo + ", estado=" + estado + ", estadoCivil=" + estadoCivil + ", nombreConyuge=" + nombreConyuge + ", apellidoConyuge=" + apellidoConyuge + ", correoCodificado=" + correoCodificado + ", fechaExpedicionLong=" + fechaExpedicionLong + ", fechaNacimientoLong=" + fechaNacimientoLong + '}';
    }

    public String getPrimerNombreApellido() {
        return primerNombreApellido;
    }

    public void setPrimerNombreApellido(String primerNombreApellido) {
        this.primerNombreApellido = primerNombreApellido;
    }
    
}
