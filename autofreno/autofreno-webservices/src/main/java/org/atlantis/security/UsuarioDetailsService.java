package org.atlantis.security;

import org.atlantis.entities.Usuario;
import org.atlantis.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.security.core.userdetails.User;

@Component
public class UsuarioDetailsService implements UserDetailsService {


    @Autowired
    private UsuarioRepository usuarioRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Usuario usuario = usuarioRepository.findByUsuario(username);
        if (usuario == null) {
            throw new UsernameNotFoundException(String.format("Usuario no encontrado", username));
        }

        UserDetails userDetails = User.withUsername(usuario.getUsuario())
                .password(usuario.getClave())
                .roles("GROUP_ADMIN").build();

        return userDetails;
    }
}
