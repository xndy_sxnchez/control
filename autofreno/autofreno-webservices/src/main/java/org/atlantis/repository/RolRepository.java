package org.atlantis.repository;

import org.atlantis.entities.Rol;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RolRepository  extends CrudRepository<Rol, Long> {
    List<Rol> findAllById(Long id);
}
