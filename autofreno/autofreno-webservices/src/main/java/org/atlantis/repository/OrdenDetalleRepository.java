/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.repository;

import org.atlantis.entities.Empresa;
import org.atlantis.entities.OrdenDetalle;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Andy Sanchez
 */
public interface OrdenDetalleRepository extends CrudRepository<OrdenDetalle, Integer> {
    
}
