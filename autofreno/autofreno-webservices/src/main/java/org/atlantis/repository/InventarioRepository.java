/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.repository;

import java.util.Date;
import java.util.List;
import org.atlantis.entities.Categoria;
import org.atlantis.entities.Inventario;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Andy Sanchez
 */
public interface InventarioRepository extends CrudRepository<Inventario, Integer>  {
    
    List<Inventario> findAllByCategoria_IdOrderByFechaAsc(Integer categoria);

    List<Inventario> findAllByFechaBetween(Date desde, Date hasta);

}
