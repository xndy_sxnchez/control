package org.atlantis.repository;

import org.atlantis.entities.Empresa;
import org.springframework.data.repository.CrudRepository;

public interface EmpresaRepository extends CrudRepository<Empresa, Integer> {
}
