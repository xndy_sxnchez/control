package org.atlantis.repository;

import org.atlantis.entities.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {

    Persona findByIdentificacion(String identificacion);

}
