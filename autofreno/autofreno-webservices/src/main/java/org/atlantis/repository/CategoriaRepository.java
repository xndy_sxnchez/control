package org.atlantis.repository;

import java.util.List;
import org.atlantis.entities.Categoria;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaRepository extends CrudRepository<Categoria, Integer> {

    List<Categoria> findAllByCategoriaTipo_Id(Integer idCategoriaTipo);
    
    //Categoria findByCategoria_Id(Long idCategoria);
}
