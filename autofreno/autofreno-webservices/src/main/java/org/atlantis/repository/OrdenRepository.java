/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.repository;

import org.atlantis.entities.Empresa;
import org.atlantis.entities.Orden;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Andy Sanchez
 */
public interface OrdenRepository extends CrudRepository<Orden, Integer> {

    @Query(value = "SELECT max(numeroOrden) FROM Orden WHERE empresa.id =?1")
    public Integer max(Integer idEmpresa);

    List<Orden> findAllByFechaBetween(Date desde, Date hasta);

}
