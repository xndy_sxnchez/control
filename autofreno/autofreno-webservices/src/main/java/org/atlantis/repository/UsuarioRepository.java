package org.atlantis.repository;

import org.atlantis.entities.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository  extends CrudRepository<Usuario, Long> {

    Usuario findByUsuario(String usuario);

    Usuario findByPersona_Id(Long idPersona);

    Usuario findByPersona_Identificacion(String identificacion);

    Usuario findByUsuarioAndClaveAndEstado(String usuario, String clave, Boolean estado);

    List<Usuario> findAllByRol_Id(Long rolId);

}
