package org.atlantis.repository;

import org.atlantis.entities.DatosVehiculo;
import org.springframework.data.repository.CrudRepository;

public interface VehiculoRepository extends CrudRepository<DatosVehiculo, Long> {

    DatosVehiculo findByPlaca(String placa);

}
