package org.atlantis.repository;

import org.atlantis.entities.CategoriaTipo;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaTipoRepository extends CrudRepository<CategoriaTipo, Integer> {
}
