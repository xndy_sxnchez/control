package org.atlantis.config;

import org.atlantis.repository.EmpresaRepository;
import org.atlantis.services.async.ANTServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapClientConfig {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("transit.control.tramitePortalWS");
        return marshaller;
    }

    @Bean
    public ANTServices antServices(Jaxb2Marshaller marshaller) {
        ANTServices client = new ANTServices();
        client.setDefaultUri("http://transitcontrol.com:8081/ws-portal/TramitePortalWS");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}