package org.atlantis.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "empresa", schema = "administracion")
public class Empresa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String ruc;
    private String razonSocial;
    private String nombreComercial;
    private String actividadEconomica;
    private String direccion;
    private String correo;
    private String correoClave;
    private String correoHost;
    private String correoPort;
    private String telefono;
    private Boolean estado;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    public Empresa() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getActividadEconomica() {
        return actividadEconomica;
    }

    public void setActividadEconomica(String actividadEconomica) {
        this.actividadEconomica = actividadEconomica;
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
       this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreoClave() {
        return correoClave;
    }

    public void setCorreoClave(String correoClave) {
        this.correoClave = correoClave;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getCorreoPort() {
        return correoPort;
    }

    public void setCorreoPort(String correoPort) {
        this.correoPort = correoPort;
    }

    public String getCorreoHost() {
        return correoHost;
    }

    public void setCorreoHost(String correoHost) {
        this.correoHost = correoHost;
    }

}

