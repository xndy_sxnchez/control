/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.entities;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Andy Sanchez
 */
@Entity
@Table(name = "inventario", schema = "aplicacion")
public class Inventario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @JoinColumn(name = "categoria")
    @ManyToOne(fetch = FetchType.EAGER)
    private Categoria categoria;
    private Integer cantidadInicial;
    private Integer cantidadActual;
    private Date fecha;
    private Date fechaCreacion;
    @Transient
    private Long fechaL;

    public Inventario() {
    }

    public Inventario(Integer id, Categoria categoria, Integer cantidadInicial, Integer cantidadActual, Date fecha, Date fechaCreacion) {
        this.id = id;
        this.categoria = categoria;
        this.cantidadInicial = cantidadInicial;
        this.cantidadActual = cantidadActual;
        this.fecha = fecha;
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public Integer getCantidadActual() {
        return cantidadActual;
    }

    public void setCantidadActual(Integer cantidadActual) {
        this.cantidadActual = cantidadActual;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getFechaL() {
        if (fecha != null) {
            fechaL = fecha.getTime();
        }
        return fechaL;
    }

    public void setFechaL(Long fechaL) {
        this.fechaL = fechaL;
    }

}
