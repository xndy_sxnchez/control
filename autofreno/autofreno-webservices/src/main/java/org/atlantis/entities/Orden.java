/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Andy Sanchez
 */
@Entity
@Table(name = "orden", schema = "aplicacion")
public class Orden {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String estado;
    private String observacion;
    private String avance;
    private String tipo;
    private Long fechaEntrega;
    private Long fechaInicio;
    @JsonIgnore
    private Date fecha;
    @JsonIgnore
    private Date fechaCreacion;
    private Integer numeroOrden;
    private Double totalOrden;
    @JoinColumn(name = "empresa")
    @ManyToOne(fetch = FetchType.EAGER)
    private Empresa empresa;
    @JoinColumn(name = "persona")
    @ManyToOne(fetch = FetchType.EAGER)
    private Persona persona;
    @JoinColumn(name = "usuario")
    @ManyToOne(fetch = FetchType.EAGER)
    private Usuario usuario;
    @JoinColumn(name = "mecanico")
    @ManyToOne(fetch = FetchType.EAGER)
    private Usuario mecanico;
    @ManyToOne(fetch = FetchType.EAGER)
    private DatosVehiculo datosVehiculo;
    @OneToMany(mappedBy = "orden", fetch = FetchType.EAGER)
    private List<OrdenDetalle> ordenDetalles;
    @Transient
    private Long fechaL;

    public Orden(Integer id, String estado, Date fecha, Integer numeroOrden, Double totalOrden, Empresa empresa, Persona persona, Usuario usuario) {
        this.id = id;
        this.estado = estado;
        this.fecha = fecha;
        this.numeroOrden = numeroOrden;
        this.totalOrden = totalOrden;
        this.empresa = empresa;
        this.persona = persona;
        this.usuario = usuario;
    }

    public Orden() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(Integer numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public Double getTotalOrden() {
        return totalOrden;
    }

    public void setTotalOrden(Double totalOrden) {
        this.totalOrden = totalOrden;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<OrdenDetalle> getOrdenDetalles() {
        return ordenDetalles;
    }

    public void setOrdenDetalles(List<OrdenDetalle> ordenDetalles) {
        this.ordenDetalles = ordenDetalles;
    }

    public Long getFechaL() {
        if (fecha != null) {
            fechaL = fecha.getTime();
        }
        return fechaL;
    }

    public void setFechaL(Long fechaL) {
        this.fechaL = fechaL;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public DatosVehiculo getDatosVehiculo() {
        return datosVehiculo;
    }

    public void setDatosVehiculo(DatosVehiculo datosVehiculo) {
        this.datosVehiculo = datosVehiculo;
    }

    public Usuario getMecanico() {
        return mecanico;
    }

    public void setMecanico(Usuario mecanico) {
        this.mecanico = mecanico;
    }

    public Long getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Long fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Long getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Long fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getAvance() {
        return avance;
    }

    public void setAvance(String avance) {
        this.avance = avance;
    }
}
