package org.atlantis.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;

@Entity
@Table(name = "usuarios", schema = "administracion")

public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String usuario;
    private String clave;
    @JsonIgnore
    private byte[] imagen;
    @JsonIgnore
    private Date fechaCreacion;
    private Boolean estado;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "persona")
    private Persona persona;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rol")
    private Rol rol;
    @Transient
    private String imagen64;
    @Transient
    private String urlImagen;

    public Usuario() {

    }

    public Usuario(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getImagen64() {
        return imagen64;
    }

    public void setImagen64(String imagen64) {
        this.imagen64 = imagen64;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", usuario='" + usuario + '\'' +
                ", clave='" + clave + '\'' +
                ", imagen=" + Arrays.toString(imagen) +
                ", fechaCreacion=" + fechaCreacion +
                ", estado=" + estado +
                ", persona=" + persona +
                ", rol=" + rol +
                ", imagen64='" + imagen64 + '\'' +
                ", urlImagen='" + urlImagen + '\'' +
                '}';
    }
}
