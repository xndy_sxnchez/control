package org.atlantis.entities;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "vehiculo", schema = "aplicacion")
public class DatosVehiculo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String idvehiculo;
    private String placa;
    private String chasis;
    private String motor;
    private String marca;
    private String modelo;
    private String identificacion;
    private String propietario;
    private String canton;
    private String avaluo;
    private String tiposervicio;

    public DatosVehiculo() {
    }

    public DatosVehiculo(Long id) {
        this.id = id;
    }

    public DatosVehiculo(String idVehiculo, String placa, String chasis, String motor, String marca, String modelo, String identificacion, String propietario, String canton, String avaluo, String tipoServicio) {
        this.idvehiculo = idVehiculo;
        this.placa = placa;
        this.chasis = chasis;
        this.motor = motor;
        this.marca = marca;
        this.modelo = modelo;
        this.identificacion = identificacion;
        this.propietario = propietario;
        this.canton = canton;
        this.avaluo = avaluo;
        this.tiposervicio = tipoServicio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("idVehiculo")
    public String getIdvehiculo() {
        return idvehiculo;
    }

    public void setIdvehiculo(String idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

    @JsonProperty("tipoServicio")
    public String getTiposervicio() {
        return tiposervicio;
    }

    public void setTiposervicio(String tiposervicio) {
        this.tiposervicio = tiposervicio;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(String avaluo) {
        this.avaluo = avaluo;
    }


}
