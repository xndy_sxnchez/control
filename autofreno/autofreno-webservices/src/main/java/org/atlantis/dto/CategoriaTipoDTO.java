package org.atlantis.dto;

import java.util.List;

public class CategoriaTipoDTO {

    private Integer id;
    private String codigo;
    private String descripcion;
    private Boolean tieneSubCategoria;
    private List<MCategoriaDTO> categorias;

    public CategoriaTipoDTO() {
    }

    public CategoriaTipoDTO(Integer id, String codigo, String descripcion, Boolean tieneSubCategoria, List<MCategoriaDTO> categorias) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.tieneSubCategoria = tieneSubCategoria;
        this.categorias = categorias;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getTieneSubCategoria() {
        return tieneSubCategoria;
    }

    public void setTieneSubCategoria(Boolean tieneSubCategoria) {
        this.tieneSubCategoria = tieneSubCategoria;
    }

    public List<MCategoriaDTO> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<MCategoriaDTO> categorias) {
        this.categorias = categorias;
    }
}
