package org.atlantis.dto;

import org.atlantis.util.AtlantisUtil;

import java.util.List;

public class WCategoriaDTO {

    private Integer id;
    private Integer cantidadDiponible;
    private String abreviatura;
    private String producto;
    private String descripcion;
    private Double valor;
    private String tipo;
    private String estado;

    public WCategoriaDTO() {

    }

    public WCategoriaDTO(Integer id, String abreviatura, String producto,
            String descripcion, Double valor, String tipo, String estado, Integer cantidadDiponible) {
        this.id = id;
        this.abreviatura = abreviatura;
        this.producto = producto;
        this.valor = valor;
        this.tipo = tipo;
        this.estado = estado;
        this.cantidadDiponible = cantidadDiponible;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getCantidadDiponible() {
        return cantidadDiponible;
    }

    public void setCantidadDiponible(Integer cantidadDiponible) {
        this.cantidadDiponible = cantidadDiponible;
    }

}
