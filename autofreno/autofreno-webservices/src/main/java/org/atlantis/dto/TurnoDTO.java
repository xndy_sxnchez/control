package org.atlantis.dto;

public class TurnoDTO {

    private Long id;
    private String turno;
    private String turnoEspera;
    private Integer idCategoria;
    private String categoria;
    private Integer idCategoriaTipo;
    private Long idCentroRevision;
    private String centroRevision;
    private Long idHorario;
    private String horario;
    private Long idSolicitante;
    private String solicitante;
    private Long idVehiculo;
    private String vehiculo;
    private Long idUsuario;
    private String usuario;
    private Integer idTipoTurno;


    //VARIALES PARA DAR EL SIGUIENTE TURNO
    private Long idTurnoModuloAnterior;
    private Long idModulo;

    public TurnoDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Long idHorario) {
        this.idHorario = idHorario;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Long getIdSolicitante() {
        return idSolicitante;
    }

    public void setIdSolicitante(Long idSolicitante) {
        this.idSolicitante = idSolicitante;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public Long getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Long idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(String vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Long getIdCentroRevision() {
        return idCentroRevision;
    }

    public void setIdCentroRevision(Long idCentroRevision) {
        this.idCentroRevision = idCentroRevision;
    }

    public String getCentroRevision() {
        return centroRevision;
    }

    public void setCentroRevision(String centroRevision) {
        this.centroRevision = centroRevision;
    }

    public Integer getIdTipoTurno() {
        return idTipoTurno;
    }

    public void setIdTipoTurno(Integer idTipoTurno) {
        this.idTipoTurno = idTipoTurno;
    }

    public Integer getIdCategoriaTipo() {
        return idCategoriaTipo;
    }

    public void setIdCategoriaTipo(Integer idCategoriaTipo) {
        this.idCategoriaTipo = idCategoriaTipo;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getTurnoEspera() {
        return turnoEspera;
    }

    public void setTurnoEspera(String turnoEspera) {
        this.turnoEspera = turnoEspera;
    }

    public Long getIdTurnoModuloAnterior() {
        return idTurnoModuloAnterior;
    }

    public void setIdTurnoModuloAnterior(Long idTurnoModuloAnterior) {
        this.idTurnoModuloAnterior = idTurnoModuloAnterior;
    }

    public Long getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(Long idModulo) {
        this.idModulo = idModulo;
    }
}

