package org.atlantis.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.atlantis.entities.DatosVehiculo;

import java.util.List;

public class AntTramite {

    private String codigoerror;
    private String mensajeerror;
    private DatosVehiculo datosvehiculo;


    public AntTramite() {
    }

    @JsonProperty("codigoError")
    public String getCodigoerror() {
        return codigoerror;
    }

    public void setCodigoerror(String codigoerror) {
        this.codigoerror = codigoerror;
    }

    @JsonProperty("mensajeError")
    public String getMensajeerror() {
        return mensajeerror;
    }

    public void setMensajeerror(String mensajeerror) {
        this.mensajeerror = mensajeerror;
    }

    @JsonProperty("datosVehiculo")
    public DatosVehiculo getDatosvehiculo() {
        return datosvehiculo;
    }

    public void setDatosvehiculo(DatosVehiculo datosvehiculo) {
        this.datosvehiculo = datosvehiculo;
    }
}

