package org.atlantis.dto;

public class TurnoModuloDTO {

    private Long idTurno;
    private Long turno;
    private Integer idModulo;
    private Integer modulo;

    public TurnoModuloDTO(Long idTurno, Long turno, Integer idModulo, Integer modulo) {
        this.idTurno = idTurno;
        this.turno = turno;
        this.idModulo = idModulo;
        this.modulo = modulo;
    }

    public Long getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Long idTurno) {
        this.idTurno = idTurno;
    }

    public Long getTurno() {
        return turno;
    }

    public void setTurno(Long turno) {
        this.turno = turno;
    }

    public Integer getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(Integer idModulo) {
        this.idModulo = idModulo;
    }

    public Integer getModulo() {
        return modulo;
    }

    public void setModulo(Integer modulo) {
        this.modulo = modulo;
    }
}
