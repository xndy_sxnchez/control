package org.atlantis.services.async;


import com.google.gson.Gson;
import org.atlantis.dto.AntTramite;
import org.atlantis.entities.DatosVehiculo;
import org.atlantis.repository.VehiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import javax.xml.bind.JAXBElement;

@Service
public class ANTServices extends WebServiceGatewaySupport {

    @Autowired
    private VehiculoRepository vehiculoRepository;

    public AntTramite findVehiculo(String placa, String identificacionPropietario) {

        transit.control.tramitePortalWS.ConsultaVehiculo request = new transit.control.tramitePortalWS.ConsultaVehiculo();
        request.setPlaca(placa);
        request.setIdentificacion(identificacionPropietario);
        request.setCodigoservicio("RMA");
        System.out.println("Requesting");

        JAXBElement<transit.control.tramitePortalWS.ConsultaVehiculoResponse> response = (JAXBElement<transit.control.tramitePortalWS.ConsultaVehiculoResponse>)
                getWebServiceTemplate()
                        .marshalSendAndReceive(request);
        Gson gson = new Gson();

        char quotes = '"';

        String antTramiteJSON = response.getValue().getResultado().replace("\\", "");

        antTramiteJSON = antTramiteJSON.replace(quotes + "{", "{");
        antTramiteJSON = antTramiteJSON.replace(quotes + "[", "[");
        antTramiteJSON = antTramiteJSON.replace(quotes + "]", "]");
        antTramiteJSON = antTramiteJSON.replace("]" + quotes, "]");
        antTramiteJSON = antTramiteJSON.replace("}" + quotes + ",", "},");
        antTramiteJSON = antTramiteJSON.replace("}" + quotes + "}", "}}");


        /*antTramiteJSON = antTramiteJSON.replace("datostramite" + quotes + ":" + quotes + "{",
                "datostramite" + quotes + ": {");

        antTramiteJSON = antTramiteJSON.replace("}" + quotes + "," + quotes + "datosvehiculo" + quotes + ":" + quotes + "{",
                "} , " + quotes + "datosvehiculo" + quotes + ": {");

        antTramiteJSON = antTramiteJSON.replace(quotes + "}" + quotes + "   }",
                "" + quotes + "} }");*/

        AntTramite antTramite = gson.fromJson(antTramiteJSON, AntTramite.class);
        DatosVehiculo datosVehiculo = antTramite.getDatosvehiculo();
        if(datosVehiculo != null){
            DatosVehiculo datosVehiculoBD = vehiculoRepository.findByPlaca(datosVehiculo.getPlaca());
            if (datosVehiculoBD != null) {
                datosVehiculo.setId(datosVehiculoBD.getId());
            }
            vehiculoRepository.save(datosVehiculo);
        }


        System.out.println("response.getValue().getResultado() " + antTramiteJSON);
        return antTramite;
    }


}
