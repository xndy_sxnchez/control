package org.atlantis.services.async;

import org.atlantis.entities.Empresa;
import org.atlantis.repository.EmpresaRepository;
import org.atlantis.util.SisVars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.Date;

@Service
public class AppServices implements Serializable {

    @Autowired
    Environment environment;
    @Autowired
    private EmpresaRepository empresaRepository;
    @Autowired
    private EmailServices emailServices;

    public String generarQR(String turno) {
        return "";
    }

    public Empresa getEmpresa() {
        return empresaRepository.findById(1).get();
    }

    public String ipServer() {
        return "http://" + InetAddress.getLoopbackAddress().getHostAddress() + ":" + environment.getProperty("server" +
                ".port") + "/atlantis/api/";
    }

    public String urlImagen(Long id, String tipo) {
        return ipServer() + "imagen/tipo/" + tipo + "/id/" + id;
    }

    public String urlImagenDefault() {
        return "http://localhost:9080/atlantis/javax.faces.resource/images/avatar/avatar-ava.jpg.xhtml?ln=prestige-layout";
    }

    private String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    public void enviarCorreo(String destinatario, String asunto, String mensaje) {
        emailServices.enviarCorreo(destinatario, asunto, mensaje);
    }

    @Async
    public void copyFileServer(String archivo, byte[] byteArchivo) {
        Date d = new Date();
        File file = new File(archivo);
        try {

            OutputStream os = new FileOutputStream(file);
            os.write(byteArchivo);
            System.out.println("Write bytes to file.");
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
