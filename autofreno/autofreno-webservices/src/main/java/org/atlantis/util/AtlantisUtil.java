package org.atlantis.util;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.atlantis.entities.Empresa;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

public class AtlantisUtil {

    public static HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory(String user, String pass) {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setHttpClient(httpClient(user, pass));
        return clientHttpRequestFactory;
    }

    public static HttpClient httpClient(String user, String pass) {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(user, pass));
        HttpClient client1 = HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();
        return client1;
    }

    public static String mailHtmlRecuperarClave(Long idUser, String identificacion, Empresa empresa) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, 24);
            StringBuilder sb = new StringBuilder();
            // cabecera:
            //sb.append("<div> <img src=\"").append(empresa.getLogo()).append("  /> </div>");
            sb.append("<h1> Cambio de Clave </h1>");
            // mensaje:
            String mensaje = "Este correo tiene una validez de 24 horas.<br/>"
                    + "Para el cambio de clave hacer clic en el siguiente enlace: "
                    + "<a href=\"" + "http://127.0.0.1:8081/atlantis/" + "registro/cambioClave.xhtml?code1=" + idUser +
                    "&code2="
                    + identificacion + "&code3=" + cal.getTimeInMillis() + "\" target=\"_new\">AQUI.</a>";
            sb.append("<p>").append(mensaje).append("</p>");
            // footer:
            sb.append("<p> No responda a este correo. Es informativo  <br/>");
            sb.append("<p> Dirección: " + empresa.getDireccion() + " <br/>");
            sb.append("Teléfono:" + empresa.getTelefono() + "</P>");
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String htmlAprobarUsuario(Long idUsuario, String usuario, Empresa empresa) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, 24);
            StringBuilder sb = new StringBuilder();
            // cabecera:
          //  sb.append("<div> <img src=\"").append(empresa.getLogo()).append("/> </div>");
            sb.append("<h1> Activación de Usuario </h1>");
            // mensaje:
            String mensaje = "Este correo tiene una validez de 24 horas.<br/>"
                    + "Para la activación del Usuario hacer clic en el siguiente enlace: "
                    + "<a href=\"" + "http://127.0.0.1:8081/atlantis/" + "registro/activarUsuario.xhtml?code1=&code2="
                    + usuario + "&code3=" + cal.getTimeInMillis() + "\" target=\"_new\">AQUI.</a>";
            sb.append("<p>").append(mensaje).append("</p>");
            // footer:
            sb.append("<p> No responda a este correo. Es informativo  <br/>");
            sb.append("<p> Dirección: " + empresa.getDireccion() + " <br/>");
            sb.append("Teléfono:" + empresa.getTelefono() + "</P>");
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] base64ToByte(String base64) {
        return Base64.getDecoder().decode(base64);
    }

    public static String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    public static boolean isEmpty(Collection l) {
        return l == null || l.isEmpty();
    }

    public static boolean isNotEmpty(Collection l) {
        return !isEmpty(l);
    }

    private Date restarFechas(Date dateInicio, Date dateFinal) {
        long milliseconds = dateFinal.getTime() - dateInicio.getTime();
        int seconds = (int) (milliseconds / 1000) % 60;
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.SECOND, seconds);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.HOUR_OF_DAY, hours);
        return c.getTime();
    }

}
