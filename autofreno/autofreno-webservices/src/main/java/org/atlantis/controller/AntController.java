package org.atlantis.controller;

import org.atlantis.dto.AntTramite;
import org.atlantis.entities.DatosVehiculo;
import org.atlantis.services.async.ANTServices;
import org.atlantis.services.async.AppServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/atlantis/api/")
public class AntController {

    @Autowired
    private ANTServices antServices;

    @RequestMapping(value = "antservice/placa/{placa}/identificacion/{identificacion}", method = RequestMethod.GET)
    public ResponseEntity<DatosVehiculo> consultaVehiculo(@PathVariable(value = "placa") String placa,
                                                          @PathVariable(value = "identificacion") String identificacion) {
        return new ResponseEntity<>(antServices.findVehiculo(placa, identificacion).getDatosvehiculo(), HttpStatus.OK);
    }

}
