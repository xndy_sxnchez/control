package org.atlantis.controller;

import java.util.Date;
import java.util.List;

import com.google.common.collect.Lists;
import org.atlantis.entities.Persona;
import org.atlantis.entities.Rol;
import org.atlantis.entities.Usuario;
import org.atlantis.repository.PersonaRepository;
import org.atlantis.repository.UsuarioRepository;
import org.atlantis.security.MD5PasswordEncoder;
import org.atlantis.services.async.AppServices;
import org.atlantis.util.AtlantisUtil;
import org.atlantis.util.SisVars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/atlantis/api/")
public class UsuarioController {


    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private AppServices appServices;



    @RequestMapping(value = "iniciarSesion", method = RequestMethod.POST)
    public ResponseEntity<?> iniciarSesion(@RequestBody Usuario user) {
        Usuario usuario = usuarioRepository.findByUsuarioAndClaveAndEstado(user.getUsuario(),
                new MD5PasswordEncoder().encode(user.getClave()), Boolean.TRUE);
        if (usuario != null) {
            if (usuario.getImagen() != null)
                usuario.setUrlImagen(appServices.urlImagen(usuario.getId(), "USUARIO"));
            else
                usuario.setUrlImagen(appServices.urlImagenDefault());
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "usuario/guardar", method = RequestMethod.POST)
    public ResponseEntity<?> registrarUsuario(@RequestBody Usuario usuario) {
        try {
            //ACTUALIZA PERSONA
            Persona persona = usuario.getPersona();
            Persona personaBD = personaRepository.findById(persona.getId()).get();
            personaBD.setCelular(persona.getCelular());
            personaBD.setCorreo(persona.getCorreo());
            personaBD.setDireccion(persona.getDireccion());
            personaBD.setTelefono(persona.getTelefono());
            personaRepository.save(personaBD);

            if (usuario.getImagen64() != null) {
                usuario.setImagen(AtlantisUtil.base64ToByte(usuario.getImagen64()));
            }
            if (usuario.getRol() == null) {
                usuario.setRol(new Rol(1L));
            }
            usuario.setEstado(Boolean.FALSE);
            usuario.setClave(new MD5PasswordEncoder().encode(usuario.getClave()));
            usuario.setFechaCreacion(new Date());

            usuario = usuarioRepository.save(usuario);

            return new ResponseEntity<>(usuario, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "usuariosEMOT", method = RequestMethod.GET)
    public ResponseEntity<List<Usuario>> findAllUsuarios() {

        return new ResponseEntity<>(Lists.newArrayList(usuarioRepository.findAll()), HttpStatus.OK);
    }


    @RequestMapping(value = "usuario/eliminar", method = RequestMethod.PUT)
    public ResponseEntity<?> eliminar(@RequestBody Usuario u) {
        try {
            u.setEstado(Boolean.FALSE);
            u = usuarioRepository.save(u);
            return new ResponseEntity<>(u, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "usuario/editar", method = RequestMethod.PUT)
    public ResponseEntity<?> editar(@RequestBody Usuario u) {
        try {
            System.out.println(u.getClave());
            u.setClave(new MD5PasswordEncoder().encode(u.getClave()));
            u = usuarioRepository.save(u);
            return new ResponseEntity<>(u, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "usuario/persona/buscar/identificacion/{identificacion}", method = RequestMethod.GET)
    public ResponseEntity<?> buscarUsuario(@PathVariable String identificacion) {
        try {
            Persona p = personaRepository.findByIdentificacion(identificacion);
            if (p != null) {
                Usuario usuario = usuarioRepository.findByPersona_Id(p.getId());
                if (usuario == null) {
                    usuario = new Usuario();
                }

                if (usuario.getPersona() != null) {
                    usuario.getPersona().setCorreoCodificado(usuario.getPersona().getCorreo().replaceAll("(?<=.{3}).(?=.*@)", "*"));
                    if (usuario.getPersona().getFechaExpedicion() != null) {
                        usuario.getPersona().setFechaExpedicionLong(usuario.getPersona().getFechaExpedicion().getTime());
                    }
                }
                return new ResponseEntity<>(usuario, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new Usuario(), HttpStatus.OK);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "usuario/validar/{nombreUsuario}", method = RequestMethod.GET)
    public ResponseEntity<?> validarUsuario(@PathVariable String nombreUsuario) {
        try {
            Usuario usuario = usuarioRepository.findByUsuario(nombreUsuario);
            if (usuario == null) {
                return new ResponseEntity<>(Boolean.FALSE, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "usuario/activarUsuario/identificacion/{identificacion}/fechaValidacion/{fechaValidacion}",
            method = RequestMethod.GET)
    public ResponseEntity<?> activarUsuario(@PathVariable String identificacion, @PathVariable Long fechaValidacion) {
        try {
            Date fecha = new Date(fechaValidacion);
            Usuario usuario = usuarioRepository.findByPersona_Identificacion(identificacion);
            if (fecha.after(new Date())) {
                if (usuario != null) {
                    usuario.setEstado(Boolean.TRUE);
                    usuarioRepository.save(usuario);
                    return new ResponseEntity<>(usuario, HttpStatus.OK);
                }
            }
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
        }
    }

    @RequestMapping(value = "usuario/recuperar/iduser/{iduser}", method
            = RequestMethod.GET)
    public ResponseEntity<?> recuperarClaveUsuario(@PathVariable Long iduser) {

        try {
            Usuario usuario = usuarioRepository.findById(iduser).get();
            appServices.enviarCorreo(usuario.getPersona().getCorreo(), SisVars.asuntoRecuperarClave,
                    AtlantisUtil.mailHtmlRecuperarClave(iduser,
                            usuario.getPersona().getIdentificacion(), appServices.getEmpresa()));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
    }

    @RequestMapping(value = "usuario/actualizarContrasenia", method = RequestMethod.POST)
    public ResponseEntity<?> actualizarContrasenia(@RequestBody Usuario usuario) {
        if (usuario != null) {
            Usuario usuarioBD = usuarioRepository.findByUsuario(usuario.getUsuario());
            if (usuarioBD != null) {
                usuarioBD.setClave(new MD5PasswordEncoder().encode(usuario.getClave()));
                usuarioRepository.save(usuarioBD);
                return new ResponseEntity<>(usuarioBD, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }


}
