package org.atlantis.controller;

import org.atlantis.config.AtlantisHttpRequestHandler;
import org.atlantis.services.async.AppServices;
import org.atlantis.repository.CategoriaRepository;
import org.atlantis.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/atlantis/api/")
public class MediaController {

    @Autowired
    private AppServices appServices;

    @Autowired
    private CategoriaRepository categoriaRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private AtlantisHttpRequestHandler atlantisHttpRequestHandler;

    @RequestMapping(value = "imagen/tipo/{tipo}/id/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] getImageWithMediaType(@PathVariable String tipo, @PathVariable Integer id) {
        byte[] imagen = null;
        switch (tipo) {
            case "USUARIO":
                imagen = usuarioRepository.findById(id.longValue()).get().getImagen();
                break;
            default:
                break;
        }
        return imagen;
    }

    

}
