/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.controller;

import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import org.atlantis.entities.CategoriaTipo;
import org.atlantis.entities.Empresa;
import org.atlantis.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andy Sanchez
 */
@RestController
@RequestMapping("/atlantis/api/")
public class EmpresaController {

    @Autowired
    private EmpresaRepository empresaRepository;

    @RequestMapping(value = "empresas", method = RequestMethod.GET)
    public ResponseEntity<List<Empresa>> findAllCategoriasTipos() {
        return new ResponseEntity<>(Lists.newArrayList(empresaRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "empresa/guardar", method = RequestMethod.POST)
    public ResponseEntity<?> registrarEmpresa(@RequestBody Empresa empresa) {
        try {
            empresa.setEstado(Boolean.TRUE);
            empresa.setFechaCreacion(new Date());
            empresa = empresaRepository.save(empresa);
            return new ResponseEntity<>(empresa, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

}
