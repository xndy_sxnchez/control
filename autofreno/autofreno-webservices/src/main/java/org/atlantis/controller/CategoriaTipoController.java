package org.atlantis.controller;

import com.google.common.collect.Lists;
import org.atlantis.repository.CategoriaRepository;
import org.atlantis.entities.CategoriaTipo;
import org.atlantis.repository.CategoriaTipoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/atlantis/api/")
public class CategoriaTipoController {


    @Autowired
    private CategoriaTipoRepository categoriaTipoRepository;
    @Autowired
    private CategoriaRepository categoriaRepository;
 
    @RequestMapping(value = "categoriasTipos", method = RequestMethod.GET)
    public ResponseEntity<List<CategoriaTipo>> findAllCategoriasTipos() {
        return new ResponseEntity<>(Lists.newArrayList(categoriaTipoRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "categoriaTipo/guardar", method = RequestMethod.POST)
    public ResponseEntity<?> registrarCategoriaTipo(@RequestBody CategoriaTipo categoriaTipo) {
        try {
            categoriaTipo.setEstado("A");
            categoriaTipo.setFechaCreacion(new Date());
            categoriaTipo = categoriaTipoRepository.save(categoriaTipo);
            return new ResponseEntity<>(categoriaTipo, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

}
