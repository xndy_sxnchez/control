package org.atlantis.controller;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import org.atlantis.dto.WCategoriaDTO;
import org.atlantis.entities.Categoria;
import org.atlantis.entities.Inventario;
import org.atlantis.repository.CategoriaRepository;
import org.atlantis.repository.InventarioRepository;
import org.atlantis.services.async.AppServices;
import org.atlantis.util.AtlantisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/atlantis/api/")
public class CategoriaController {

    @Autowired
    private AppServices appServices;
    @Autowired
    private InventarioRepository inventarioRepository;
    @Autowired
    private CategoriaRepository categoriaRepository;

    private List<WCategoriaDTO> wCategoriaDTOS;
    private WCategoriaDTO wCategoriaDTO;

    @RequestMapping(value = "categoria/id/{idCategoria}", method = RequestMethod.GET)
    public ResponseEntity<?> buscarCategoria(@PathVariable Integer idCategoria) {
        try {
            Categoria categoria = categoriaRepository.findById(idCategoria).get();
            return new ResponseEntity<>(categoria, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "categorias", method = RequestMethod.GET)
    public ResponseEntity<List<WCategoriaDTO>> findAllCategorias() {
        wCategoriaDTOS = new ArrayList<>();
        for (Categoria c : Lists.newArrayList(categoriaRepository.findAll())) {

            Integer totalDisponible = 0;
            List<Inventario> inventarios = inventarioRepository.findAllByCategoria_IdOrderByFechaAsc(c.getId());
            for (Inventario inv : inventarios) {
                totalDisponible = totalDisponible + inv.getCantidadActual();
            }

            wCategoriaDTO = new WCategoriaDTO(c.getId(), c.getAbreviatura(), c.getProducto(),
                    c.getDescripcionBreve(),
                    c.getValor(),
                    c.getCategoriaTipo().getCodigo(),
                    c.getEstado(), totalDisponible);
            wCategoriaDTOS.add(wCategoriaDTO);
        }
        return new ResponseEntity<>(wCategoriaDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/categoria/guardar", method = RequestMethod.POST)
    public ResponseEntity<?> registrarCategoria(@RequestBody Categoria categoria) {
        try {
            categoria.setEstado("A");
            categoria.setFechaCreacion(new Date());
            categoria = categoriaRepository.save(categoria);
            return new ResponseEntity<>(categoria, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "categoria/eliminar", method = RequestMethod.PUT)
    public ResponseEntity<?> eliminarCategoria(@RequestBody Categoria categoria) {
        try {
            categoria.setEstado("I");
            categoria = categoriaRepository.save(categoria);
            return new ResponseEntity<>(categoria, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }
}
