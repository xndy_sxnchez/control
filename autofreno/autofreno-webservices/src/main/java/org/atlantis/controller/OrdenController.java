/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.controller;

import com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;

import org.atlantis.entities.*;
import org.atlantis.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Andy Sanchez
 */
@RestController
@RequestMapping("/atlantis/api/")
public class OrdenController {

    @Autowired
    private OrdenRepository ordenRepository;
    @Autowired
    private OrdenDetalleRepository ordenDetalleRepository;
    @Autowired
    private InventarioRepository inventarioRepository;
    @Autowired
    private VehiculoRepository vehiculoRepository;
    @Autowired
    private PersonaRepository personaRepository;

    @RequestMapping(value = "ordenes", method = RequestMethod.GET)
    public ResponseEntity<List<Orden>> findAllOrdenes() {
        return new ResponseEntity<>(Lists.newArrayList(ordenRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "orden/idOrden/{idOrden}/avance/{avance}", method = RequestMethod.GET)
    public ResponseEntity<?> actualizarOrden(@PathVariable Integer idOrden, @PathVariable String avance) {
        Orden orden = ordenRepository.findById(idOrden).get();
        orden.setAvance(avance);
        ordenRepository.save(orden);
        return new ResponseEntity<>(orden, HttpStatus.OK);
    }

    @RequestMapping(value = "ordenes/desde/{desde}/hasta/{hasta}", method = RequestMethod.GET)
    public ResponseEntity<List<Orden>> findAllOrdenesFecha(@PathVariable Long desde, @PathVariable Long hasta) {
        List<Orden> ordens = ordenRepository.findAllByFechaBetween(new Date(desde), new Date(hasta));
        return new ResponseEntity<>(ordens, HttpStatus.OK);
    }

    @RequestMapping(value = "/orden/guardar", method = RequestMethod.POST)
    public ResponseEntity<?> registrarOrden(@RequestBody Orden orden) {
        try {
            DatosVehiculo datosVehiculo = orden.getDatosVehiculo();
            if (datosVehiculo != null)
                vehiculoRepository.save(datosVehiculo);
            List<OrdenDetalle> detalles = orden.getOrdenDetalles();
            orden.setEstado("A");
            if (orden.getTipo().equals("ORDEN DE PAGO")) {
                orden.setAvance("INICIADO");
            } else {
                orden.setAvance("PROFORMA");
            }

            Persona persona = orden.getPersona();
            persona.setDireccion(persona.getDireccion());
            persona.setTelefono(persona.getTelefono());
            persona.setCorreo(persona.getCorreo());
            personaRepository.save(persona);

            orden.setFecha(new Date());
            orden.setFechaCreacion(new Date());
            Integer numero = ordenRepository.max(orden.getEmpresa().getId());
            if (numero == null) {
                numero = 0;
            }
            orden.setNumeroOrden(numero + 1);
            orden = ordenRepository.save(orden);
            Integer totalDisponible = 0;
            List<Inventario> inventarios;
            for (OrdenDetalle detalle : detalles) {
                detalle.setOrden(orden);
                ordenDetalleRepository.save(detalle);
                if (orden.getTipo().equals("ORDEN DE PAGO")) {
                    inventarios = inventarioRepository.findAllByCategoria_IdOrderByFechaAsc(detalle.getCategoria().getId());
                    for (Inventario inv : inventarios) {
                        if (inv.getCantidadActual() > 0 && !(inv.getCantidadInicial().equals(inv.getCantidadActual()))) {
                            inv.setCantidadActual(inv.getCantidadActual() - detalle.getCantidad());
                            inventarioRepository.save(inv);
                        }
                    }
                }
            }


            return new ResponseEntity<>(orden, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

}
