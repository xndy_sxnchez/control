package org.atlantis.controller;


import com.google.common.collect.Lists;
import org.atlantis.entities.Rol;
import org.atlantis.repository.RolRepository;
import org.atlantis.services.async.AppServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/atlantis/api/")
public class RolController {
    @Autowired
    private AppServices appServices;
    @Autowired
    private RolRepository rolRepository;

    @RequestMapping(value = "roles", method = RequestMethod.GET)
    public ResponseEntity<List<Rol>> findAllRoles() {
        return new ResponseEntity<>(Lists.newArrayList(rolRepository.findAll()), HttpStatus.OK);
    }
}
