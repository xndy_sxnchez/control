/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlantis.controller;

import com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;

import org.atlantis.entities.Categoria;
import org.atlantis.entities.Empresa;
import org.atlantis.entities.Inventario;
import org.atlantis.entities.Orden;
import org.atlantis.repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Andy Sanchez
 */
@RestController
@RequestMapping("/atlantis/api/")
public class InventarioController {

    @Autowired
    private InventarioRepository inventarioRepository;

    @RequestMapping(value = "inventarios", method = RequestMethod.GET)
    public ResponseEntity<List<Inventario>> findAllInventarios() {
        return new ResponseEntity<>(Lists.newArrayList(inventarioRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "inventarios/desde/{desde}/hasta/{hasta}", method = RequestMethod.GET)
    public ResponseEntity<List<Inventario>> findAllInventariosFecha(@PathVariable Long desde,
                                                                    @PathVariable Long hasta) {
        List<Inventario> inventarios = inventarioRepository.findAllByFechaBetween(new Date(desde), new Date(hasta));
        return new ResponseEntity<>(inventarios, HttpStatus.OK);
    }


    @RequestMapping(value = "inventario/guardar", method = RequestMethod.POST)
    public ResponseEntity<?> registrarInventario(@RequestBody Inventario inventario) {
        try {
            if (inventario.getCantidadActual() == null) {
                inventario.setCantidadActual(inventario.getCantidadInicial());
            }
            inventario.setFecha(new Date(inventario.getFechaL()));
            inventario.setFechaCreacion(new Date());
            inventario = inventarioRepository.save(inventario);
            return new ResponseEntity<>(inventario, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "inventario/categoria/id/{idCategoria}", method = RequestMethod.GET)
    public ResponseEntity<?> buscarCategoria(@PathVariable Integer idCategoria) {
        try {
            Integer totalDisponible = 0;
            List<Inventario> inventarios = inventarioRepository.findAllByCategoria_IdOrderByFechaAsc(idCategoria);
            for (Inventario inv : inventarios) {
                totalDisponible = totalDisponible + inv.getCantidadActual();
            }
            return new ResponseEntity<>(totalDisponible, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

}
