package org.atlantis.controller;

import com.google.common.collect.Lists;
import org.atlantis.services.async.AppServices;
import org.atlantis.entities.Persona;
import org.atlantis.repository.PersonaRepository;
import org.atlantis.util.AtlantisUtil;
import org.atlantis.util.SisVars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import org.atlantis.entities.CategoriaTipo;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/atlantis/api/")
public class PersonaController {
    
    @Autowired
    private PersonaRepository personaRepository;
    
    @RequestMapping(value = "persona/aplicacion/{aplicacion}/identificacion/{identificacion}",
            method = RequestMethod.GET)
    public ResponseEntity<Persona> buscarPersona(@PathVariable(value = "aplicacion") String aplicacion,
            @PathVariable(value = "identificacion") String identificacion) {
        try {
            Boolean estaRegistrado;
            Long idPersona = null;
            Persona persona = personaRepository.findByIdentificacion(identificacion);
            if (persona == null) {
                estaRegistrado = Boolean.FALSE;
            } else if (persona.getFechaExpedicion() == null) {
                estaRegistrado = Boolean.FALSE;
                idPersona = persona.getId();
            } else {
                estaRegistrado = Boolean.TRUE;
            }
            if (!estaRegistrado) {
                RestTemplate restTemplate = new RestTemplate(AtlantisUtil.getClientHttpRequestFactory(SisVars.userSgr,
                        SisVars.passSgr));
                URI uri
                        = new URI(SisVars.appUrlConsultarPersona + "SGR" + "/codigoPaquete/" + (identificacion.length() == 10 ? "C" : "J")
                                + "/identificacion/" + identificacion);
                ResponseEntity<Persona> contribuyente = restTemplate.getForEntity(uri, Persona.class);
                if (contribuyente.getBody() != null) {
                    persona = contribuyente.getBody();
                    persona.setIdentificacion(identificacion);
                    persona.setTipoDocumento((identificacion.length() == 10 ? "CÉDULA" : "RUC"));
                    if (persona.getFechaExpedicion() == null) {
                        persona.setFechaExpedicion(new Date(contribuyente.getBody().getFechaExpedicionLong()));
                    }
                    if (persona.getFechaNacimiento() == null) {
                        if (contribuyente.getBody().getFechaNacimientoLong() != null) {
                            persona.setFechaNacimiento(new Date(contribuyente.getBody().getFechaNacimientoLong()));
                        }
                    }
                    if (persona.getTipoDocumento() == null) {
                        persona.setTipoDocumento((identificacion.length() == 10 ? "CÉDULA" : "RUC"));
                    }
                    if (contribuyente.getBody().getCorreo() != null) {
                        persona.setCorreo(contribuyente.getBody().getCorreo());
                    }
                    if (idPersona == null) {
                        persona.setFechaCreacion(new Date());
                    }
                    persona = personaRepository.save(persona);
                    persona.setFechaExpedicionLong(persona.getFechaExpedicion().getTime());
                }
                return new ResponseEntity<>(persona, HttpStatus.OK);
            } else {
                persona.setFechaExpedicionLong(persona.getFechaExpedicion().getTime());
                return new ResponseEntity<>(persona, HttpStatus.OK);
            }
        } catch (URISyntaxException | RestClientException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
    
    @RequestMapping(value = "personas", method = RequestMethod.GET)
    public ResponseEntity<List<Persona>> findAllCategoriasTipos() {
        return new ResponseEntity<>(Lists.newArrayList(personaRepository.findAll()), HttpStatus.OK);
    }
    
    @RequestMapping(value = "persona/guardar", method = RequestMethod.POST)
    public ResponseEntity<?> registrarPersona(@RequestBody Persona persona) {
        try {
            
            Persona personaBD = personaRepository.findById(persona.getId()).get();
            
            personaBD.setDireccion(persona.getDireccion());
            personaBD.setTelefono(persona.getTelefono());
            personaBD.setCorreo(persona.getCorreo());
            personaBD = personaRepository.save(personaBD);
            
            return new ResponseEntity<>(personaBD, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }
    
}
