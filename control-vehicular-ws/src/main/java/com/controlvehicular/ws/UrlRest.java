/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.ws;

/**
 *
 * @author Tech2Go
 */
public class UrlRest {

    public final static String urlServer = "http://127.0.0.1:8795/";

    public final static String apiLogin(String nombreusuario, String contrasenia) {
        return urlServer + "api/inicioSesion/nombreusuario/" + nombreusuario + "/contrasenia/" + contrasenia;
    }

    public static String apiRecintos = urlServer + "/api/recintoselectorales/recintos/";
    public static String apiGuardarRecintos = urlServer + "api/recintoselectorales/saveRecintos/";

    public static String apiUsuarios = urlServer + "/api/usuarios";
    public static String apiGuardarUsuarios = urlServer + "api/usuario/guardar";
    public static String apiEliminarUsuario = urlServer + "api/usuario/eliminar";
    public static String apiEditarUsuario = urlServer + "api/usuario/editar";

    public static String apiRoles = urlServer + "/api/roles";
    public static String apiGuardarRol = urlServer + "api/rol/guardar";
    public static String apiEliminarRol = urlServer + "api/rol/eliminar";
    public static String apiEditarRol = urlServer + "api/rol/editar";

    public static String apiProfesor = urlServer + "/api/profesores";
    public static String apiGuardarProfesor = urlServer + "api/profesor/guardar";
    public static String apiEditarProfesor = urlServer + "api/profesor/editar";
    public static String apiEliminarProfesor = urlServer + "api/profesor/eliminar";

    public static String apiVehiculo = urlServer + "/api/vehiculos";
    public static String apiGuardarVehiculo = urlServer + "api/vehiculo/guardar";
    public static String apiEditarVehiculo = urlServer + "api/vehiculo/editar";
    public static String apiEliminarVehiculo = urlServer + "api/vehiculo/eliminar";

    public static String apiHorario = urlServer + "/api/horarios";
    public static String apiGuardarHorario = urlServer + "api/horario/guardar";
    public static String apiEditarHorario = urlServer + "api/horario/editar";
    public static String apiEliminarHorario = urlServer + "api/horario/eliminar";

    public static String apiModelo = urlServer + "/api/modelos";

    public static String apiVehiculoProfesor = urlServer + "/api/vehiculosProfesores";
    public static String apiGuardarVehiculoProfesor = urlServer + "api/vehiculoProfesor/guardar";
    public static String apiEditarVehiculoProfesor = urlServer + "api/vehiculoProfesor/editar";
    public static String apiEliminarVehiculoProfesor = urlServer + "api/vehiculoProfesor/eliminar";

    public static String apiProfesorHorario = urlServer + "/api/profesoresHorarios";
    public static String apiGuardarProfesorHorario = urlServer + "api/profesorHorario/guardar";
    public static String apiEditarProfesorHorario = urlServer + "api/profesorHorario/editar";
    public static String apiEliminarprofesorHorario = urlServer + "api/profesorHorario/eliminar";

    public static String apiParqueos = urlServer + "api/parqueos";

    public static String apiControlVehicular = urlServer + "api/controlVehicular";
    public static String apiEnviarCorreoControlVehicular = urlServer + "api/controlVehicular/id";
    public static String apiImagenEntradaControlVehicular = urlServer + "api/controlVehicular/imagenEntrada/";
    public static String apiImagenSalidaControlVehicular = urlServer + "api/controlVehicular/imagenSalida/";

}
