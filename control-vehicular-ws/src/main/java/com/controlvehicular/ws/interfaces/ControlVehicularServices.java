/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.ws.interfaces;

import com.controlvehicular.entities.ControlVehicular;
import com.controlvehicular.entities.Horario;
import com.controlvehicular.entities.Modelo;
import com.controlvehicular.entities.Profesor;
import com.controlvehicular.entities.ProfesorHorario;
import com.controlvehicular.entities.Rol;
import com.controlvehicular.entities.Usuario;
import com.controlvehicular.entities.Vehiculo;
import com.controlvehicular.entities.VehiculoProfesor;
import java.util.List;

/**
 *
 * @author Tech2Go
 */
public interface ControlVehicularServices {

    public Usuario login(Usuario usuario);

    public List<Usuario> findAllUsuarios();

    public Usuario saveUsuario(Usuario usuario);

    public Usuario eliminarUsuario(Usuario u);

    public Usuario editarUsuario(Usuario u);

    public List<Rol> findAllRoles();

    public Rol saveRol(Rol rol);

    public Rol eliminarRol(Rol rol);

    public Rol editarRol(Rol rol);

    public List<Profesor> findAllProfesores();

    public Profesor saveProfesor(Profesor dato);

    public Profesor editarProfesor(Profesor dato);

    public void eliminarProfesor(Profesor dato);

    public List<Vehiculo> findAllVehiculos();

    public Vehiculo saveVehiculo(Vehiculo dato);

    public Vehiculo editarVehiculo(Vehiculo dato);

    public void eliminarVehiculo(Vehiculo dato);

    public List<Horario> findAllHorarios();

    public Horario saveHorario(Horario dato);

    public Horario editarHorario(Horario dato);

    public void eliminarHorario(Horario dato);

    public List<Modelo> findAllModelos();

    public List<VehiculoProfesor> findAllVehiculoProfesor();
   
    public VehiculoProfesor saveVehiculoProfesor(VehiculoProfesor dato);

    public void editarVehiculoProfesor(VehiculoProfesor dato);
    
    public void eliminarVehiculoProfesor(VehiculoProfesor dato);

    public List<ProfesorHorario> findAllProfesorHorario();

    public ProfesorHorario saveProfesorHorario(ProfesorHorario dato);
    
    public void editarProfesorHorario(ProfesorHorario dato);

    public void eliminarProfesorHorario(ProfesorHorario dato);
    
    public List<ControlVehicular> findAllControlVehicular();
    
    public List<String> findAllParqueosDisponibles();
    
    public void enviarCorreoControlVehicular(Long idControlVehicular);

}
