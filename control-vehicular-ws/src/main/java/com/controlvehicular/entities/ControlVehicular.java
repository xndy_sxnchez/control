/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;

/**
 *
 * @author gutya
 */
public class ControlVehicular {

    private Long id;
    private String tiempoEstacionado;
    private String profesorHorario;
    private VehiculoProfesor vehiculoProfesor;
    private Long lFecha;
    private Long lHoraIngreso;
    private Long lHoraSalida;
    @JsonIgnore
    private Date fecha;
    @JsonIgnore
    private Date horaIngreso;
    @JsonIgnore
    private Date horaSalida;
    @JsonIgnore
    private Date fechaActual = new Date();

    private String horaIn;
    private String horaSal;
    private List<ProfesorHorario> profesorHorarios;

    public Date getFechaActual() {
        return fechaActual;
    }

    public void setFechaActual(Date fechaActual) {
        this.fechaActual = fechaActual;
    }

    public List<ProfesorHorario> getProfesorHorarios() {
        return profesorHorarios;
    }

    public void setProfesorHorarios(List<ProfesorHorario> profesorHorarios) {
        this.profesorHorarios = profesorHorarios;
    }

    public ControlVehicular() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTiempoEstacionado() {

//if(lHoraSalida == null)
//        Long resta = fechaActual.getTime() - lHoraIngreso;
//        resta = resta / (1000 * 60);
//        tiempoEstacionado = resta.toString();
        return tiempoEstacionado;
    }

    public void setTiempoEstacionado(String tiempoEstacionado) {
        this.tiempoEstacionado = tiempoEstacionado;
    }

    public String getProfesorHorario() {
        return profesorHorario;
    }

    public void setProfesorHorario(String profesorHorario) {
        this.profesorHorario = profesorHorario;
    }

    public VehiculoProfesor getVehiculoProfesor() {
        return vehiculoProfesor;
    }

    public void setVehiculoProfesor(VehiculoProfesor vehiculoProfesor) {
        this.vehiculoProfesor = vehiculoProfesor;
    }

    public Long getlFecha() {
        return lFecha;
    }

    public void setlFecha(Long lFecha) {
        this.lFecha = lFecha;
    }

    public Long getlHoraIngreso() {
        return lHoraIngreso;
    }

    public void setlHoraIngreso(Long lHoraIngreso) {
        this.lHoraIngreso = lHoraIngreso;
    }

    public Long getlHoraSalida() {
        return lHoraSalida;
    }

    public void setlHoraSalida(Long lHoraSalida) {
        this.lHoraSalida = lHoraSalida;
    }

    public Date getFecha() {
        if (lFecha != null) {
            fecha = new Date(lFecha);
        }
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHoraIngreso() {
        if (lHoraIngreso != null) {
            horaIngreso = new Date(lHoraIngreso);
        }
        return horaIngreso;
    }

    public void setHoraIngreso(Date horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public Date getHoraSalida() {
        if (lHoraSalida != null) {
            horaSalida = new Date(lHoraSalida);
        }
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getHoraIn() {
        return horaIn;
    }

    public void setHoraIn(String horaIn) {
        this.horaIn = horaIn;
    }

    public String getHoraSal() {
        return horaSal;
    }

    public void setHoraSal(String horaSal) {
        this.horaSal = horaSal;
    }

}
