/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.entities;

import java.io.Serializable;

public class Horario implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String lunesDesde;
    private String martesDesde;
    private String miercolesDesde;
    private String juevesDesde;
    private String viernesDesde;
    private String sabadoDesde;
    private String domingoDesde;
    private String observacionHorario;
    private String lunesHasta;
    private String martesHasta;
    private String miercolesHasta;
    private String juevesHasta;
    private String viernesHasta;
    private String sabadoHasta;
    private String domingoHasta;

    public Horario() {
    }

    public Horario(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLunesDesde() {
        return lunesDesde;
    }

    public void setLunesDesde(String lunesDesde) {
        this.lunesDesde = lunesDesde;
    }

    public String getMartesDesde() {
        return martesDesde;
    }

    public void setMartesDesde(String martesDesde) {
        this.martesDesde = martesDesde;
    }

    public String getMiercolesDesde() {
        return miercolesDesde;
    }

    public void setMiercolesDesde(String miercolesDesde) {
        this.miercolesDesde = miercolesDesde;
    }

    public String getJuevesDesde() {
        return juevesDesde;
    }

    public void setJuevesDesde(String juevesDesde) {
        this.juevesDesde = juevesDesde;
    }

    public String getViernesDesde() {
        return viernesDesde;
    }

    public void setViernesDesde(String viernesDesde) {
        this.viernesDesde = viernesDesde;
    }

    public String getSabadoDesde() {
        return sabadoDesde;
    }

    public void setSabadoDesde(String sabadoDesde) {
        this.sabadoDesde = sabadoDesde;
    }

    public String getDomingoDesde() {
        return domingoDesde;
    }

    public void setDomingoDesde(String domingoDesde) {
        this.domingoDesde = domingoDesde;
    }

    public String getObservacionHorario() {
        return observacionHorario;
    }

    public void setObservacionHorario(String observacionHorario) {
        this.observacionHorario = observacionHorario;
    }

    public String getLunesHasta() {
        return lunesHasta;
    }

    public void setLunesHasta(String lunesHasta) {
        this.lunesHasta = lunesHasta;
    }

    public String getMartesHasta() {
        return martesHasta;
    }

    public void setMartesHasta(String martesHasta) {
        this.martesHasta = martesHasta;
    }

    public String getMiercolesHasta() {
        return miercolesHasta;
    }

    public void setMiercolesHasta(String miercolesHasta) {
        this.miercolesHasta = miercolesHasta;
    }

    public String getJuevesHasta() {
        return juevesHasta;
    }

    public void setJuevesHasta(String juevesHasta) {
        this.juevesHasta = juevesHasta;
    }

    public String getViernesHasta() {
        return viernesHasta;
    }

    public void setViernesHasta(String viernesHasta) {
        this.viernesHasta = viernesHasta;
    }

    public String getSabadoHasta() {
        return sabadoHasta;
    }

    public void setSabadoHasta(String sabadoHasta) {
        this.sabadoHasta = sabadoHasta;
    }

    public String getDomingoHasta() {
        return domingoHasta;
    }

    public void setDomingoHasta(String domingoHasta) {
        this.domingoHasta = domingoHasta;
    }

    @Override
    public String toString() {
        return "Horario{" + "id=" + id + ", lunesDesde=" + lunesDesde + ", martesDesde=" + martesDesde + ", miercolesDesde=" + miercolesDesde + ", juevesDesde=" + juevesDesde + ", viernesDesde=" + viernesDesde + ", sabadoDesde=" + sabadoDesde + ", domingoDesde=" + domingoDesde + ", observacionHorario=" + observacionHorario + ", lunesHasta=" + lunesHasta + ", martesHasta=" + martesHasta + ", miercolesHasta=" + miercolesHasta + ", juevesHasta=" + juevesHasta + ", viernesHasta=" + viernesHasta + ", sabadoHasta=" + sabadoHasta + ", domingoHasta=" + domingoHasta + '}';
    }


    
}