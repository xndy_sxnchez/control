/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.Modelo;
import com.controlvehicular.entities.Vehiculo;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class VehiculoMB implements Serializable {

    @Inject
    private ControlVehicularServices controlVehicularServices;

    private List<Modelo> modelos;
    private List<Vehiculo> datos;
    private Vehiculo dato;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        modelos = controlVehicularServices.findAllModelos();
        datos = controlVehicularServices.findAllVehiculos();
    }

    public void loadValue() {
        dato = new Vehiculo();

    }

    public void save() {
        controlVehicularServices.saveVehiculo(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Ingreso Correcto", null));
        loadData();
    }

    public void eliminar(Vehiculo r) {
        controlVehicularServices.eliminarVehiculo(r);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
    }

    public void loadEditar(Vehiculo r) {
        dato = r;

    }

    public void editar() {
        controlVehicularServices.editarVehiculo(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public List<Vehiculo> getDatos() {
        return datos;
    }

    public void setDatos(List<Vehiculo> datos) {
        this.datos = datos;
    }

    public Vehiculo getDato() {
        return dato;
    }

    public void setDato(Vehiculo dato) {
        this.dato = dato;
    }

    public List<Modelo> getModelos() {
        return modelos;
    }

    public void setModelos(List<Modelo> modelos) {
        this.modelos = modelos;
    }

}
