/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.Rol;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author gutya
 */
@Named(value = "rolesMB")
@ViewScoped
public class RolesMB implements Serializable {

    @Inject
    private ControlVehicularServices controlVehicularServices;

    private List<Rol> datos;
    private Rol dato;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        datos = controlVehicularServices.findAllRoles();
    }

    public void loadValue() {
        dato = new Rol();

    }

    public void save() {
        controlVehicularServices.saveRol(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Ingreso Correcto", null));
        loadData();
    }

    public void eliminar(Rol r) {
        controlVehicularServices.eliminarRol(r);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
    }

    public void loadEditar(Rol r) {
        dato = r;

    }

    public void editar() {
        controlVehicularServices.editarRol(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public List<Rol> getDatos() {
        return datos;
    }

    public void setDatos(List<Rol> datos) {
        this.datos = datos;
    }

    public Rol getDato() {
        return dato;
    }

    public void setDato(Rol dato) {
        this.dato = dato;
    }

}
