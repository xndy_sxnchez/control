/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

//import com.encuesta.model.Alcalde;
//import com.encuesta.model.Prefectos;
//import com.encuesta.model.VotosAlcaldes;
//import com.encuesta.model.VotosPrefecto;
import com.controlvehicular.ws.UrlRest;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.BarChartSeries;
import org.primefaces.model.chart.BubbleChartModel;
import org.primefaces.model.chart.BubbleChartSeries;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.DonutChartModel;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.LinearAxis;
import org.primefaces.model.chart.MeterGaugeChartModel;
import org.primefaces.model.chart.OhlcChartModel;
import org.primefaces.model.chart.OhlcChartSeries;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Tech2Go
 */
@ManagedBean(name = "estadisticas")
@ViewScoped
public class Estadisticas implements Serializable {

    private Client client;
    
//    private List<VotosAlcaldes> votosAlcaldes;
//    private List<Alcalde> alcaldes;
//    private Long cantidadVotosAlcaldes = 0L;
//    
//    private List<VotosPrefecto> votosPrefectos;
//    private List<Prefectos> prefectos;
//    private Long cantidadVotosPrefecto = 0L;

    private LineChartModel lineModel1;
    private LineChartModel lineModelPrefectos;

    @PostConstruct
    public void init() {

        client = ClientBuilder.newClient();
        
        /*
            ALCALDES
        */
        
//        votosAlcaldes = client.target(UrlRest.apiVotoAlcaldes)
//                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<VotosAlcaldes>>() {
//        });
//        alcaldes = client.target(UrlRest.apiAlcaldes)
//                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Alcalde>>() {
//        });
//        
//        if (votosAlcaldes != null && !votosAlcaldes.isEmpty()) {
//            cantidadVotosAlcaldes = Long.valueOf(votosAlcaldes.size());
//        }

        /*
            PREFECTOS
        */
//        votosPrefectos = client.target(UrlRest.apiVotoPrefectos)
//                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<VotosPrefecto>>() {
//        });
//        prefectos = client.target(UrlRest.apiPrefectos)
//                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Prefectos>>() {
//        });
//        
//        if (votosPrefectos != null && !votosPrefectos.isEmpty()) {
//            cantidadVotosPrefecto = Long.valueOf(votosPrefectos.size());
//        }

        createLineModels();

    }

    public LineChartModel getLineModel1() {
        return lineModel1;
    }

    public LineChartModel getLineModelPrefectos() {
        return lineModelPrefectos;
    }
    
    

    private void createLineModels() {

        lineModel1 = initLinearModelAlcaldes();
        lineModel1.setTitle("Candidatos");
        lineModel1.setLegendPosition("e");
        lineModel1.setShowPointLabels(true);
        lineModel1.getAxes().put(AxisType.X, new CategoryAxis("Candidatos"));
        Axis yAxis = lineModel1.getAxis(AxisType.Y);
        yAxis = lineModel1.getAxis(AxisType.Y);
        yAxis.setLabel("Votos");
        yAxis.setMin(0);
        ///yAxis.setMax(cantidadVotosAlcaldes + 4);
        yAxis.setTickFormat("%'.0f");

        Axis xAxis = lineModel1.getAxis(AxisType.X);
        xAxis = lineModel1.getAxis(AxisType.X);
        xAxis.setTickAngle(-42);
        
        
        
        
        
        
        
        lineModelPrefectos = initLinearModelPrefectos();
        lineModelPrefectos.setTitle("Candidatos");
        lineModelPrefectos.setLegendPosition("e");
        lineModelPrefectos.setShowPointLabels(true);
        lineModelPrefectos.getAxes().put(AxisType.X, new CategoryAxis("Candidatos"));
        Axis yAxisPrefecto = lineModelPrefectos.getAxis(AxisType.Y);
        yAxisPrefecto = lineModelPrefectos.getAxis(AxisType.Y);
        yAxisPrefecto.setLabel("Votos");
        yAxisPrefecto.setMin(0);
        //yAxisPrefecto.setMax(cantidadVotosPrefecto + 4);
        yAxisPrefecto.setTickFormat("%'.0f");

        Axis xAxisPrefecto = lineModelPrefectos.getAxis(AxisType.X);
        xAxisPrefecto = lineModelPrefectos.getAxis(AxisType.X);
        xAxisPrefecto.setTickAngle(-42);
        
        

    }
    
    private LineChartModel initLinearModelAlcaldes() {
        LineChartModel model = new LineChartModel();
        ChartSeries boys = null;
//        for (Alcalde a : alcaldes) {
//            boys = new ChartSeries();
//            boys.setLabel(a.getNombresAlcalde());
//            boys.set(a.getNombresAlcalde() + " " + a.getApellidosAlcalde(), a.getCantidadVotos());
//            model.addSeries(boys);
//        }
        return model;
    }
    
    private LineChartModel initLinearModelPrefectos() {
        LineChartModel model = new LineChartModel();
        ChartSeries boys = null;
//        for (Prefectos a : prefectos) {
//            boys = new ChartSeries();
//            boys.setLabel(a.getNombresPrefecto());
//            boys.set(a.getNombresPrefecto() + " " + a.getApellidosPrefecto(), a.getCantidadVotos());
//            model.addSeries(boys);
//        }
        return model;
    }

}
