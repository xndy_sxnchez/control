/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.Rol;
import com.controlvehicular.entities.Usuario;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Tech2Go
 */
@Named(value = "usuariosMB")
@ViewScoped
public class UsuariosMB implements Serializable {

    @Inject
    private ControlVehicularServices controlVehicularServices;

    private List<Usuario> usuarios;
    private List<Rol> roles;

    private Long idRecinto;
    private Usuario usuario;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        usuarios = controlVehicularServices.findAllUsuarios();
        roles = controlVehicularServices.findAllRoles();
    }

    public void loadValue() {
        usuario = new Usuario();

    }

    public void saveUsuarios() {
        controlVehicularServices.saveUsuario(usuario);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Ingreso Correcto", null));
        loadData();
    }

    public void eliminar(Usuario r) {
        controlVehicularServices.eliminarUsuario(r);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
    }

    public void loadEditar(Usuario r) {
        usuario = r;

    }

    public void editar() {
        controlVehicularServices.editarUsuario(usuario);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public Long getIdRecinto() {
        return idRecinto;
    }

    public void setIdRecinto(Long idRecinto) {
        this.idRecinto = idRecinto;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

}
