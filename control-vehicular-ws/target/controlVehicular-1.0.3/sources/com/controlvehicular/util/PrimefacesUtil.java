/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.util;

import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Tech2Go
 */
public class PrimefacesUtil {
    
    public static void redirectNewTab(String url) { 
        PrimeFaces.current().executeScript("window.open('" + url + "', '_blank');");
    }
    
    
}
