/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.util;


import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.RandomAccessFileOrArray;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 *  
 */
public class Utils {

    private static final int[] PATTERN = {2, 1, 2, 1, 2, 1, 2, 1, 2};
    private static final int[] CASO_9 = {4, 3, 2, 7, 6, 5, 4, 3, 2};
    private static final int[] CASO_6 = {3, 2, 7, 6, 5, 4, 3, 2};
    private static final String NUMERIC_REGEX = "^[0-9]+$";
    private static final String DECIMAL_REGEX = "^[+]?\\d+([.]\\d+)?$";
    private static final String EMAIL_REGEX = "^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)*(\\.[A-Za-z]{1,})$";
    

    public static BigDecimal bigdecimalTo2Decimals(BigDecimal inNumber) {
        String temp = inNumber.toString();
        BigDecimal outNumber;
        int indice = temp.indexOf('.');
        if (((inNumber.toString().length() - 1) - indice) > 2) {
            String tempNew = temp.substring(0, indice + 3);
            outNumber = new BigDecimal(tempNew);
            if (((temp.length()) - (indice + 1)) >= 3) {
                if (Integer.parseInt(temp.substring(tempNew.length(), tempNew.length() + 1)) >= 5) {
                    outNumber = outNumber.add(new BigDecimal("0.01"));
                }
            }
        } else {
            outNumber = inNumber;
        }
        return outNumber;
    }

    public static List<String> separadorComas(String correos) {
        List<String> correosResulList = new ArrayList<>();
        String temp = correos;
        int indice = temp.indexOf(',');
        if (indice > 0) {
            do {
                String correo1 = temp.substring(0, indice);
                correosResulList.add(correo1);
                String correoRestante = temp.substring(indice + 1, temp.length());
                temp = correoRestante;
                indice = temp.indexOf(',');

            } while (indice > 0);
            correosResulList.add(temp);
        } else {
            correosResulList.add(correos);
        }
        return correosResulList;
    }

    public static Boolean isRepetido(Collection<String> val, Object nuevo) {
        boolean i = false;
        for (String x : val) {
            if (x.equals(nuevo)) {
                i = true;
                return i;
            }
        }
        return i;
    }

    public static synchronized boolean validarEmailConExpresion(String email) {
        return validatePattern(EMAIL_REGEX, email);
    }

    public static Long restarFechas(Date fechaActual, Date fechaIngreso) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(fechaActual);
        cal2.setTime(fechaIngreso);
        long milis1 = cal1.getTimeInMillis();
        long milis2 = cal2.getTimeInMillis();
        long diff = milis2 - milis1;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        long diffHour = diff / (60 * 60 * 1000);
        diffHour = diffHour - (diffDays * 24);
        if (diffHour > 0) {
            diffDays++;
        }
        return diffDays;
    }

    public static synchronized boolean validateCCRuc(final String identificacion) {
        if (identificacion == null) {
            return false;
        }
        if (identificacion.trim().isEmpty()) {
            return false;
        }
        if (!validateNumberPattern(identificacion)) {
            return false;
        }
        if (identificacion.length() != 10 & identificacion.length() != 13) {
            return false;
        }
        int[] coeficientes = null;
        int indiceDigitoVerificador = 9;
        int modulo = 11;

        if ((identificacion.length() == 13) && !identificacion.substring(10, 13).equals("001")) {
            return false;
        }
        if (identificacion.charAt(2) == '9') {
            coeficientes = CASO_9;
        } else if (identificacion.charAt(2) == '6') {
            coeficientes = CASO_6;
            indiceDigitoVerificador = 8;
        } else if (identificacion.charAt(2) < '6') {
            coeficientes = PATTERN;
            modulo = 10;
        }
        return verify(identificacion.toCharArray(), coeficientes, indiceDigitoVerificador, modulo);
    }

    private static boolean verify(final char[] array, final int[] coeficientes,
            final int indiceDigitoVerificador, final int modulo) {
        if (coeficientes == null) {
            return false;
        }
        int sum = 0;
        int aux;
        for (int i = 0; i < coeficientes.length; i++) {
            aux = new Integer(String.valueOf(array[i])) * coeficientes[i];
            if ((modulo == 10) && (aux > 9)) {
                aux -= 9;
            }
            sum += aux;
        }
        int mod = sum % modulo;
        mod = mod == 0 ? modulo : mod;
        final int res = (modulo - mod);
        Integer valorVerificar = null;
        if (array.length == 13) {
            valorVerificar = Integer.valueOf(String.valueOf(array[array.length - (13 - indiceDigitoVerificador)]));
        } else if (array.length == 10) {
            valorVerificar = Integer.valueOf(String.valueOf(array[array.length - (10 - indiceDigitoVerificador)]));
        }
        return res == valorVerificar;
    }

    public static synchronized boolean validateNumberPattern(final String valor) {
        return validatePattern(NUMERIC_REGEX, valor);
    }

    public static synchronized boolean validateDecimalPattern(final String valor) {
        return validatePattern(DECIMAL_REGEX, valor);
    }

    public static synchronized boolean validatePattern(final String patron, final String valor) {
        final Pattern patter = Pattern.compile(patron);
        final Matcher matcher = patter.matcher(valor);
        return matcher.matches();
    }

    public static Date sumarRestarDiasFecha(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }

    public static String pasarUtf(String cadena) throws UnsupportedEncodingException {
        if (cadena != null) {
            byte[] bytes = cadena.getBytes("ISO-8859-1");
            cadena = new String(bytes, "UTF-8");
        }
        return cadena;
    }

    public static String randomNumericString() {
        int i = (int) (Math.random() * 100000);
        return String.valueOf(i);
    }

    public static Integer getDateValues(String formatValue, Date value) {
        Integer res = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        f.format(value);
        switch (formatValue.toUpperCase()) {
            case "Y":
                res = f.getCalendar().get(Calendar.YEAR);
                break;
            case "M":
                res = f.getCalendar().get(Calendar.MONTH);
                break;
            case "D":
                res = f.getCalendar().get(Calendar.DAY_OF_MONTH);
                break;
        }
        return res;
    }

    public static String completarCadenaConCeros(String cadena, Integer longitud) {
        if (cadena == null) {
            return null;
        }
        if (cadena.length() > longitud) {
            return cadena;
        }
        String ceros = "";
        for (int i = 0; i < longitud; i++) {
            ceros = ceros + "0";
        }
        int tamanio = cadena.length();
        ceros = ceros.substring(0, longitud - tamanio);
        cadena = ceros + cadena;
        return cadena;
    }

    public static String convertirMesALetra(Integer fechames) {
        String mes;
        switch (fechames) {
            case 1:
                mes = "ENERO";
                break;
            case 2:
                mes = "FEBRERO";
                break;

            case 3:
                mes = "MARZO";
                break;

            case 4:
                mes = "ABRIL";
                break;

            case 5:
                mes = "MAYO";
                break;

            case 6:
                mes = "JUNIO";
                break;

            case 7:
                mes = "JULIO";
                break;

            case 8:
                mes = "AGOSTO";
                break;

            case 9:
                mes = "SEPTIEMBRE";
                break;

            case 10:
                mes = "OCTUBRE";
                break;

            case 11:
                mes = "NOVIEMBRE";
                break;

            default:
                mes = "DICIEMBRE";
        }
        return mes;
    }

    public static String quitarSaltos(String cadena) {
        return cadena.replace("\r", "").replace("\n", "");
    }

    public static int getNumberOfPagesDocumento(byte[] bytes) {
        int ret = -2;
        try {
            RandomAccessFileOrArray pdfFile = new RandomAccessFileOrArray(bytes);
            PdfReader reader = new PdfReader(pdfFile, new byte[0]);
            ret = reader.getNumberOfPages();
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    public static boolean isDecimal(String cad) {
        try {
            Double.parseDouble(cad);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isEmpty(List<?> l) {
        return l == null || l.isEmpty();
    }

    public static boolean isEmpty(Collection l) {
        return l == null || l.isEmpty();
    }

    public static boolean isNotEmpty(List<?> l) {
        return !Utils.isEmpty(l);
    }

    public static boolean isNotEmpty(Collection l) {
        return !Utils.isEmpty(l);
    }

    /**
     * Si el String
     * es nulo
     * returna
     * vacio, caso
     * contrario el
     * mismo valor.
     *
     * @param
     * nombres
     * @return
     */
    public static String isEmpty(String nombres) {
        if (nombres == null || nombres.trim().isEmpty()) {
            return "";
        }
        return nombres;
    }

    /**
     * Verifica que
     * el valor
     * numerico no
     * sea nulo
     * <code>value</code>
     * y retorna el
     * mismo valor
     * de
     * <code>value</code>
     * caso
     * contrario
     * retorna -1.
     *
     * @param <T>
     * @param value
     * Valor a
     * verificar.
     * @return si el
     * valor de
     * <code>value</code>
     * es nulo
     * retorna -1
     * caso
     * contrario el
     * valor de
     * <code>value</code>
     */
    public static <T> T isNull(T value) {
        if (value == null || value.toString().trim().length() < 0) {
            return (T) new BigInteger("-1");
        }
        return (T) value;
    }

    /**
     * Verifica si
     * <code>value</code>
     * es nulo y
     * retorna
     * <code>true</code>,
     * caso
     * contrario
     * retorna
     * <code>false</code>.
     *
     * @param value
     * Tipo de Dato
     * Númerico de
     * cualquier
     * tipo
     * primitivo o
     * objecto.
     * @return True
     * si el null
     * caso
     * contrario
     * false.
     */
    public static Boolean isNumberNull(Number value) {
        if (value == null || value.longValue() < 0L) {
            return true;
        }
        return false;
    }

    public static <T> T get(final List<T> values, int idx) {
        if (values.size() > idx) {
            return values.get(idx);
        }
        return null;
    }

    public static <T> T get(final Collection<T> values, int idx) {
        if (values.size() > idx) {
            List<T> result = new ArrayList<>(values);
            return result.get(idx);
        }
        return null;
    }

    /**
     * Le da formato
     * a la fecha
     * con el
     * pattern que
     * se le pasa
     * como
     * parametro
     *
     * @param
     * pattern
     * Formato que
     * se desea
     * obtener.
     * @param
     * fechaFin
     * Fecha a dar
     * formato.
     * @return Fecha
     * con el
     * formato
     * esperado.
     */
    public static String dateFormatPattern(String pattern, Date fechaFin) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(fechaFin);
    }

    public static String dateFormatPattern(SimpleDateFormat format, Date fecha) {
        return format.format(fecha);
    }

    public static boolean isNum(String nom) {
        try {
            Long.parseLong(nom);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public static Integer getAnio(Date fechaIngreso) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaIngreso);
        return c.get(Calendar.YEAR);
    }

    public static Integer getMes(Date fechaIngreso) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaIngreso);
        return c.get(Calendar.MONTH);
    }

    public static Integer getDia(Date fechaIngreso) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaIngreso);
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static Date getPrimerDiaDelAnio(Integer anio) {
        Calendar fecha = Calendar.getInstance();
        fecha.set(anio, Calendar.JANUARY, 1, 0, 0, 0);
        return fecha.getTime();
    }

    public static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }


    public static int boolean2int(Boolean x) {
        if (x) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *
     * @param <T>
     * @param object
     * Lista de
     * objecto
     * principal
     * @param
     * previousValues
     * lista a
     * retornar
     * @param
     * duplicateArray
     * Lista a
     * comparar si
     * estan
     * repetidos.
     * @param
     * compare 0
     * para realizar
     * comparacion
     * en binario,
     * para hacer la
     * comparacion
     * como texto 1
     * @return List
     */
    public static <T> List<T> verificarRepetidos(final List<T> object, final List<T> previousValues, final List<T> duplicateArray, final int compare) {
        Iterator<T> iterator = object.iterator();
        while (iterator.hasNext()) {
            T next = iterator.next();
            int count = 0;
            for (T t : duplicateArray) {
                if (compare == 0) {
                    if (next.equals(t)) {
                        count++;
                    }
                } else {
                    if (String.valueOf(next).equalsIgnoreCase(String.valueOf(t))) {
                        count++;
                    }
                }
            }
            if (count == 0 && !previousValues.contains(next)) {
                previousValues.add(next);
            }

        }
        return previousValues;
    }

    public static String getRandomUUID() {
        return UUID.randomUUID().toString();
    }


    public static List getList(List asList) {
        if (asList == null) {
            return new ArrayList<>();
        } else {
            return asList;
        }
    }

    public static String retornarVacio(Object valor) {
        if (valor == null) {
            return "vacio";
        }
        if (valor.toString().isEmpty()) {
            return "vacio";
        }
        return valor.toString();
    }

    public static String cargarUrlImagenes(String subReporte) {
        if (subReporte == null) {
            return null;
        }
        try {// \/css/smb/logo.png
            String temp = null;
            if (subReporte.toLowerCase().endsWith("png") || subReporte.toLowerCase().endsWith("jpg")) {
                subReporte = subReporte.replace("//", "/");
                subReporte = subReporte.replace("\\", "/");
                if (subReporte.contains("/css")) {
                    temp = subReporte.substring(subReporte.indexOf("/css"), subReporte.length());
                } else if (subReporte.contains("/image")) {
                    temp = subReporte.substring(subReporte.indexOf("/image"), subReporte.length());
                } else if (subReporte.contains("/resources")) {
                    temp = subReporte.substring(subReporte.indexOf("/resources"), subReporte.length());
                } else if (subReporte.contains("/template")) {
                    temp = subReporte.substring(subReporte.indexOf("/template"), subReporte.length());
                }
                URL resource = Utils.class.getResource("/META-INF/resources" + temp);
                if (resource != null) {
                    return "jar:" + resource.getFile();
                } else {
                    System.out.println("No se hallo recurso: " + temp);
                    return null;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    public static Collection setCollection(Collection c, Object obj) {
        List l = (List) c;
        l.set(l.indexOf(obj), obj);
        return l;
    }

    public static String convertirDiaALetra(Integer fechaDia) {
        String dia;
        switch (fechaDia) {
            case 1:
                dia = "LUNES";
                break;
            case 2:
                dia = "MARTES";
                break;

            case 3:
                dia = "MIERCOLES";
                break;

            case 4:
                dia = "JUEVES";
                break;

            case 5:
                dia = "VIERNES";
                break;

            case 6:
                dia = "SABADO";
                break;

            case 7:
                dia = "DOMINGO";
                break;

            default:
                dia = "LUNES";
        }
        return dia;
    }

    public static Integer getNumeroDia(Date fechaIngreso) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaIngreso);
        return c.get(Calendar.DAY_OF_WEEK);
    }
    
    public static Integer getHora(Date fechaIngreso) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaIngreso);
        return c.get(Calendar.HOUR_OF_DAY);
    }
    
    public static Integer getMinuto(Date fechaIngreso) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaIngreso);
        return c.get(Calendar.MINUTE);
    }
    
    public static Integer getSegundo(Date fechaIngreso) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaIngreso);
        return c.get(Calendar.SECOND);
    }
    
    
}