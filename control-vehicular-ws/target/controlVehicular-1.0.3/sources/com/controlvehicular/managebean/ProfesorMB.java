/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.Profesor;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class ProfesorMB implements Serializable  {

    @Inject
    private ControlVehicularServices controlVehicularServices;

    private List<Profesor> datos;
    private Profesor dato;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        datos = controlVehicularServices.findAllProfesores();
    }

    public void loadValue() {
        dato = new Profesor();

    }

    public void save() {
        controlVehicularServices.saveProfesor(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Ingreso Correcto", null));
        loadData();
    }

    public void eliminar(Profesor r) {
        controlVehicularServices.eliminarProfesor(r);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
    }

    public void loadEditar(Profesor r) {
        dato = r;

    }

    public void editar() {
        controlVehicularServices.editarProfesor(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public List<Profesor> getDatos() {
        return datos;
    }

    public void setDatos(List<Profesor> datos) {
        this.datos = datos;
    }

    public Profesor getDato() {
        return dato;
    }

    public void setDato(Profesor dato) {
        this.dato = dato;
    }
}
