/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.ControlVehicular;
import com.controlvehicular.ws.UrlRest;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class ControlVehicularMB implements Serializable {

    @Inject
    private ControlVehicularServices controlVehicularServices;

    private List< ControlVehicular> datos;
    private List<String> parqueos;
    private ControlVehicular dato;
    private String urlImageEntrada;
    private String urlImageSalida;

    @PostConstruct
    public void init() {
        loadData();
    }

    public void loadData() {
        datos = controlVehicularServices.findAllControlVehicular();
        parqueos = controlVehicularServices.findAllParqueosDisponibles();
    }

    public void loadValue() {
        dato = new ControlVehicular();
    }

    public void enviarCorreo(Long idControlVehicular) {
        controlVehicularServices.enviarCorreoControlVehicular(idControlVehicular);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Correo Enviado", null));
    }

    public void loadImage(Long idControlVehicular) {
        this.urlImageEntrada = UrlRest.apiImagenEntradaControlVehicular + idControlVehicular.toString();
        this.urlImageSalida = UrlRest.apiImagenSalidaControlVehicular + idControlVehicular.toString();
    }

    public boolean filterByDate(Object value, Object filter, Locale locale) {
        if (filter == null) {
            return true;
        }
        if (value == null) {
            return false;
        }
        return DateUtils.truncatedEquals((Date) filter, (Date) value, Calendar.DATE);
    }

    public ControlVehicularServices getControlVehicularServices() {
        return controlVehicularServices;
    }

    public void setControlVehicularServices(ControlVehicularServices controlVehicularServices) {
        this.controlVehicularServices = controlVehicularServices;
    }

    public List<ControlVehicular> getDatos() {
        return datos;
    }

    public void setDatos(List<ControlVehicular> datos) {
        this.datos = datos;
    }

    public ControlVehicular getDato() {
        return dato;
    }

    public void setDato(ControlVehicular dato) {
        this.dato = dato;
    }

    public String getUrlImageEntrada() {
        return urlImageEntrada;
    }

    public void setUrlImageEntrada(String urlImageEntrada) {
        this.urlImageEntrada = urlImageEntrada;
    }

    public String getUrlImageSalida() {
        return urlImageSalida;
    }

    public void setUrlImageSalida(String urlImageSalida) {
        this.urlImageSalida = urlImageSalida;
    }

    public List<String> getParqueos() {
        return parqueos;
    }

    public void setParqueos(List<String> parqueos) {
        this.parqueos = parqueos;
    }

}
