/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.Horario;
import com.controlvehicular.util.Utils;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.io.Serializable;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;

/**
 *
 * @author gutya
 */
@Named(value = "horario")
@ViewScoped
public class HorarioMB implements Serializable {

    @Inject
    private ControlVehicularServices controlVehicularServices;

    private List<Horario> datos;
    private Horario dato;

    private DefaultScheduleModel calendario;
    private DefaultScheduleEvent evento;

    private Horario horarioDias;

    private Date dlunesDesde;
    private Date dmartesDesde;
    private Date dmiercolesDesde;
    private Date djuevesDesde;
    private Date dviernesDesde;
    private Date dsabadoDesde;
    private Date ddomingoDesde;

    private Date dlunesHasta;
    private Date dmartesHasta;
    private Date dmiercolesHasta;
    private Date djuevesHasta;
    private Date dviernesHasta;
    private Date dsabadoHasta;
    private Date ddomingoHasta;

    private String lunesDesde;
    private String martesDesde;
    private String miercolesDesde;
    private String juevesDesde;
    private String viernesDesde;
    private String sabadoDesde;
    private String domingoDesde;

    private String lunesHasta;
    private String martesHasta;
    private String miercolesHasta;
    private String juevesHasta;
    private String viernesHasta;
    private String sabadoHasta;
    private String domingoHasta;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        datos = controlVehicularServices.findAllHorarios();
    }

    public void loadValue() {
        dato = new Horario();
        horarioDias = new Horario();
    }

    public void save() {
        System.out.println("dato: " + dato.toString());
        controlVehicularServices.saveHorario(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Ingreso Correcto", null));
        loadData();
    }

    public void eliminar(Horario r) {
        controlVehicularServices.eliminarHorario(r);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
    }

    public void loadEditar(Horario r) {
        dato = r;

    }

    public void editar() {
        controlVehicularServices.editarHorario(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public void onEventSelect(SelectEvent selectEvent) {
        evento = (DefaultScheduleEvent) selectEvent.getObject();
        if (evento != null) {
//            horario = manager.find(Horario.class, Long.parseLong(evento.getId()));
//
//            if (!horario.getGrupoDetallePlanificacionCollection().isEmpty()) {
//                //tipoAviso = agenda.getTipoAvisoList().get(0);
//            }
        }
    }

    public void agregarHorario() throws ParseException {
        Format formatter = new SimpleDateFormat("HH:mm");

        if (validarFechasHorario()) {
            if (dlunesDesde != null && dlunesHasta != null) {

                lunesDesde = formatter.format(dlunesDesde);
                lunesHasta = formatter.format(dlunesHasta);

                this.horarioDias.setLunesDesde(lunesDesde);
                this.horarioDias.setLunesHasta(lunesHasta);

            }

            if (dmartesDesde != null && dmartesHasta != null) {

                martesDesde = formatter.format(dmartesDesde);
                martesHasta = formatter.format(dmartesHasta);

                this.horarioDias.setMartesDesde(martesDesde);
                this.horarioDias.setMartesHasta(martesHasta);

            }
            if (dmiercolesDesde != null && dmiercolesHasta != null) {

                miercolesDesde = formatter.format(miercolesDesde);
                miercolesHasta = formatter.format(dmiercolesHasta);

                this.horarioDias.setMiercolesDesde(miercolesDesde);
                this.horarioDias.setMiercolesHasta(miercolesHasta);

            }
            if (djuevesDesde != null && djuevesHasta != null) {

                juevesDesde = formatter.format(djuevesDesde);
                juevesHasta = formatter.format(djuevesHasta);

                this.horarioDias.setJuevesDesde(juevesDesde);
                this.horarioDias.setJuevesHasta(juevesHasta);

            }
            if (dviernesDesde != null && dviernesHasta != null) {

                viernesDesde = formatter.format(dviernesDesde);
                viernesHasta = formatter.format(dviernesHasta);

                this.horarioDias.setViernesDesde(viernesDesde);
                this.horarioDias.setViernesHasta(viernesHasta);

            }

            if (dsabadoDesde != null && dsabadoHasta != null) {

                sabadoDesde = formatter.format(dsabadoDesde);
                sabadoHasta = formatter.format(dsabadoHasta);

                this.horarioDias.setSabadoDesde(sabadoDesde);
                this.horarioDias.setSabadoHasta(sabadoHasta);

            }
            System.out.println("horarioDias: " + horarioDias.toString());

            controlVehicularServices.saveHorario(horarioDias);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Ingreso Correcto", null));
            loadData();
            
        }else{
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Verifique los datos de los horarios", null));
        }
    }

    public Boolean validarFechasHorario() {
        System.out.println("dlunesDesde: " + dlunesDesde);
        System.out.println("dlunesHasta: " + dlunesHasta);
        if (dlunesDesde != null && dlunesHasta != null) {
            if (Utils.getHora(dlunesDesde) >= Utils.getHora(dlunesHasta)) {
                if (Utils.getMinuto(dlunesDesde) >= Utils.getMinuto(dlunesHasta)) {
                    return false;
                }
            }
        }
        if (dmartesDesde != null && dmartesHasta != null) {
            if (Utils.getHora(dmartesDesde) >= Utils.getHora(dmartesHasta)) {
                if (Utils.getMinuto(dmartesDesde) >= Utils.getMinuto(dmartesHasta)) {
                    return false;
                }
            }
        }
        if (dmiercolesDesde != null && dmiercolesHasta != null) {
            if (Utils.getHora(dmiercolesDesde) >= Utils.getHora(dmiercolesHasta)) {
                if (Utils.getMinuto(dmiercolesDesde) >= Utils.getMinuto(dmiercolesHasta)) {
                    return false;
                }
            }
        }
        if (djuevesDesde != null && djuevesHasta != null) {
            if (Utils.getHora(djuevesDesde) >= Utils.getHora(djuevesHasta)) {
                if (Utils.getMinuto(djuevesDesde) >= Utils.getMinuto(djuevesHasta)) {
                    return false;
                }
            }
        }
        if (dviernesDesde != null && dviernesHasta != null) {
            if (Utils.getHora(dviernesDesde) >= Utils.getHora(dviernesHasta)) {
                if (Utils.getMinuto(dviernesDesde) >= Utils.getMinuto(dviernesHasta)) {
                    return false;
                }
            }
        }
        if (dsabadoDesde != null && dsabadoHasta != null) {
            if (Utils.getHora(dsabadoDesde) >= Utils.getHora(dsabadoHasta)) {
                if (Utils.getMinuto(dsabadoDesde) >= Utils.getMinuto(dsabadoHasta)) {
                    return false;
                }
            }
        }

        return true;
    }

    public List<Horario> getDatos() {
        return datos;
    }

    public void setDatos(List<Horario> datos) {
        this.datos = datos;
    }

    public Horario getDato() {
        return dato;
    }

    public void setDato(Horario dato) {
        this.dato = dato;
    }

    public Horario getHorarioDias() {
        return horarioDias;
    }

    public void setHorarioDias(Horario horarioDias) {
        this.horarioDias = horarioDias;
    }

    public Date getDlunesDesde() {
        return dlunesDesde;
    }

    public void setDlunesDesde(Date dlunesDesde) {
        this.dlunesDesde = dlunesDesde;
    }

    public Date getDmartesDesde() {
        return dmartesDesde;
    }

    public void setDmartesDesde(Date dmartesDesde) {
        this.dmartesDesde = dmartesDesde;
    }

    public Date getDmiercolesDesde() {
        return dmiercolesDesde;
    }

    public void setDmiercolesDesde(Date dmiercolesDesde) {
        this.dmiercolesDesde = dmiercolesDesde;
    }

    public Date getDjuevesDesde() {
        return djuevesDesde;
    }

    public void setDjuevesDesde(Date djuevesDesde) {
        this.djuevesDesde = djuevesDesde;
    }

    public Date getDviernesDesde() {
        return dviernesDesde;
    }

    public void setDviernesDesde(Date dviernesDesde) {
        this.dviernesDesde = dviernesDesde;
    }

    public Date getDsabadoDesde() {
        return dsabadoDesde;
    }

    public void setDsabadoDesde(Date dsabadoDesde) {
        this.dsabadoDesde = dsabadoDesde;
    }

    public Date getDdomingoDesde() {
        return ddomingoDesde;
    }

    public void setDdomingoDesde(Date ddomingoDesde) {
        this.ddomingoDesde = ddomingoDesde;
    }

    public Date getDlunesHasta() {
        return dlunesHasta;
    }

    public void setDlunesHasta(Date dlunesHasta) {
        this.dlunesHasta = dlunesHasta;
    }

    public Date getDmartesHasta() {
        return dmartesHasta;
    }

    public void setDmartesHasta(Date dmartesHasta) {
        this.dmartesHasta = dmartesHasta;
    }

    public Date getDmiercolesHasta() {
        return dmiercolesHasta;
    }

    public void setDmiercolesHasta(Date dmiercolesHasta) {
        this.dmiercolesHasta = dmiercolesHasta;
    }

    public Date getDjuevesHasta() {
        return djuevesHasta;
    }

    public void setDjuevesHasta(Date djuevesHasta) {
        this.djuevesHasta = djuevesHasta;
    }

    public Date getDviernesHasta() {
        return dviernesHasta;
    }

    public void setDviernesHasta(Date dviernesHasta) {
        this.dviernesHasta = dviernesHasta;
    }

    public Date getDsabadoHasta() {
        return dsabadoHasta;
    }

    public void setDsabadoHasta(Date dsabadoHasta) {
        this.dsabadoHasta = dsabadoHasta;
    }

    public Date getDdomingoHasta() {
        return ddomingoHasta;
    }

    public void setDdomingoHasta(Date ddomingoHasta) {
        this.ddomingoHasta = ddomingoHasta;
    }

    public String getLunesDesde() {
        return lunesDesde;
    }

    public void setLunesDesde(String lunesDesde) {
        this.lunesDesde = lunesDesde;
    }

    public String getMartesDesde() {
        return martesDesde;
    }

    public void setMartesDesde(String martesDesde) {
        this.martesDesde = martesDesde;
    }

    public String getMiercolesDesde() {
        return miercolesDesde;
    }

    public void setMiercolesDesde(String miercolesDesde) {
        this.miercolesDesde = miercolesDesde;
    }

    public String getJuevesDesde() {
        return juevesDesde;
    }

    public void setJuevesDesde(String juevesDesde) {
        this.juevesDesde = juevesDesde;
    }

    public String getViernesDesde() {
        return viernesDesde;
    }

    public void setViernesDesde(String viernesDesde) {
        this.viernesDesde = viernesDesde;
    }

    public String getSabadoDesde() {
        return sabadoDesde;
    }

    public void setSabadoDesde(String sabadoDesde) {
        this.sabadoDesde = sabadoDesde;
    }

    public String getDomingoDesde() {
        return domingoDesde;
    }

    public void setDomingoDesde(String domingoDesde) {
        this.domingoDesde = domingoDesde;
    }

    public String getLunesHasta() {
        return lunesHasta;
    }

    public void setLunesHasta(String lunesHasta) {
        this.lunesHasta = lunesHasta;
    }

    public String getMartesHasta() {
        return martesHasta;
    }

    public void setMartesHasta(String martesHasta) {
        this.martesHasta = martesHasta;
    }

    public String getMiercolesHasta() {
        return miercolesHasta;
    }

    public void setMiercolesHasta(String miercolesHasta) {
        this.miercolesHasta = miercolesHasta;
    }

    public String getJuevesHasta() {
        return juevesHasta;
    }

    public void setJuevesHasta(String juevesHasta) {
        this.juevesHasta = juevesHasta;
    }

    public String getViernesHasta() {
        return viernesHasta;
    }

    public void setViernesHasta(String viernesHasta) {
        this.viernesHasta = viernesHasta;
    }

    public String getSabadoHasta() {
        return sabadoHasta;
    }

    public void setSabadoHasta(String sabadoHasta) {
        this.sabadoHasta = sabadoHasta;
    }

    public String getDomingoHasta() {
        return domingoHasta;
    }

    public void setDomingoHasta(String domingoHasta) {
        this.domingoHasta = domingoHasta;
    }

}
