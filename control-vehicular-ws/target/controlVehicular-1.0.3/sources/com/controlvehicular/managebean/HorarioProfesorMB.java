/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.Horario;
import com.controlvehicular.entities.Profesor;
import com.controlvehicular.entities.ProfesorHorario;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class HorarioProfesorMB implements Serializable {

    @Inject
    private ControlVehicularServices controlVehicularServices;

    private List<Horario> horarios;
    private Long idHorario;

    private List<Profesor> profesores;
    private Long idProfesor;

    private ProfesorHorario dato;
    private List<ProfesorHorario> datos;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        profesores = controlVehicularServices.findAllProfesores();
        horarios = controlVehicularServices.findAllHorarios();
        datos = controlVehicularServices.findAllProfesorHorario();
    }

    public void load() {
        dato = new ProfesorHorario();
    }

    public void loadEditar(ProfesorHorario r) {
        dato = r;
        idProfesor = r.getProfesor().getId();
        idHorario = r.getHorario().getId();
    }

    public void save() {
        dato.setProfesor(new Profesor(idProfesor));
        dato.setHorario(new Horario(idHorario));
        controlVehicularServices.saveProfesorHorario(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Ingreso Correcto", null));
        loadData();
    }

    public void editar() {
        dato.setProfesor(new Profesor(idProfesor));
        dato.setHorario(new Horario(idHorario));
        controlVehicularServices.editarProfesorHorario(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public void eliminar(ProfesorHorario r) {
        controlVehicularServices.eliminarProfesorHorario(r);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public List<Horario> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<Horario> horarios) {
        this.horarios = horarios;
    }

    public List<Profesor> getProfesores() {
        return profesores;
    }

    public void setProfesores(List<Profesor> profesores) {
        this.profesores = profesores;
    }

    public ProfesorHorario getDato() {
        return dato;
    }

    public void setDato(ProfesorHorario dato) {
        this.dato = dato;
    }

    public List<ProfesorHorario> getDatos() {
        return datos;
    }

    public void setDatos(List<ProfesorHorario> datos) {
        this.datos = datos;
    }

    public Long getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Long idHorario) {
        this.idHorario = idHorario;
    }

    public Long getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(Long idProfesor) {
        this.idProfesor = idProfesor;
    }

}
