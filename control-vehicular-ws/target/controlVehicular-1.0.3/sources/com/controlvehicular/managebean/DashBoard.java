/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

/**
 *
 * @author Tech2Go
 */
@ManagedBean(name = "dashBoard")
@ViewScoped
public class DashBoard implements Serializable {

    private Client client;
//    private List<Alcalde> alcaldes;

    private Integer diasRestantes;
    
    @PostConstruct
    public void init() {
        client = ClientBuilder.newClient();
//        alcaldes = client.target(UrlRest.apiAlcaldes)
//                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Alcalde>>() {
//        });
        diasRestantes();
    }

    public void diasRestantes() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date fechaFinal = dateFormat.parse("2019-03-24");
            diasRestantes = (int) ((fechaFinal.getTime() - new Date().getTime()) / 86400000);
        } catch (ParseException ex) {
            Logger.getLogger(DashBoard.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Integer getDiasRestantes() {
        return diasRestantes;
    }

    public void setDiasRestantes(Integer diasRestantes) {
        this.diasRestantes = diasRestantes;
    }

    
//    
//    public List<Alcalde> getAlcaldes() {
//        return alcaldes;
//    }
//
//    public void setAlcaldes(List<Alcalde> alcaldes) {
//        this.alcaldes = alcaldes;
//    }

}
