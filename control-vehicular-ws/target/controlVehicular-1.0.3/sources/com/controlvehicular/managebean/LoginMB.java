/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.Usuario;
import com.controlvehicular.session.UsuarioSession;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;

/**
 *
 * @author Tech2Go
 */
@Named(value = "loginMB")
@ViewScoped
public class LoginMB implements Serializable {

    @Inject
    private ControlVehicularServices controlVehicularServices;
    @Inject
    private UsuarioSession usuarioSession;

    private List<Usuario> usuarios;

    private Long idRecinto, idProvincia, idCanton;
    private Usuario login, registro;
    private Boolean esMovil = Boolean.FALSE;

    private Long idParroquia;

    @PostConstruct
    public void init() {
        ///System.out.println("idCanton " + idCanton);
        login = new Usuario();
        registro = new Usuario();
        try {
        } catch (Exception e) {

        }

    }

    public String logincontrol() {
        Usuario response = controlVehicularServices.login(login);
        if (response != null) {
            if (response.getId() != null) {
                response.setContrasenia(login.getContrasenia());
                usuarioSession.setUsuario(response);
                return "/dashboard.xhtml?faces-redirect=true";

            }
        }

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Contraseña o Usuario Incorrectos"));

        return "login.xhtml";
    }

    public String logout() {
//        HttpSession session = UtilHttp.getSession();
//        session.invalidate();
//        
        usuarioSession.setUsuario(null);
        return "/login.xhtml?faces-redirect=true";
    }

    public String crearUsuario() {
        if (esMovil && idCanton == null) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Necesita Ingresar el Cantón"));
            return "";
        }
        
        registro = controlVehicularServices.saveUsuario(registro);
        if (registro != null && registro.getId() != null) {

            FacesContext facesContext = FacesContext.getCurrentInstance();
            Flash flash = facesContext.getExternalContext().getFlash();
            flash.setKeepMessages(true);
            flash.setRedirect(true);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Usuario Registrado "));
            registro = new Usuario();
            return "login.xhtml?faces-redirect=true";
        } else {
            return "";
        }

    }

    public Long getIdParroquia() {
        return idParroquia;
    }

    public void setIdParroquia(Long idParroquia) {
        this.idParroquia = idParroquia;
    }

    public void onComplete() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Usuario Registrado"));
    }

    public Long getIdCanton() {
        return idCanton;
    }

    public void setIdCanton(Long idCanton) {
        this.idCanton = idCanton;
    }

    public Usuario getLogin() {
        return login;
    }

    public void setLogin(Usuario login) {
        this.login = login;
    }

    public Long getIdRecinto() {
        return idRecinto;
    }

    public void setIdRecinto(Long idRecinto) {
        this.idRecinto = idRecinto;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Long getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Long idProvincia) {
        this.idProvincia = idProvincia;
    }

    public Usuario getRegistro() {
        return registro;
    }

    public void setRegistro(Usuario registro) {
        this.registro = registro;
    }

    public Boolean getEsMovil() {
        return esMovil;
    }

    public void setEsMovil(Boolean esMovil) {
        this.esMovil = esMovil;
    }

}
