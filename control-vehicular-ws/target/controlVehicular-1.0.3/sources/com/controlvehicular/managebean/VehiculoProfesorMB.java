/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.managebean;

import com.controlvehicular.entities.Profesor;
import com.controlvehicular.entities.Vehiculo;
import com.controlvehicular.entities.VehiculoProfesor;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author gutya
 */
@Named
@ViewScoped
public class VehiculoProfesorMB implements Serializable {

    @Inject
    private ControlVehicularServices controlVehicularServices;

    private List<Vehiculo> vehiculos;
    private Long idVehiculo;

    private List<Profesor> profesores;
    private Long idProfesor;

    private VehiculoProfesor dato;
    private List<VehiculoProfesor> datos;

    @PostConstruct
    public void init() {
        loadData();
    }

    private void loadData() {
        profesores = controlVehicularServices.findAllProfesores();
        vehiculos = controlVehicularServices.findAllVehiculos();
        datos = controlVehicularServices.findAllVehiculoProfesor();
    }

    public void loadValue() {
        dato = new VehiculoProfesor();

    }

    public void loadEditar(VehiculoProfesor r) {
        dato = r;
        idProfesor = r.getProfesor().getId();
        idVehiculo = r.getVehiculo().getId();
    }

    public void save() {
        dato.setProfesor(new Profesor(idProfesor));
        dato.setVehiculo(new Vehiculo(idVehiculo));
        controlVehicularServices.saveVehiculoProfesor(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Ingreso Correcto", null));
        loadData();
    }

    public void eliminar(VehiculoProfesor r) {
        controlVehicularServices.eliminarVehiculoProfesor(r);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public void editar() {
        dato.setProfesor(new Profesor(idProfesor));
        dato.setVehiculo(new Vehiculo(idVehiculo));
        controlVehicularServices.editarVehiculoProfesor(dato);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Datos Actualizados", null));
        loadData();
    }

    public List<VehiculoProfesor> getDatos() {
        return datos;
    }

    public void setDatos(List<VehiculoProfesor> datos) {
        this.datos = datos;
    }

    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(List<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public List<Profesor> getProfesores() {
        return profesores;
    }

    public void setProfesores(List<Profesor> profesores) {
        this.profesores = profesores;
    }

    public VehiculoProfesor getDato() {
        return dato;
    }

    public void setDato(VehiculoProfesor dato) {
        this.dato = dato;
    }

    public Long getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Long idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public Long getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(Long idProfesor) {
        this.idProfesor = idProfesor;
    }

}
