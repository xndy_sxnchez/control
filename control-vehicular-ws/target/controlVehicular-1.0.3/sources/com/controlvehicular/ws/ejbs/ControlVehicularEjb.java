/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.ws.ejbs;

import com.controlvehicular.entities.ControlVehicular;
import com.controlvehicular.entities.Horario;
import com.controlvehicular.entities.Modelo;
import com.controlvehicular.entities.Profesor;
import com.controlvehicular.entities.ProfesorHorario;
import com.controlvehicular.entities.Rol;
import com.controlvehicular.entities.Usuario;
import com.controlvehicular.entities.Vehiculo;
import com.controlvehicular.entities.VehiculoProfesor;
import com.controlvehicular.session.UsuarioSession;
import com.controlvehicular.ws.UrlRest;
import com.controlvehicular.ws.interfaces.ControlVehicularServices;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 *
 * @author Tech2Go
 */
@Singleton(name = "ControlVehicularEjb")
//@Lock(LockType.READ)
@ApplicationScoped
public class ControlVehicularEjb implements ControlVehicularServices {

    @Inject
    private UsuarioSession usuarioSession;

    private final Client client = ClientBuilder.newClient();
    private HttpAuthenticationFeature feature = null;

    @Override
    public Usuario login(Usuario usuario) {
        try {
            Usuario customer
                    = client.target(UrlRest.apiLogin(usuario.getNombreUsuario(), usuario.getContrasenia()))
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .get(Usuario.class);
            return customer;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Usuario> findAllUsuarios() {
        List<Usuario> usuarios = new ArrayList<Usuario>();
        usuarios = client.target(UrlRest.apiUsuarios)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Usuario>>() {
        });
        return usuarios;
    }

    @Override
    public Usuario saveUsuario(Usuario usuario) {
        Usuario response = client.target(UrlRest.apiGuardarUsuarios).request().post(Entity.json(usuario), Usuario.class);
        return response;
    }

    @Override
    public Usuario eliminarUsuario(Usuario u) {
        Usuario response = client.target(UrlRest.apiEliminarUsuario).request().put(Entity.json(u), Usuario.class);
        return response;
    }

    @Override
    public Usuario editarUsuario(Usuario u) {
        Usuario response = client.target(UrlRest.apiEditarUsuario).request().put(Entity.json(u), Usuario.class);
        return response;
    }

    @Override
    public List<Rol> findAllRoles() {
        List<Rol> response = new ArrayList<Rol>();
        response = client.target(UrlRest.apiRoles)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Rol>>() {
        });
        return response;
    }

    @Override
    public Rol saveRol(Rol rol) {
        Rol response = client.target(UrlRest.apiGuardarRol).request().post(Entity.json(rol), Rol.class);
        return response;
    }

    @Override
    public Rol eliminarRol(Rol rol) {
        Rol response = client.target(UrlRest.apiEliminarRol).request().put(Entity.json(rol), Rol.class);
        return response;
    }

    @Override
    public Rol editarRol(Rol rol) {
        Rol response = client.target(UrlRest.apiEditarRol).request().put(Entity.json(rol), Rol.class);
        return response;
    }

    @Override
    public List<Profesor> findAllProfesores() {
        List<Profesor> response = new ArrayList<>();
        response = client.target(UrlRest.apiProfesor)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Profesor>>() {
        });
        return response;
    }

    @Override
    public Profesor saveProfesor(Profesor dato) {
        Profesor response = client.target(UrlRest.apiGuardarProfesor).request().post(Entity.json(dato), Profesor.class);
        return response;
    }

    @Override
    public Profesor editarProfesor(Profesor dato) {
        Profesor response = client.target(UrlRest.apiEditarProfesor).request().put(Entity.json(dato), Profesor.class);
        return response;
    }

    @Override
    public void eliminarProfesor(Profesor dato) {
        client.target(UrlRest.apiEliminarProfesor).request().put(Entity.json(dato));
    }

    @Override
    public List<Vehiculo> findAllVehiculos() {
        List<Vehiculo> response = client.target(UrlRest.apiVehiculo)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Vehiculo>>() {
        });
        return response;
    }

    @Override
    public Vehiculo saveVehiculo(Vehiculo dato) {
        Vehiculo response = client.target(UrlRest.apiGuardarVehiculo).request().post(Entity.json(dato), Vehiculo.class);
        return response;
    }

    @Override
    public Vehiculo editarVehiculo(Vehiculo dato) {
        Vehiculo response = client.target(UrlRest.apiEditarVehiculo).request().put(Entity.json(dato), Vehiculo.class);
        return response;
    }

    @Override
    public void eliminarVehiculo(Vehiculo dato) {
        client.target(UrlRest.apiEliminarVehiculo).request().put(Entity.json(dato));
    }

    @Override
    public List<Horario> findAllHorarios() {
        List<Horario> response = client.target(UrlRest.apiHorario)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Horario>>() {
        });
        return response;
    }

    @Override
    public Horario saveHorario(Horario dato) {
        Horario response = client.target(UrlRest.apiGuardarHorario).request().post(Entity.json(dato), Horario.class);
        return response;
    }

    @Override
    public Horario editarHorario(Horario dato) {
        Horario response = client.target(UrlRest.apiEditarHorario).request().put(Entity.json(dato), Horario.class);
        return response;
    }

    @Override
    public void eliminarHorario(Horario dato) {
        client.target(UrlRest.apiEliminarHorario).request().put(Entity.json(dato));
    }

    @Override
    public List<Modelo> findAllModelos() {
        List<Modelo> response = client.target(UrlRest.apiHorario)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<Modelo>>() {
        });
        return response;
    }

    @Override
    public List<VehiculoProfesor> findAllVehiculoProfesor() {
        List<VehiculoProfesor> response = client.target(UrlRest.apiVehiculoProfesor)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<VehiculoProfesor>>() {
        });
        return response;
    }

    @Override
    public VehiculoProfesor saveVehiculoProfesor(VehiculoProfesor dato) {
        VehiculoProfesor response = client.target(UrlRest.apiGuardarVehiculoProfesor).request().post(Entity.json(dato), VehiculoProfesor.class);
        return response;
    }

    @Override
    public void eliminarVehiculoProfesor(VehiculoProfesor dato) {
        client.target(UrlRest.apiEliminarVehiculoProfesor).request().put(Entity.json(dato));
    }

    @Override
    public void editarVehiculoProfesor(VehiculoProfesor dato) {
        client.target(UrlRest.apiEditarVehiculoProfesor).request().put(Entity.json(dato));
    }

    @Override
    public List<ProfesorHorario> findAllProfesorHorario() {
        List<ProfesorHorario> response = client.target(UrlRest.apiProfesorHorario)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<ProfesorHorario>>() {
        });
        return response;
    }

    @Override
    public ProfesorHorario saveProfesorHorario(ProfesorHorario dato) {
        ProfesorHorario response = client.target(UrlRest.apiGuardarProfesorHorario).request().post(Entity.json(dato), ProfesorHorario.class);
        return response;
    }

    @Override
    public void eliminarProfesorHorario(ProfesorHorario dato) {
        client.target(UrlRest.apiEliminarprofesorHorario).request().put(Entity.json(dato));
    }

    @Override
    public void editarProfesorHorario(ProfesorHorario dato) {
        client.target(UrlRest.apiEditarProfesorHorario).request().put(Entity.json(dato));
    }

    @Override
    public List<ControlVehicular> findAllControlVehicular() {
        List<ControlVehicular> response = client.target(UrlRest.apiControlVehicular)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<ControlVehicular>>() {
        });
        return response;
    }

    @Override
    public List<String> findAllParqueosDisponibles() {
        List<String> response = client.target(UrlRest.apiParqueos)
                .request(MediaType.APPLICATION_JSON).get(new GenericType<List<String>>() {
        });
        return response;
    }

    @Override
    public void enviarCorreoControlVehicular(Long idControlVehicular) {
        client.target(UrlRest.apiEnviarCorreoControlVehicular + "/" + idControlVehicular.toString()).request().get();

    }

}
