/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author gutya
 */
@Entity
@Table(name = "control_vehicular", schema = "aplicacion")
public class ControlVehicular implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    //@JoinColumn(name = "profesor_horario", referencedColumnName = "id", insertable = false, updatable = false)
    //@ManyToOne(fetch = FetchType.EAGER)
    private String profesorHorario;
    @JoinColumn(name = "vehiculo_profesor", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private VehiculoProfesor vehiculoProfesor;
    @JsonIgnore
    private Date fecha;
    @JsonIgnore
    private Date horaIngreso;
    @JsonIgnore
    private Date horaSalida;
    private String tiempoEstacionado;
    @JsonIgnore
    private String imagenEntrada;
    @JsonIgnore
    private String imagenSalida;

    @Transient
    private Long lFecha;
    @Transient
    private Long lHoraIngreso;
    @Transient
    private Long lHoraSalida;

    @Transient
    private String horaIn;
    @Transient
    private String horaSal;
    
    @Transient
    private List<ProfesorHorario> profesorHorarios;

    public List<ProfesorHorario> getProfesorHorarios() {       
        return profesorHorarios;
    }

    public void setProfesorHorarios(List<ProfesorHorario> profesorHorarios) {
        this.profesorHorarios = profesorHorarios;
    }
    
    

    public ControlVehicular() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(Date horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getTiempoEstacionado() {
        return tiempoEstacionado;
    }

    public void setTiempoEstacionado(String tiempoEstacionado) {
        this.tiempoEstacionado = tiempoEstacionado;
    }

    public String getImagenEntrada() {
        return imagenEntrada;
    }

    public void setImagenEntrada(String imagenEntrada) {
        this.imagenEntrada = imagenEntrada;
    }

    public String getImagenSalida() {
        return imagenSalida;
    }

    public void setImagenSalida(String imagenSalida) {
        this.imagenSalida = imagenSalida;
    }

    public String getProfesorHorario() {
        return profesorHorario;
    }

    public void setProfesorHorario(String profesorHorario) {
        this.profesorHorario = profesorHorario;
    }

    public VehiculoProfesor getVehiculoProfesor() {
        return vehiculoProfesor;
    }

    public void setVehiculoProfesor(VehiculoProfesor vehiculoProfesor) {
        this.vehiculoProfesor = vehiculoProfesor;
    }

    public Long getlFecha() {
        if (fecha != null) {
            lFecha = fecha.getTime();
        }
        return lFecha;
    }

    public void setlFecha(Long lFecha) {
        this.lFecha = lFecha;
    }

    public Long getlHoraIngreso() {
        if (horaIngreso != null) {
            lHoraIngreso = horaIngreso.getTime();
        }
        return lHoraIngreso;
    }

    public void setlHoraIngreso(Long lHoraIngreso) {
        this.lHoraIngreso = lHoraIngreso;
    }

    public Long getlHoraSalida() {
        if (horaSalida != null) {
            lHoraSalida = horaSalida.getTime();
        }
        return lHoraSalida;
    }

    public void setlHoraSalida(Long lHoraSalida) {
        this.lHoraSalida = lHoraSalida;
    }

    public String getHoraIn() {
        return horaIn;
    }

    public void setHoraIn(String horaIn) {
        this.horaIn = horaIn;
    }

    public String getHoraSal() {
        return horaSal;
    }

    public void setHoraSal(String horaSal) {
        this.horaSal = horaSal;
    }

}
