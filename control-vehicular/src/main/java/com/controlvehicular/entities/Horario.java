/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.entities;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author gutya
 */
@Entity
@Table(name = "horario", schema = "aplicacion")

public class Horario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "lunes_desde")

    private String lunesDesde;
    @Column(name = "martes_desde")
    private String martesDesde;
    @Column(name = "miercoles_desde")
    private String miercolesDesde;
    @Column(name = "jueves_desde")
    private String juevesDesde;
    @Column(name = "viernes_desde")

    private String viernesDesde;
    @Column(name = "sabado_desde")

    private String sabadoDesde;
    @Column(name = "domingo_desde")

    private String domingoDesde;
    @Size(max = 500)
    @Column(name = "observacion_horario")
    private String observacionHorario;
    @Column(name = "lunes_hasta")

    private String lunesHasta;
    @Column(name = "martes_hasta")

    private String martesHasta;
    @Column(name = "miercoles_hasta")

    private String miercolesHasta;
    @Column(name = "jueves_hasta")

    private String juevesHasta;
    @Column(name = "viernes_hasta")

    private String viernesHasta;
    @Column(name = "sabado_hasta")

    private String sabadoHasta;
    @Column(name = "domingo_hasta")

    private String domingoHasta;

    public Horario() {
    }

    public Horario(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLunesDesde() {
        return lunesDesde;
    }

    public void setLunesDesde(String lunesDesde) {
        this.lunesDesde = lunesDesde;
    }

    public String getMartesDesde() {
        return martesDesde;
    }

    public void setMartesDesde(String martesDesde) {
        this.martesDesde = martesDesde;
    }

    public String getMiercolesDesde() {
        return miercolesDesde;
    }

    public void setMiercolesDesde(String miercolesDesde) {
        this.miercolesDesde = miercolesDesde;
    }

    public String getJuevesDesde() {
        return juevesDesde;
    }

    public void setJuevesDesde(String juevesDesde) {
        this.juevesDesde = juevesDesde;
    }

    public String getViernesDesde() {
        return viernesDesde;
    }

    public void setViernesDesde(String viernesDesde) {
        this.viernesDesde = viernesDesde;
    }

    public String getSabadoDesde() {
        return sabadoDesde;
    }

    public void setSabadoDesde(String sabadoDesde) {
        this.sabadoDesde = sabadoDesde;
    }

    public String getDomingoDesde() {
        return domingoDesde;
    }

    public void setDomingoDesde(String domingoDesde) {
        this.domingoDesde = domingoDesde;
    }

    public String getObservacionHorario() {
        return observacionHorario;
    }

    public void setObservacionHorario(String observacionHorario) {
        this.observacionHorario = observacionHorario;
    }

    public String getLunesHasta() {
        return lunesHasta;
    }

    public void setLunesHasta(String lunesHasta) {
        this.lunesHasta = lunesHasta;
    }

    public String getMartesHasta() {
        return martesHasta;
    }

    public void setMartesHasta(String martesHasta) {
        this.martesHasta = martesHasta;
    }

    public String getMiercolesHasta() {
        return miercolesHasta;
    }

    public void setMiercolesHasta(String miercolesHasta) {
        this.miercolesHasta = miercolesHasta;
    }

    public String getJuevesHasta() {
        return juevesHasta;
    }

    public void setJuevesHasta(String juevesHasta) {
        this.juevesHasta = juevesHasta;
    }

    public String getViernesHasta() {
        return viernesHasta;
    }

    public void setViernesHasta(String viernesHasta) {
        this.viernesHasta = viernesHasta;
    }

    public String getSabadoHasta() {
        return sabadoHasta;
    }

    public void setSabadoHasta(String sabadoHasta) {
        this.sabadoHasta = sabadoHasta;
    }

    public String getDomingoHasta() {
        return domingoHasta;
    }

    public void setDomingoHasta(String domingoHasta) {
        this.domingoHasta = domingoHasta;
    }

}