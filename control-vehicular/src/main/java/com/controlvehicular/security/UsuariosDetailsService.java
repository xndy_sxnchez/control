package com.controlvehicular.security;
import com.controlvehicular.entities.Usuario;
import com.controlvehicular.repository.UsuarioRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.security.core.userdetails.User;

@Component
public class UsuariosDetailsService  implements UserDetailsService{

    @Autowired
    private UsuarioRepository usuarioRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Usuario usuario = usuarioRepository.findByNombreUsuario(username);

        if(usuario == null){
            throw new UsernameNotFoundException(String.format("Usuario no encontrado", username));
        }
       
        UserDetails userDetails = User.withUsername(usuario.getNombreUsuario())
                .password(usuario.getContrasenia())
                .roles("USER").build();
        return userDetails;
    }


}
