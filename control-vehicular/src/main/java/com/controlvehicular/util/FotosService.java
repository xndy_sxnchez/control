package com.controlvehicular.util;

import com.controlvehicular.Constantes;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FotosService {


    public static Long uploadFile(InputStream stream, String nombre, String contentType) {

        Connection conn = getConnection();
        Long oid = null;
        try {
            conn.setAutoCommit(false);
            LargeObjectManager lobj = ((org.postgresql.PGConnection) conn).getLargeObjectAPI();
            oid = lobj.createLO(LargeObjectManager.READ | LargeObjectManager.WRITE);
            LargeObject obj = lobj.open(oid, LargeObjectManager.WRITE);

            byte buf[] = new byte[20480];
            int s, tl = 0;
            while ((s = stream.read(buf, 0, 20480)) > 0) {
                obj.write(buf, 0, s);
                tl += s;
            }

            // Close the large object
            obj.close();

            // Now insert the row into table
            // Now insert the row into table
            PreparedStatement ps = conn.prepareStatement("INSERT INTO aplicacion.archivos VALUES (?, ?, ?, ?)");
            ps.setLong(1, oid);
            ps.setString(2, nombre);
            ps.setTimestamp(3, new Timestamp((new Date()).getTime()));
            ps.setString(4, contentType);
            ps.executeUpdate();
            ps.close();
            stream.close();

            // Finally, commit the transaction.
            conn.commit();

        } catch (SQLException | IOException ex) {
            Logger.getLogger(FotosService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FotosService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return oid;
    }

    public static InputStream streamFile(Long oid) {
        LargeObjectManager lobj = null;
        LargeObject obj = null;
        InputStream is = null;
        Connection conn = getConnection();
        try {
            if (conn == null) {
                return null;
            }
            conn.setAutoCommit(false);
            lobj = ((org.postgresql.PGConnection) conn).getLargeObjectAPI();
            try {
                obj = lobj.open(oid, LargeObjectManager.READ);
            } catch (SQLException e) {
                e.printStackTrace();
                conn.close();
                return null;
            }
            byte buf[] = obj.read(obj.size());
            InputStream stream = new ByteArrayInputStream(buf);
            is = stream;
            obj.close();
        } catch (SQLException ex) {
            Logger.getLogger(FotosService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FotosService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return is;
    }


    public static Connection getConnection() {
        try {
            Class.forName(Constantes.driver);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FotosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        Properties props = new Properties();
        props.setProperty("user", Constantes.userName);
        props.setProperty("password", Constantes.password);
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(Constantes.url, props);
        } catch (SQLException ex) {
            Logger.getLogger(FotosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }


}
