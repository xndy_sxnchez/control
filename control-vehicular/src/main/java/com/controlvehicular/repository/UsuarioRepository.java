package com.controlvehicular.repository;

import com.controlvehicular.entities.Usuario;
import org.springframework.data.repository.CrudRepository;


public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findByNombreUsuario(String nombreUsuario);

    Usuario findByNombreUsuarioAndContraseniaAndEstado(String nombreUsuario, String contasenia, String estado);

}
