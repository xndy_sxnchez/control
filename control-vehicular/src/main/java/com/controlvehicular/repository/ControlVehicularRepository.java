/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.repository;

import com.controlvehicular.entities.ControlVehicular;
import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author gutya
 */
public interface ControlVehicularRepository  extends CrudRepository<ControlVehicular, Long>{
    
    public List<ControlVehicular> findAllByOrderByIdDesc();
    
    public List<ControlVehicular> findAllByFechaAndHoraSalidaIsNull(Date fecha);
    
}
