package com.controlvehicular.repository;

import com.controlvehicular.entities.Modelo;
import org.springframework.data.repository.CrudRepository;

public interface ModeloRepository  extends CrudRepository<Modelo, Long> {
}
