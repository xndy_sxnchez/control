package com.controlvehicular.repository;

import com.controlvehicular.entities.VehiculoProfesor;
import org.springframework.data.repository.CrudRepository;

public interface VehiculoProfesorRepository extends CrudRepository<VehiculoProfesor, Long> {
}
