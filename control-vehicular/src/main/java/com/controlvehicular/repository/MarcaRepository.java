package com.controlvehicular.repository;

import com.controlvehicular.entities.Marca;
import org.springframework.data.repository.CrudRepository;

public interface MarcaRepository  extends CrudRepository<Marca, Long> {
}
