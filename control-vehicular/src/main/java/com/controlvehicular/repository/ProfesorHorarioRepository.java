package com.controlvehicular.repository;


import com.controlvehicular.entities.ProfesorHorario;
import org.springframework.data.repository.CrudRepository;

public interface ProfesorHorarioRepository  extends CrudRepository<ProfesorHorario, Long> {
}
