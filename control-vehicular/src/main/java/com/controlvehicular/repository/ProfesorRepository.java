package com.controlvehicular.repository;

import com.controlvehicular.entities.Profesor;
import org.springframework.data.repository.CrudRepository;

public interface ProfesorRepository extends CrudRepository<Profesor, Long> {

    

}
