package com.controlvehicular.repository;

import com.controlvehicular.entities.Rol;
import org.springframework.data.repository.CrudRepository;

public interface RolRepository  extends CrudRepository<Rol, Long> {
}
