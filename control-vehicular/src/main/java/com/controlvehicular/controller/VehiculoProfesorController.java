/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.controller;

import com.controlvehicular.entities.VehiculoProfesor;
import com.controlvehicular.repository.VehiculoProfesorRepository;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gutya
 */
@RestController
@RequestMapping("/api/")
public class VehiculoProfesorController {

    @Autowired
    private VehiculoProfesorRepository vehiculoProfesorRepository;

    @RequestMapping(value = "vehiculosProfesores", method = RequestMethod.GET)
    public ResponseEntity<List<VehiculoProfesor>> findAllVehiculos() {
        return new ResponseEntity<>(Lists.newArrayList(vehiculoProfesorRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "vehiculoProfesor/guardar", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> registrar(@RequestBody VehiculoProfesor p) {
        try {
            p.setFechaCreacion(new Date());
            p = vehiculoProfesorRepository.save(p);
            return new ResponseEntity<>(p, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "vehiculoProfesor/eliminar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> eliminar(@RequestBody VehiculoProfesor p) {
        try {
            vehiculoProfesorRepository.delete(p);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "vehiculoProfesor/editar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> editar(@RequestBody VehiculoProfesor p) {
        try {
            vehiculoProfesorRepository.save(p);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }
}
