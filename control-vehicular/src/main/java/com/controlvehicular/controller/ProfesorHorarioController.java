/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.controller;

import com.controlvehicular.entities.ProfesorHorario;
import com.controlvehicular.repository.ProfesorHorarioRepository;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gutya
 */
@RestController
@RequestMapping("/api/")
public class ProfesorHorarioController {

    @Autowired
    private ProfesorHorarioRepository profesorHorarioRepository;

    @RequestMapping(value = "profesoresHorarios", method = RequestMethod.GET)
    public ResponseEntity<List<ProfesorHorario>> findAllVehiculos() {
        return new ResponseEntity<>(Lists.newArrayList(profesorHorarioRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "profesorHorario/guardar", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> registrar(@RequestBody ProfesorHorario p) {
        try {
            p.setFechaCreacion(new Date());
            p = profesorHorarioRepository.save(p);
            return new ResponseEntity<>(p, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "profesorHorario/eliminar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> eliminar(@RequestBody ProfesorHorario p) {
        try {
            profesorHorarioRepository.delete(p);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "profesorHorario/editar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> editar(@RequestBody ProfesorHorario p) {
        try {
            profesorHorarioRepository.save(p);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

}
