/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.controller;

import com.controlvehicular.entities.Vehiculo;
import com.controlvehicular.repository.VehiculoRepository;
import com.google.common.collect.Lists;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gutya
 */
@RestController
@RequestMapping("/api/")
public class VehiculoController {
     
     @Autowired
    private VehiculoRepository vehiculoRepository;

    @RequestMapping(value = "vehiculos", method = RequestMethod.GET)
    public ResponseEntity<List<Vehiculo>> findAllVehiculos() {
        return new ResponseEntity<>(Lists.newArrayList(vehiculoRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "vehiculo/guardar", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> registrar(@RequestBody Vehiculo p) {
        try {
            p = vehiculoRepository.save(p);
            return new ResponseEntity<>(p, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "vehiculo/eliminar", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<?> eliminar(@RequestBody Vehiculo p) {
        try {
            vehiculoRepository.delete(p);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "vehiculo/editar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> editar(@RequestBody Vehiculo rol) {
        try {
            rol = vehiculoRepository.save(rol);
            return new ResponseEntity<>(rol, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }
    
}
