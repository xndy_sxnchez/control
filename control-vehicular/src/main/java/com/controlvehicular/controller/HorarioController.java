/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.controller;

import com.controlvehicular.entities.Horario;
import com.controlvehicular.repository.HorarioRepository;
import com.google.common.collect.Lists;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gutya
 */
@RestController
@RequestMapping("/api/")
public class HorarioController {

    @Autowired
    private HorarioRepository horarioRepository;

    @RequestMapping(value = "horarios", method = RequestMethod.GET)
    public ResponseEntity<List<Horario>> findAllHorarios() {
        return new ResponseEntity<>(Lists.newArrayList(horarioRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "horario/guardar", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> registrar(@RequestBody Horario p) {
        try {
            p = horarioRepository.save(p);
            return new ResponseEntity<>(p, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "horario/eliminar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> eliminar(@RequestBody Horario p) {
        try {
            horarioRepository.delete(p);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "horario/editar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> editar(@RequestBody Horario rol) {
        try {
            rol = horarioRepository.save(rol);
            return new ResponseEntity<>(rol, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

}
