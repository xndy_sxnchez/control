package com.controlvehicular.controller;

import com.controlvehicular.entities.Usuario;
import com.controlvehicular.repository.UsuarioRepository;
import com.controlvehicular.security.MD5PasswordEncoder;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @RequestMapping(value = "/inicioSesion/nombreusuario/{nombreusuario}/contrasenia/{contrasenia}", method
            = RequestMethod.GET)
    public Usuario login(@PathVariable String nombreusuario, @PathVariable String contrasenia) {
        Usuario usuario
                = usuarioRepository.findByNombreUsuarioAndContraseniaAndEstado(nombreusuario, new MD5PasswordEncoder().encode(contrasenia), "A");
        return usuario;
    }

    @RequestMapping(value = "usuario/guardar", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> registrarUsuario(@RequestBody Usuario aclUser) {

        try {
            aclUser.setEstado("A");
            aclUser.setContrasenia(new MD5PasswordEncoder().encode(aclUser.getContrasenia()));
            aclUser.setFechaIngreso(new Date());
            aclUser = usuarioRepository.save(aclUser);
            return new ResponseEntity<>(aclUser, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "usuarios", method = RequestMethod.GET)
    public ResponseEntity<List<Usuario>> findAllUsuarios() {
        return new ResponseEntity<>(Lists.newArrayList(usuarioRepository.findAll()), HttpStatus.OK);
    }
    
    
     @RequestMapping(value = "usuario/eliminar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> eliminar(@RequestBody Usuario u) {
        try {
            u.setEstado("I");
            u = usuarioRepository.save(u);
            return new ResponseEntity<>(u, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "usuario/editar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> editar(@RequestBody Usuario u) {
        try {
            u = usuarioRepository.save(u);
            return new ResponseEntity<>(u, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

}
