/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.controller;

import com.controlvehicular.entities.Modelo;
import com.controlvehicular.repository.ModeloRepository;
import com.google.common.collect.Lists;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gutya
 */
@RestController
@RequestMapping("/api/")
public class ModeloController {

    @Autowired
    private ModeloRepository modeloRepository;

    @RequestMapping(value = "modelos", method = RequestMethod.GET)
    public ResponseEntity<List<Modelo>> findAllModelos() {
        return new ResponseEntity<>(Lists.newArrayList(modeloRepository.findAll()), HttpStatus.OK);
    }

}
