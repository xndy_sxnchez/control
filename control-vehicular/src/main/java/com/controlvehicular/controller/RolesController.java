/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.controller;

import com.controlvehicular.entities.Rol;
import com.controlvehicular.repository.RolRepository;
import com.google.common.collect.Lists;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gutya
 */
@RestController
@RequestMapping("/api/")
public class RolesController {

    @Autowired
    private RolRepository rolRepository;

    @RequestMapping(value = "roles", method = RequestMethod.GET)
    public ResponseEntity<List<Rol>> findAllRoles() {
        return new ResponseEntity<>(Lists.newArrayList(rolRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "rol/guardar", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> registrar(@RequestBody Rol rol) {
        try {
            rol.setEstado(Boolean.TRUE);
            rol = rolRepository.save(rol);
            return new ResponseEntity<>(rol, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "rol/eliminar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> eliminar(@RequestBody Rol rol) {
        try {
            rol.setEstado(Boolean.FALSE);
            rol = rolRepository.save(rol);
            return new ResponseEntity<>(rol, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "rol/editar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> editar(@RequestBody Rol rol) {
        try {
            rol = rolRepository.save(rol);
            return new ResponseEntity<>(rol, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

}
