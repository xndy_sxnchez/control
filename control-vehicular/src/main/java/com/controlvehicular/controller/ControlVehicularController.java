/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.controller;

import com.controlvehicular.async.EmailService;
import com.controlvehicular.entities.ControlVehicular;
import com.controlvehicular.entities.ProfesorHorario;
import com.controlvehicular.repository.ControlVehicularRepository;
import com.controlvehicular.repository.ProfesorHorarioRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author gutya
 */
@RestController
@RequestMapping("/api/")
public class ControlVehicularController {

    @Autowired
    private ControlVehicularRepository controlVehicularRepository;

    @Autowired
    private ProfesorHorarioRepository profesorHorarioRepository;

    @Autowired
    private EmailService emailService;

    public static SimpleDateFormat sdfResult = new SimpleDateFormat("HH:mm:ss", Locale.US);
    public static SimpleDateFormat sdfResultMinutos = new SimpleDateFormat("m", Locale.US);

    @RequestMapping(value = "parqueos", method = RequestMethod.GET)
    public ResponseEntity<List<String>> findAllParqueosDisponibles() throws ParseException {

        Integer totalParqueo = 25;
        List<String> parqueos = new ArrayList<>();

        Date today = new Date();

        List<ControlVehicular> controlVehiculars = controlVehicularRepository.findAllByFechaAndHoraSalidaIsNull(today);
        Integer parqueosBase = controlVehiculars.size();

        System.out.println("parqueosBase: " + parqueosBase);

        if (parqueosBase != 0) {
            if (parqueosBase <= totalParqueo) {
                for (int i = 1; i <= parqueosBase; i++) {
                    parqueos.add("OCUPADO");
                }
                for (int i = 1; i <= (totalParqueo - parqueosBase); i++) {
                    parqueos.add("DISPONIBLE");
                }
            } else {
                for (int i = 1; i <= totalParqueo; i++) {
                    parqueos.add("DISPONIBLES");
                }
            }
        } else {
            for (int i = 1; i <= totalParqueo; i++) {
                parqueos.add("DISPONIBLES");
            }
        }


        return new ResponseEntity<>(parqueos, HttpStatus.OK);
    }

    @RequestMapping(value = "controlVehicular", method = RequestMethod.GET)
    public ResponseEntity<List<ControlVehicular>> findAllControlVehicular() throws ParseException {
        List<ControlVehicular> controlVehiculars = controlVehicularRepository.findAllByOrderByIdDesc();

        String horaE, horaS;
        Date dateE, dateS;
        String d1970 = " 1970-01-01 ";

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        List<ProfesorHorario> profesorHorarios;

        for (ControlVehicular cv : controlVehiculars) {
            profesorHorarios = new ArrayList<>();
            if (cv.getProfesorHorario() != null) {
                String[] profesorHorarioArray = cv.getProfesorHorario().split("-");
                for (String s : profesorHorarioArray) {
                    profesorHorarios.add(profesorHorarioRepository.findById(Long.valueOf(s.trim())).get());
                }
            }
            cv.setProfesorHorarios(profesorHorarios);
            if (cv.getHoraIngreso() != null) {

                if (cv.getHoraIngreso().toString().contains(d1970.trim())) {
                    cv.setHoraIn(cv.getHoraIngreso().toString().replace(d1970.trim(), " "));
                    cv.setHoraIn(cv.getHoraIn().substring(0, cv.getHoraIn().length() - 2));
                }
            }
            if (cv.getHoraSalida() != null) {
                if (cv.getHoraSalida().toString().contains(d1970.trim())) {
                    cv.setHoraSal(cv.getHoraSalida().toString().replace(d1970.trim(), " "));
                    cv.setHoraSal(cv.getHoraSal().substring(0, cv.getHoraSal().length() - 2));
                }
            }

            if (cv.getHoraIngreso() != null && cv.getHoraSalida() != null) {
                horaE = sdf2.format(cv.getFecha()) + " " + cv.getHoraIngreso().toString();
                horaS = sdf2.format(cv.getFecha()) + " " + cv.getHoraSalida().toString();
                if (horaE.contains(d1970)) {
                    horaE = horaE.replace(d1970, " ");
                    horaE = horaE.substring(0, horaE.length() - 2);
                }
                if (horaS.contains(d1970)) {
                    horaS = horaS.replace(d1970, " ");
                    horaS = horaS.substring(0, horaS.length() - 2);
                }

                dateE = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(horaE);
                dateS = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(horaS);

                if (cv.getTiempoEstacionado() == null) {
                    Date difference2 = restarFechas(dateE, dateS);
                    cv.setTiempoEstacionado(sdfResult.format(difference2));
                    controlVehicularRepository.save(cv);
                }
            }

            if (cv.getHoraIngreso() != null && cv.getHoraSalida() == null) {
                horaE = sdf2.format(cv.getFecha()) + " " + cv.getHoraIngreso().toString();
                horaS = sdf3.format(new Date());
                if (horaE.contains(d1970)) {
                    horaE = horaE.replace(d1970, " ");
                    horaE = horaE.substring(0, horaE.length() - 2);
                }
                if (horaS.contains(d1970)) {
                    horaS = horaS.replace(d1970, " ");
                    horaS = horaS.substring(0, horaS.length() - 2);
                }

                dateE = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(horaE);
                dateS = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(horaS);

                if (cv.getTiempoEstacionado() == null) {
                    Date difference2 = restarFechas(dateE, dateS);
                    cv.setTiempoEstacionado(sdfResult.format(difference2));
                }
            }

        }
        return new ResponseEntity<>(controlVehiculars, HttpStatus.OK);
    }

    @RequestMapping(value = "controlVehicular/id/{idControlVehicular}", method
            = RequestMethod.GET)
    public ResponseEntity<?> login(@PathVariable Long idControlVehicular) {

        ControlVehicular c = controlVehicularRepository.findById(idControlVehicular).get();

        emailService.sendMail("EXCESO", "EXCESO 2", c.getVehiculoProfesor().getVehiculo(),
                c.getVehiculoProfesor().getProfesor().getCorreo());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "controlVehicular/imagenEntrada/{idControlVehicular}", method = RequestMethod.GET,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] getImageEntrada(@PathVariable Long idControlVehicular) throws IOException {
        ControlVehicular c = controlVehicularRepository.findById(idControlVehicular).get();
        InputStream targetStream = new ByteArrayInputStream(Base64.decodeBase64(c.getImagenEntrada()));
        return IOUtils.toByteArray(targetStream);
    }

    @RequestMapping(value = "controlVehicular/imagenSalida/{idControlVehicular}", method = RequestMethod.GET,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] getImageSalida(@PathVariable Long idControlVehicular) throws IOException {
        ControlVehicular c = controlVehicularRepository.findById(idControlVehicular).get();
        InputStream targetStream = new ByteArrayInputStream(Base64.decodeBase64(c.getImagenSalida()));
        return IOUtils.toByteArray(targetStream);
    }

    private Date restarFechas(Date dateInicio, Date dateFinal) {
        long milliseconds = dateFinal.getTime() - dateInicio.getTime();
        int seconds = (int) (milliseconds / 1000) % 60;
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.SECOND, seconds);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.HOUR_OF_DAY, hours);
        return c.getTime();
    }

}
