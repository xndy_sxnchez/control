/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.controller;

import com.controlvehicular.entities.Profesor;
import com.controlvehicular.repository.ProfesorRepository;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gutya
 */
@RestController
@RequestMapping("/api/")
public class ProfesorController {
    
     @Autowired
    private ProfesorRepository profesorRepository;

    @RequestMapping(value = "profesores", method = RequestMethod.GET)
    public ResponseEntity<List<Profesor>> findAllProfesores() {
        return new ResponseEntity<>(Lists.newArrayList(profesorRepository.findAll()), HttpStatus.OK);
    }

    @RequestMapping(value = "profesor/guardar", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<?> registrar(@RequestBody Profesor p) {
        try {
            p.setFechaCreacion(new Date());
            p = profesorRepository.save(p);
            return new ResponseEntity<>(p, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "profesor/eliminar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> eliminar(@RequestBody Profesor p) {
        try {
            profesorRepository.delete(p);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "profesor/editar", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<?> editar(@RequestBody Profesor rol) {
        try {
            rol = profesorRepository.save(rol);
            return new ResponseEntity<>(rol, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }
    
}
