/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.async;

import com.controlvehicular.entities.Vehiculo;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class EmailService {

    @Async
    public void sendMail(String header, String mensaje, Vehiculo vehiculo, String destinatario) {
        try {
            //System.out.println("correo " + correoContribuyente);
            Email correo = new Email(vehiculo, header, mensaje, destinatario);
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            Future<Boolean> future = executorService.submit(() -> {
                //  Thread.sleep(2000);
                return correo.sendMail();
            });
            while (!future.isDone()) {
                //System.out.println("sending mail");
            }

            System.out.println("sended mail");

        } catch (Exception e) {
            Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, null, e);
        }
    }


}
