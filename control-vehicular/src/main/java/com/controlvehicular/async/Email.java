/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlvehicular.async;

import com.controlvehicular.entities.Vehiculo;
import java.io.File;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * @author Origami
 */
public class Email implements Serializable {

    protected List<File> adjuntos = new ArrayList<>();
    protected String usuarioCorreo = Constantes.correo_entidad;
    protected String password = Constantes.pass_correo_entidad;
    protected String destinatario;
    protected String copiaOcultaBCC;
    protected String copiaCC;
    protected String asunto;
    protected String mensaje;
    private Vehiculo vehiculo;
    private String header = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "<meta charset=\"UTF-8\">\n" +
            "\n" +
            "<style type=\"text/css\"> \n" +
            "#logo{ \n" +
            "width: 500px; \n" +
            "text-align: center; \n" +
            "} \n" +
            "#header_colored{ \n" +
            "background-color:#18219C; \n" +
            "height:5% \n" +
            "border-radius: 30px 30px 0px 0px; \n" +
            "-moz-border-radius: 30px 30px 0px 0px; \n" +
            "-webkit-border-radius: 30px 30px 0px 0px; \n" +
            "border: 0px solid #18219C;\n" +
            "} \n" +
            "#footer_colored{ \n" +
            "background-color:#549FE9; \n" +
            "border: 0px 0px 30px 30px; \n" +
            "-moz-border-radius: 0px 0px 30px 30px; \n" +
            "-webkit-border-radius: 0px 0px 30px 30px; \n" +
            "border: 0px solid #549FE9; \n" +
            "} \n" +
            "</style> \n" +
            "</head> \n" +
            "\n" +
            "<body>\n" +
            "\t<center>\n" +
            "\t\t<div style=\"width:550px;\"><br/>\n" +
            "\t\t\t<div> \n" +
            "\t\t\t\t<p><img src=\"https://upload.wikimedia.org/wikipedia/commons/f/ff/UGlogo.png\" style=\"width:550px;\"/></p> \n" +
            "\t\t\t</div>\n" +
            "\t\t\t<div align=\"justify\">\n";

    private String footer = "<br><br>\n" +
            "        \t</div>\n" +
            "      \t\t<div id=\"body_colored\" style=\"width: 550px;height: 10px; background-color: #549FE9\" align=\"center\"></div>\t\t\t\n" +
            "\t\t\t<div style=\"background-color: #549FE9; \">\n" +
            "\t\t\t\t<font color=#000000; style=\"font-weight: bold;\">\n" +
            "\t\t\t\t\tCONTROL VEHICULAR<br>\n" +
            "\t\t\t\t\tPor favor no responda a este correo. El mensaje es informativo.<br>\n" +
            "\t\t\t\t</font>\n" +
            "\t\t\t</div>\t\n" +
            "\t\t\t<div id=\"footer_colored\" style=\"width: 550px;height: 15px;\" align=\"center\"></div>\n" +
            "\t\t</div>\n" +
            "    </center>\n" +
            "</body> \n" +
            "</html>";
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    public Email(Vehiculo vehiculo, String copiaOcultaBCC, String copiaCC, String asunto, String mensaje, String destinatario) throws ParseException {
        String mensajeTemp =
                header + "\t\t\t EL VEHICULO CON PLACAS # " + vehiculo.getPlaca() + " tiene ua advertencia por estacionarse en horario no permitido";
        this.vehiculo = vehiculo;
        this.destinatario = destinatario;
        this.copiaOcultaBCC = copiaOcultaBCC;
        this.copiaCC = copiaCC;
        this.asunto = asunto;
        //System.out.println("mensajeTemp " + mensajeTemp);
        this.mensaje = mensajeTemp;
    }

    public Email(Vehiculo vehiculo, String header, String mensaje, String destinatario) throws ParseException {
        this(vehiculo, null, null, header, mensaje, destinatario);
    }

    public boolean sendMail() {
        try {
            //INGRESO DE LAS PROPIEDADES DE LA CONEXION
            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "smtp");
            props.setProperty("mail.smtp.host", Constantes.smtp_host);
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", Constantes.smtp_port);
            props.setProperty("mail.smtp.user", usuarioCorreo);
            props.setProperty("mail.smtp.auth", "true");
            //INSTANCIA DE LA SESSION
            Session session = Session.getInstance(props, null);
            //CUERPO DEL MENSAJE
            MimeMessage mimeMessage = new MimeMessage(session);

            mimeMessage.setFrom(new InternetAddress(usuarioCorreo, Constantes.entidad));
            mimeMessage.setSubject("CONTROL VEHICULAR");
            mimeMessage.setSentDate(new Date());
            mimeMessage.addRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
            if (copiaOcultaBCC != null) {
                mimeMessage.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(copiaOcultaBCC));
            }
            if (copiaCC != null) {
                mimeMessage.addRecipients(Message.RecipientType.CC, InternetAddress.parse(copiaCC));
            }
            //TEXTO DEL MENSAJE
            MimeBodyPart texto = new MimeBodyPart();
            //texto.setText(mensaje);
            //System.out.println("mensaje " + mensaje);
            texto.setContent(mensaje, "text/html; charset=utf-8");
            //CONTENEDOR DE LAS PARTES
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(texto);
            //ADJUNTAR LOS ARCHIVO EN PARTES
            /* MimeBodyPart file;
            for (File f : adjuntos) {
                file = new MimeBodyPart();
                file.attachFile(f);
                multipart.addBodyPart(file);
            }*/
            //AGREGAR MULTIPART EN CUERPO DEL MENSAJE
            mimeMessage.setContent(multipart);
            // ENVIAR MENSAJE
            Transport transport = session.getTransport("smtp");
            transport.connect(usuarioCorreo, password);
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            transport.close();

        } catch (Exception ex) {
            Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

}
